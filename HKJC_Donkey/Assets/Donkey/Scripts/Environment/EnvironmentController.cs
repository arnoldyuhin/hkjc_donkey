﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HKJC.Donkey.Environment {
	public class EnvironmentController : MonoBehaviour {
		public enum EnvironmentState {
			Normal, Cloud, Thunder
		}
		private EnvironmentState state = EnvironmentState.Normal;
		public EnvironmentState State {
			get { return state; }
			set {
				switch (state) {
				case EnvironmentState.Cloud:
					UnloopCloudParticles ();
					break;
				case EnvironmentState.Thunder:
					UnloopThunderParticles ();
					break;
				}
				state = value;
				switch (state) {
				case EnvironmentState.Normal:
					UnloopCloudParticles ();
					UnloopThunderParticles ();
					ChangedToNormalEnvironment?.Invoke ();
					break;
				case EnvironmentState.Cloud:
					LoopCloudParticle ();
					ChangedToCloudEnvironment?.Invoke ();
					break;
				case EnvironmentState.Thunder:
					LoopThunderParticle ();
					ChangedToThunderEnvironment?.Invoke ();
					break;

				}
			}
		}

		public UnityEvent ChangedToNormalEnvironment;
		public UnityEvent ChangedToCloudEnvironment;
		public UnityEvent ChangedToThunderEnvironment;


		[SerializeField] private List<ParticleSystem> cloudParticles = new List<ParticleSystem> ();
		[SerializeField] private List<ParticleSystem> thunderParticles = new List<ParticleSystem> ();

		public void ChangeEnvironment () {
			switch (State) {
			case EnvironmentState.Normal:
				State = EnvironmentState.Cloud;
				break;
			case EnvironmentState.Cloud:
				State = EnvironmentState.Thunder;
				break;
			}
		}

		public void ReturnEnvironment () {
			switch (State) {
			case EnvironmentState.Thunder:
				State = EnvironmentState.Cloud;
				break;
			case EnvironmentState.Cloud:
				State = EnvironmentState.Normal;
				break;
			}
		}

		public void ResetEnvironment () {
			State = EnvironmentState.Normal;
		}

		private void LoopCloudParticle () {
			for (int i = 0; i < cloudParticles.Count; i++) {
				ParticleSystem.MainModule main = cloudParticles [i].main;
				main.simulationSpeed = 1f;
			}
			//for (int i = 0; i < cloudParticles.Count; i++) {
			//	var p = cloudParticles [i].main;
			//	p.loop = true;
			//}
		}

		private void LoopThunderParticle () {
			//for (int i = 0; i < thunderParticles.Count; i++) {
			//	var p = thunderParticles [i].main;
			//	p.loop = true;
			//}
		}

		private void UnloopCloudParticles () {
			for (int i = 0; i < cloudParticles.Count; i++) {
				cloudParticles [i].Stop (true, ParticleSystemStopBehavior.StopEmitting);
				ParticleSystem.MainModule main = cloudParticles [i].main;
				main.simulationSpeed = 3f;
				//var p = cloudParticles [i].main;
				//p.loop = false;

			}
		}
		private void UnloopThunderParticles () {
			for (int i = 0; i < thunderParticles.Count; i++) {
				thunderParticles [i].Stop (true, ParticleSystemStopBehavior.StopEmitting);
				//var p = thunderParticles [i].main;
				//p.loop = false;
			}
		}


	}
}