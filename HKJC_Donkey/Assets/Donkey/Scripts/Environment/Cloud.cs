﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace HKJC.Donkey.Environment {
	public class Cloud : MonoBehaviour {

		[SerializeField] private ParticleSystem particle;

		public void PlayParticle(bool flag) {
			//particle.
		}

		public void Show (float duration, float delay = 0f) {
			this.transform.DOKill ();
			if (!this.gameObject.activeSelf) {
				this.gameObject.SetActive (true);
			}
			this.transform.localScale = Vector3.zero;
			this.transform.DOScale (1f, duration)
				.SetEase (Ease.OutBack)
				.SetDelay (delay);
		}

		public void Destroy (float duration, float delay = 0f) {
			this.transform.DOKill ();
			this.transform.DOScale (0f, duration)
				.SetEase (Ease.InBack)
				.SetDelay (delay)
				.OnComplete (() => {
					GameObject.Destroy (this.gameObject);
				});
		}
	}
}