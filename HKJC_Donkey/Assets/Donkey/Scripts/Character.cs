﻿using System;
using System.Collections;
using System.Collections.Generic;
using GamestryLab.Audio;
using GamestryLab.Utilities;
using HKJC.Donkey.Dialogue;
using UnityEngine;

namespace HKJC.Donkey {
	public class Character : MonoBehaviour {

		[SerializeField] private Animator animator;
		[SerializeField] private DialogueDatabase dialogueDatabase;
		[SerializeField] private DialogueBox dialogueBox;

		//private string currentDialogueId;

		private AudioPlayer audioplayer;
		public AudioPlayer Audio {
			get {
				if (audioplayer == null) {
					audioplayer = ServiceLocator.GetService<AudioPlayer> () as AudioPlayer;
				}
				return audioplayer;
			}
		}

		public Action Updated;
		public Action ReactionEnded;

		private void Update () {
			Updated?.Invoke ();
		}

		public void SetDialogue (string dialogueId) {
			DialogueData data = dialogueDatabase.GetData (dialogueId);
			if (!LanguageMaster.Instance.IsEnglish) {
				dialogueBox.SetDialogue (data.Message);
			} else {
				dialogueBox.SetDialogue (data.EnglishMessage);
			}
		}

		public void ShowDialogue (string dialogueId) {
			SetDialogue (dialogueId);
			//if (currentDialogueId == dialogueId) return;
			//currentDialogueId = dialogueId;
			float showDuration = 1f;
			dialogueBox.Show (showDuration);
		}

		public void PlayDialogueSound (string dialogueId) {
			DialogueData data = dialogueDatabase.GetData (dialogueId);
			if (!string.IsNullOrEmpty (data.SoundId)) {
				Audio.PlayOneShot (data.SoundId);
			}
		}

		public void ShowAnimation (string dialogueId) {
			DialogueData data = dialogueDatabase.GetData (dialogueId);
			presetAnimState = data.AnimationClip;
			CrossFadeAnimation (data.AnimationClip);
		}

		public void ShowReactAnimation (string dialogueId, Action callback) {
			DialogueData data = dialogueDatabase.GetData (dialogueId);
			ReactAndPlayBackAnimation (data.ReactAnimationClip, data.AnimationClip);
			ReactionEnded += callback;
		}

		public void HideDialogue () {
			float hideDuration = 1f;
			dialogueBox.Hide (hideDuration);
		}

		public void PlayAnimation (string animState) {
			if (animator == null) return;
			ResetAnimatorPos ();
			animator.Play (animState);
		}

		public void CrossFadeAnimation (string animState) {
			if (animator == null) return;
			ResetAnimatorPos ();
			animator.CrossFade (animState, 0.05f);
		}

		public void ReactAndPlayBackAnimation (string reactState, string animState) {
			if (animator == null) return;
			reactCounter = 0f;
			Updated += CheckReactionEnded;
			CrossFadeAnimation (reactState);
			ReactionEnded += CrossFadeToPrsetAnimation;
			presetAnimState = animState;
		}

		public void StopReaction () {
			Updated = null;
			CrossFadeToPrsetAnimation ();
		}

		private float reactTime = 2f;
		private float reactCounter = 0f;
		private void CheckReactionEnded () {
			if (reactCounter < reactTime) {
				reactCounter += Time.deltaTime;
				return;
			}
			Updated -= CheckReactionEnded;
			ReactionEnded?.Invoke ();
		}

		private string presetAnimState;
		private void CrossFadeToPrsetAnimation () {
			ReactionEnded = null;
			CrossFadeAnimation (presetAnimState);
		}

		public void ResetAnimatorPos () {
			animator.transform.localPosition = Vector3.zero;
			animator.transform.localEulerAngles = Vector3.zero;
		}
	}
}