﻿using System.Collections;
using System.Collections.Generic;
using GamestryLab.Utilities;
using UnityEngine;
using TMPro;

public class LanguageMaster : MonoBehaviour {
	public static LanguageMaster Instance;

	[SerializeField] private bool isEnglish;
	public bool IsEnglish {
		get { return isEnglish; }
		set {
			isEnglish = value;
			ChangeResultLanguage (isEnglish);
		}
	}

	[SerializeField] private TextMeshProUGUI resultTitle;
	[SerializeField] private TextMeshProUGUI resultButton;
	[SerializeField] private TextMeshProUGUI prestartDialogue;



	private void Awake () {
		if (Instance == null) {
			Instance = this;
		} else {
			Destroy (this.gameObject);
		}
	}

	private void ChangeResultLanguage (bool isEnglish) {
		if (resultTitle != null) {
			resultTitle.text = isEnglish ? "Result" : "遊戲結束";
		}
		if (resultButton != null) {
			resultButton.text = isEnglish ? "Back to title" : "返回標題";
		}
		if (prestartDialogue != null) {
			prestartDialogue.text = isEnglish ?
				"Vans are faster and more convenient. Withthe app, we can check the real-time location as well. You have been working for me for a long time, I do hope that you can learn other things. How about I transfer you to another department?"
				:
				"阿驢～市⾯上依啲叫做物流～貨Van⼜⽅便⼜快⼜企理，⽤埋個app仲可以check住啲貨去到邊。你幫咗我咁耐，我都希望你可以學下其他嘢，不如去第⼆個部⾨幫下⼿啦～";
		}
	}
}
