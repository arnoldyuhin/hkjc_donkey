﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace HKJC.Donkey.Objects {
	public class Carrot : RecycleableObject {
		public Rigidbody Rigidbody;
		[SerializeField] private Collider collider;
		[SerializeField] private ParticleSystem hitParticle;
		[SerializeField] private float activeDuration = 5f;
		private float activatedTime;

		public enum CarrotState { Off, Transition, Ready, Active, Die }
		private CarrotState state;
		public CarrotState State {
			get { return state; }
			set {
				switch (state) {
				case CarrotState.Active:
					Rigidbody.isKinematic = true;
					break;
				case CarrotState.Die:
					Rigidbody.isKinematic = true;
					break;
				}
				state = value;
				switch (state) {
				case CarrotState.Off:
					collider.enabled = false;
					break;
				case CarrotState.Transition:
					collider.enabled = false;
					break;
				case CarrotState.Ready:
					collider.enabled = false;
					break;
				case CarrotState.Active:
					collider.enabled = true;
					Rigidbody.isKinematic = false;
					break;
				case CarrotState.Die:
					Rigidbody.isKinematic = false;
					break;
				}
			}
		}

		private bool canEmitParticle = false;
		public bool CanEmitParticle {
			get; set;
		}

		public void Spawn () {
			Rigidbody.isKinematic = true;
			float spawnDuration = 0.2f;
			State = CarrotState.Transition;
			this.transform.localScale = Vector3.zero;
			this.transform.DOKill ();
			this.transform.DOScale (0.3f, spawnDuration)
				.SetEase (Ease.OutBack)
				.OnComplete (() => {
					State = CarrotState.Ready;
				});
		}

		public void Activate () {
			State = CarrotState.Active;
			activatedTime = Time.time;
			float scaleDuration = 0.5f;
			this.transform.DOKill ();
			this.transform.DOScale (1f, scaleDuration)
			.SetEase (Ease.OutQuad);
		}

		private void Update () {
			CountTime ();
		}

		private void CountTime () {
			if (State != CarrotState.Active) return;
			if (Time.time - activatedTime > activeDuration) {
				Disappear ();
			}
		}

		public void Disappear () {
			State = CarrotState.Die;
			float disappearDuration = 1f;
			this.transform.DOKill ();
			this.transform.DOScale (0f, disappearDuration)
				.SetEase (Ease.InBack)
				.OnComplete (() => {
					State = CarrotState.Off;
					Recycle ();
				});
		}

		public void Kill () {
			State = CarrotState.Die;
			float killDuration = 1f;
			Rigidbody.velocity = Vector3.zero;
			Rigidbody.AddForce (UnityEngine.Random.Range (0f, 50f) * UnityEngine.Random.onUnitSphere + Vector3.up * 200f);
			this.transform.DOKill ();
			this.transform.DOScale (0f, killDuration)
				.SetEase (Ease.OutQuad)
				.OnComplete (() => {
					State = CarrotState.Off;
					hitParticle.transform.SetParent (this.transform);
					hitParticle.transform.localPosition = Vector3.zero;
					Recycle ();
				});
		}

		private void OnCollisionEnter (Collision collision) {
			if (!CanEmitParticle) return;
			//if (collision.collider.tag == "IgnoreCarrot") return;
			CanEmitParticle = false;
			hitParticle.transform.SetParent (null);
			hitParticle.transform.position = collision.GetContact (0).point;
			hitParticle.Play ();
		}
	}
}