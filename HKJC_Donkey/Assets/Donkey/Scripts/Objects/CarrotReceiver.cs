﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HKJC.Donkey.Objects {
	public class CarrotReceiver : MonoBehaviour {

		public UnityEvent OnReceived;
		[SerializeField] private bool isKillingCarrot;

		public void OnCollisionEnter (Collision collision) {
			Carrot carrot = collision.collider.GetComponent<Carrot> ();
			if (carrot == null) return;
			if (carrot.State == Carrot.CarrotState.Active) {
				OnReceived?.Invoke ();
				if (isKillingCarrot) {
					carrot.Kill ();
				}
			}

		}
	}
}