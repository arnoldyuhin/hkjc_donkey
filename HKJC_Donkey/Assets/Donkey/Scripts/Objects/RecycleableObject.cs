﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HKJC.Donkey.Objects {
	public class RecycleableObject : MonoBehaviour {
		public event Action<RecycleableObject> Recycled;

		protected virtual void Recycle () {
			Recycled?.Invoke (this);
		}
	}
}
