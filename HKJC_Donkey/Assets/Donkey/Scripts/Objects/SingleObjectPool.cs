﻿using System.Collections;
using System.Collections.Generic;
using GamestryLab.Debugger;
using UnityEngine;

namespace HKJC.Donkey.Objects {
	public class SingleObjectPool : MonoBehaviour {
		[SerializeField] private RecycleableObject recycleableObject;
		private List<RecycleableObject> objectInUse = new List<RecycleableObject> ();
		private List<RecycleableObject> objectInPool = new List<RecycleableObject> ();

		public RecycleableObject GetObject () {
			RecycleableObject ro = null;
			if (objectInPool.Count > 0) {
				ro = objectInPool [0];
				objectInPool.Remove (ro);
			} else {
				ro = CreateObject ();
			}
			objectInUse.Add (ro);
			ro.gameObject.SetActive (true);
			ro.Recycled += RecycleObject;
			return ro;
		}

		public void SetObject (RecycleableObject obj) {
			ClearAll ();
			recycleableObject = obj;
		}

		private RecycleableObject CreateObject () {
			RecycleableObject ro = UnityEngine.Object.Instantiate (recycleableObject) as RecycleableObject;
			return ro;
		}

		public void ClearAll () {
			for (int i = 0; i < objectInUse.Count; i++) {
				GameObject.Destroy (objectInUse [i].gameObject);
			}
			objectInUse.Clear ();
			for (int i = 0; i < objectInPool.Count; i++) {
				GameObject.Destroy (objectInPool [i].gameObject);
			}
			objectInPool.Clear ();
		}

		public void RecycleObject (RecycleableObject ro) {
			if (!objectInUse.Contains (ro)) {
				DebugLog.Log (Color.red, "{0} is not exist in ObjectPool({1}.", ro.name, this.gameObject.name);
				return;
			}
			ro.Recycled -= RecycleObject;
			ro.gameObject.SetActive (false);
			ro.transform.SetParent (this.transform);
			objectInUse.Remove (ro);
			objectInPool.Add (ro);
		}
	}
}
