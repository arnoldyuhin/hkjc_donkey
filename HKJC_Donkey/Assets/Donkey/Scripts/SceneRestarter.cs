﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HKJC.Donkey {
	public class SceneRestarter : MonoBehaviour {

		[SerializeField] private float holdTime = 5f;
		private float currentTime = 0f;

		[SerializeField] private string sceneName;

		private void Update () {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				currentTime = 0f;
			}
			if (Input.GetKey(KeyCode.Escape)) {
				HoldToRestart ();
			}
		}


		public void HoldToRestart () {
			if (currentTime > holdTime) {
				Restart ();
			} else {
				currentTime += Time.deltaTime;
			}
		}

		private void Restart () {
			SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
		}
	}
}