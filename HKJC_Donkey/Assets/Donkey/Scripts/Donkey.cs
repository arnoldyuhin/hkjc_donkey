﻿using System.Collections;
using System.Collections.Generic;
using GamestryLab.Operations;
using UnityEngine;

namespace HKJC.Donkey {
	public class Donkey : MonoBehaviour {
		[SerializeField] private Character character;
		[SerializeField] private IntParameter roundParam;

		public enum DonkeyState { Off, Normal, React }
		private DonkeyState state = DonkeyState.Off;
		public DonkeyState State {
			get { return state; }
			set { state = value; }
		}

		public void ResetAnimation () {
			character.PlayAnimation ("A");
			character.StopReaction ();
		}

		public void TurnOnReaction (bool flag) {
			if (flag == true) {
				State = DonkeyState.Normal;
			} else {
				State = DonkeyState.Off;
				character.StopReaction ();
			}
		}

		public void ShowDialogue (string id) {
			character.ShowDialogue (id);
		}

		public void ShowInitialDialogue () {
			string dialogueId = string.Format ("R{0}_initial", roundParam.Value);
			character.ShowDialogue (dialogueId);
			character.PlayDialogueSound (dialogueId);
			character.ShowAnimation (dialogueId);
		}

		public void ShowInitialDialogueSilently () {
			string dialogueId = string.Format ("R{0}_initial", roundParam.Value);
			character.SetDialogue (dialogueId);
			character.ShowAnimation (dialogueId);
		}

		//public void ShowInitialDialogue

		public void ShowReactDialogue () {
			string dialogueId = string.Format ("R{0}_react", roundParam.Value);
			character.ShowDialogue (dialogueId);
			character.ShowAnimation (dialogueId);
		}

		public void ShowInitialAnimation () {
			string dialogueId = string.Format ("R{0}_initial", roundParam.Value);
			character.ShowAnimation (dialogueId);
		}

		public void ShowCollectDialogue () {
			if (State == DonkeyState.Off) return;
			if (State == DonkeyState.React) return;
			State = DonkeyState.React;
			string dialogueId = string.Format ("R{0}_initial", roundParam.Value);
			character.ShowDialogue (dialogueId);
			character.ShowReactAnimation (dialogueId, () => {
				State = DonkeyState.Normal;
			});
		}
	}
}