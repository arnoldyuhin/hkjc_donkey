﻿using System.Collections;
using System.Collections.Generic;
using HKJC.Donkey.UI;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;
using GamestryLab.Audio;
using GamestryLab.Utilities;

namespace HKJC.Donkey.Answer {
	public class AnswerBox : MonoBehaviour {

		[SerializeField] private Transform originTransform;

		[SerializeField] private Collider collider;

		[SerializeField] private TextMeshProUGUI answerText;
		[SerializeField] private InteractableObject interactableObject;

		[SerializeField] private MeshRenderer textRenderer;
		[SerializeField] private Material normalTextMaterial;
		[SerializeField] private Material greyTextMaterial;

		[SerializeField] private Image markImage;
		[SerializeField] private Sprite correctSprite;
		[SerializeField] private Sprite wrongSprite;

		public enum AnswerBoxState { Off, Active }
		private AnswerBoxState state = AnswerBoxState.Off;
		public AnswerBoxState State {
			get { return state; }
			set {
				switch (state) {
				case AnswerBoxState.Off:
					break;
				case AnswerBoxState.Active:
					break;
				}
				state = value;
				switch (state) {
				case AnswerBoxState.Off:
					collider.enabled = false;
					interactableObject.State = InteractableObject.InteractableUIState.Off;
					break;
				case AnswerBoxState.Active:
					collider.enabled = true;
					interactableObject.State = InteractableObject.InteractableUIState.Idle;
					break;
				}
			}
		}

		private AudioPlayer audioplayer;
		public AudioPlayer Audio {
			get {
				if (audioplayer == null) {
					audioplayer = ServiceLocator.GetService<AudioPlayer> () as AudioPlayer;
				}
				return audioplayer;
			}
		}

		private AnswerData data;
		public AnswerData Data {
			get { return data; }
		}
		private bool isAlreadySelected = false;
		public bool IsAlreadySelected {
			get { return isAlreadySelected; }
			set { isAlreadySelected = value; }
		}

		[SerializeField] private Vector3 originalPos = Vector3.zero;
		[SerializeField] private Vector3 originalEulerAngles = Vector3.zero;

		public void Reset () {
			State = AnswerBoxState.Off;
			interactableObject.State = InteractableObject.InteractableUIState.Off;
			IsAlreadySelected = false;
			HideMarkImage ();
		}

		public void MoveToOrigin (float delay = 0f) {
			this.transform.DOKill ();
			this.transform.SetParent (originTransform, true);
			float moveDuration = 1f;
			this.transform.DOScale (1f, moveDuration)
				.SetDelay (delay);
			this.transform.DOLocalRotate (Vector3.zero, moveDuration)
				.SetEase (Ease.OutQuad)
				.SetDelay (delay);
			this.transform.DOLocalMove (Vector3.zero, moveDuration)
				.SetEase (Ease.OutQuad)
				.SetDelay (delay)
				.OnComplete (() => {
					EnableInteract (true);
					State = AnswerBoxState.Active;
					Audio.PlayOneShot ("event:/BubbleAppear");
				});
		}



		public void ResetPosition () {
			//old version
			this.transform.localPosition = originalPos;
			this.transform.eulerAngles = originalEulerAngles;
		}

		public void SetSelectedColor (bool flag) {
			textRenderer.material = flag ? greyTextMaterial : normalTextMaterial;
		}

		public void EnableInteract (bool flag) {
			if (interactableObject.State == InteractableObject.InteractableUIState.Off) return;
			if (IsAlreadySelected) {
				//enable every start select, not enable it if already selected this round
				interactableObject.State = InteractableObject.InteractableUIState.Disable;
				return;
			}

			if (flag == true) {
				interactableObject.State = InteractableObject.InteractableUIState.Idle;
			} else {
				interactableObject.State = InteractableObject.InteractableUIState.Disable;
			}
		}

		public void SetAnswerData (AnswerData answerData) {
			data = answerData;
			if (!LanguageMaster.Instance.IsEnglish) {
				answerText.text = answerData.Message;
			} else {
				answerText.text = answerData.EnglishMessage;
			}
			markImage.sprite = data.IsCorrect ? correctSprite : wrongSprite;
		}

		public void Show (float duration, float delay = 0f) {
			this.transform.DOKill ();
			if (!this.gameObject.activeSelf) {
				this.gameObject.SetActive (true);
			}
			this.transform.localScale = Vector3.zero;
			this.transform.DOScale (1f, duration)
				.SetEase (Ease.OutBack)
				.SetDelay (delay);
			Audio.PlayOneShot ("event:/BubbleAppear");
		}

		public void Hide (float duration, float delay = 0f) {
			this.transform.DOKill ();
			this.transform.DOScale (0f, duration)
				.SetEase (Ease.InBack)
				.SetDelay (delay)
				.OnComplete (() => {
					this.gameObject.SetActive (false);
				});
			Audio.PlayOneShot ("event:/BubbleDisappear");
		}

		public void ShowMarkImage (float duration) {
			if (!markImage.gameObject.activeSelf) {
				markImage.gameObject.SetActive (true);
			}
			RectTransform rect = markImage.transform as RectTransform;
			rect.localScale = Vector3.zero;
			rect.DOKill ();
			rect.DOScale (1f, duration)
				.SetEase (Ease.OutBounce);
			if (data.IsCorrect) {
				Audio.PlayOneShot ("event:/Correct");
				Audio.PlayOneShot ("event:/Clap");
			} else {
				Audio.PlayOneShot ("event:/Wrong");
			}
		}

		public void HideMarkImage () {
			RectTransform rect = markImage.transform as RectTransform;
			rect.DOKill ();
			markImage.gameObject.SetActive (false);
		}

		public void Select () {
			ShowMarkImage (1f);
			isAlreadySelected = true;
			interactableObject.State = InteractableObject.InteractableUIState.Off;
			if (!data.IsCorrect) {
				SetSelectedColor (true);
			}
		}

		public void DebugEnterInteractable () {
			interactableObject.OnPointerEnter ();
		}

		public void DebugClickInteractable () {
			interactableObject.OnPointerClick ();
		}
	}
}
