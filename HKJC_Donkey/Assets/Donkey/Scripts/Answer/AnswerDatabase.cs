﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HKJC.Donkey.Answer {
	public class AnswerDatabase : ScriptableObject {
		public IntListOfAnswerData Datas = new IntListOfAnswerData ();
	}

	[Serializable]
	public class AnswerDataStorage : SerializableDictionary.Storage<List<AnswerData>> { }
	[Serializable]
	public class IntListOfAnswerData : SerializableDictionary<int, List<AnswerData>, AnswerDataStorage> { }
}