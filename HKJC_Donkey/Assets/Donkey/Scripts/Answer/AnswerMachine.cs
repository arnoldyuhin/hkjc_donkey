﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GamestryLab.Debugger;
using GamestryLab.Operations;
using HKJC.Donkey.Dialogue;
using HKJC.Donkey.Props;
using UnityEngine;

namespace HKJC.Donkey.Answer {
	public class AnswerMachine : MonoBehaviour {
		[SerializeField] private List<AnswerBox> answerBoxes = new List<AnswerBox> ();

		[SerializeField] private AnswerDatabase answerDatabase;
		[SerializeField] private DialogueBox dialogueBox;

		[SerializeField] private IntParameter roundParam;

		private AnswerBox selectedBox;

		[SerializeField] private BoolParameter IsAnswerSelectedParam;
		[SerializeField] private BoolParameter IsAnswerCorrectParam;

		[SerializeField] private Transform showOffTransform;

		[Header ("Props")]
		[SerializeField] private PropsMaster propsMaster;

		public string SelectedAnswerId;

		private void Update () {
#if UNITY_EDITOR
			if (Input.GetKeyDown (KeyCode.Alpha1)) {
				answerBoxes [0].DebugEnterInteractable ();
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) {
				answerBoxes [1].DebugEnterInteractable ();
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) {
				answerBoxes [2].DebugEnterInteractable ();
			}
			if (Input.GetKeyUp (KeyCode.Alpha1)) {
				answerBoxes [0].DebugClickInteractable ();
			}
			if (Input.GetKeyUp (KeyCode.Alpha2)) {
				answerBoxes [1].DebugClickInteractable ();
			}
			if (Input.GetKeyUp (KeyCode.Alpha3)) {
				answerBoxes [2].DebugClickInteractable ();
			}
#endif
		}

		public void ResetBoxes () {
			selectedBox = null;
			for (int i = 0; i < answerBoxes.Count; i++) {
				////old version
				//answerBoxes [i].IsAlreadySelected = false;
				//answerBoxes [i].HideMarkImage ();
				//answerBoxes [i].transform.SetParent (this.transform);
				//answerBoxes [i].transform.localScale = Vector3.one;
				//answerBoxes [i].ResetPosition ();

				answerBoxes [i].Reset ();
			}
		}

		public void SetupAnswers () {
			int round = roundParam.Value;
			List<AnswerData> answerDatas = new List<AnswerData> (answerDatabase.Datas [round]);
			if (answerDatas.Count == 0) return;
			answerDatas = new List<AnswerData> (GamestryLab.System.Random.RandomRearrange<AnswerData> (answerDatas));
			for (int i = 0; i < answerDatas.Count; i++) {
				answerBoxes [i].SetAnswerData (answerDatas [i]);
				answerBoxes [i].SetSelectedColor (false);
				answerBoxes [i].State = AnswerBox.AnswerBoxState.Off;
			}
		}

		public void ShowAnswers () {
			float showDuration = 0.5f;
			float showDelay = 0.2f;
			for (int i = 0; i < answerBoxes.Count; i++) {
				answerBoxes [i].Show (showDuration, showDelay * i);
			}
		}

		public void HideAnswers () {
			float hideDuration = 0.5f;
			float delay = 0.5f;
			for (int i = 0; i < answerBoxes.Count; i++) {
				//if (selectedBox == answerBoxes [i]) {
				//	answerBoxes [i].Hide (hideDuration, delay);
				//}
				//answerBoxes [i].Hide (hideDuration);


				//if (answerBoxes [i].Data.IsCorrect) continue;
				if (answerBoxes [i] == selectedBox) continue;
				answerBoxes [i].Hide (hideDuration);
			}

			//props
			propsMaster.ClearProps ();
		}

		public void ForceHideAllAnswers () {
			float hideDuration = 0.5f;
			for (int i = 0; i < answerBoxes.Count; i++) {
				answerBoxes [i].Hide (hideDuration);
			}
		}

		public void EnableBoxesInteract (bool flag) {
			//depreciated
			for (int i = 0; i < answerBoxes.Count; i++) {
				answerBoxes [i].EnableInteract (flag);
			}
		}

		public void SelectAnswer (AnswerBox box) {
			if (box.IsAlreadySelected) return;
			//EnableBoxesInteract (false);
			box.Select ();
			IsAnswerSelectedParam.Value = true;
			AnswerData data = box.Data;
			IsAnswerCorrectParam.Value = data.IsCorrect;
			selectedBox = box;
			SelectedAnswerId = data.Id;
		}

		public void ShowSelectedDialogue () {
			//if (selectedBox == null) return;
			//dialogueBox.SetDialogue (selectedBox.Data.Message);
			float showDuration = 1f;
			dialogueBox.Show (showDuration);
		}

		public void HideSelectedDialogue () {
			float showDuration = 1f;
			dialogueBox.Hide (showDuration);
		}

		public void SendCorrectAnswerToShow () {
			float sendDuration = 1f;
			selectedBox.transform.SetParent (showOffTransform, true);
			selectedBox.transform.DOKill ();
			selectedBox.transform.DOLocalMove (Vector3.zero, sendDuration)
				.SetEase (Ease.OutQuad);
			selectedBox.transform.DOLocalRotate (Vector3.zero, sendDuration)
				.SetEase (Ease.OutQuad);
			selectedBox.transform.DOScale (Vector3.one, sendDuration)
				.SetEase (Ease.OutQuad);
		}

		public void HideCorrectAnswer () {
			float hideDuration = 1f;
			selectedBox.Hide (hideDuration);
		}

		#region props related

		public void SetUpPropsAndAnswers () {
			propsMaster.SpawnProps ();
			propsMaster.SetUpAnswerBoxes (answerBoxes);
		}

		public void ClearAndResetProps () {

		}

		#endregion

	}
}
