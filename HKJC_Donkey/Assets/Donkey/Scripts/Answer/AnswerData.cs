﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HKJC.Donkey.Answer {
	public class AnswerData : ScriptableObject {
		public string Id;
		public string Message;
		public string EnglishMessage;
		public bool IsCorrect = false;
	}
}