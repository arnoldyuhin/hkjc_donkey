﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class BGMFader : MonoBehaviour {

	[SerializeField] private StudioEventEmitter BGMEmitter;
	[SerializeField] private float fadeOutVolume = 0.2f;
	[SerializeField] private float normalVolume = 1f;
	private Coroutine fadeCoroutine;

	public void FadeOutBGM (float duration) {
		BGMEmitter.EventInstance.setVolume (fadeOutVolume);
		if (fadeCoroutine != null) {
			StopCoroutine(fadeCoroutine);
			fadeCoroutine = null;
		}
		fadeCoroutine = StartCoroutine(FadeOutBGMProcedure (duration));
	}

	private IEnumerator FadeOutBGMProcedure(float duration) {
		float currentTime = 0f;
		float startVolume = fadeOutVolume;
		float currentVolume = startVolume;
		BGMEmitter.EventInstance.setVolume (startVolume);
		while (currentTime < duration) {
			currentTime += Time.deltaTime;
			if (currentTime > duration * 0.5f) {
				currentVolume = startVolume + (normalVolume - startVolume) * ((currentTime - duration * 0.5f) / (duration * 0.5f));
				BGMEmitter.EventInstance.setVolume (currentVolume);
			}
			yield return null;
		}
		BGMEmitter.EventInstance.setVolume (normalVolume);
		fadeCoroutine = null;
	}
}
