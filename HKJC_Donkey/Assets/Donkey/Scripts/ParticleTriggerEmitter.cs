﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HKJC.Donkey {
	public class ParticleTriggerEmitter : MonoBehaviour {

		public UnityEvent Triggered;

		private void Update () {
			if (Input.anyKeyDown) {
				Triggered.Invoke ();
			}
		}

		private void OnParticleTrigger () {
			Triggered?.Invoke();
		}
	}
}