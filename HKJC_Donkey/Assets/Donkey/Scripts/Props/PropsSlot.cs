﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GamestryLab.Audio;
using GamestryLab.Utilities;
using HKJC.Donkey.Answer;
using UnityEngine;

namespace HKJC.Donkey.Props {
	public class PropsSlot : MonoBehaviour {
		[SerializeField] private List<PropsObject> availablePropsPrefabs = new List<PropsObject> ();
		[SerializeField] private Transform answerShowOffTransform;
		[SerializeField] private ParticleSystem smokeParticle;

		private AnswerBox answerBox;
		private PropsObject props;

		private AudioPlayer audioplayer;
		public AudioPlayer Audio {
			get {
				if (audioplayer == null) {
					audioplayer = ServiceLocator.GetService<AudioPlayer> () as AudioPlayer;
				}
				return audioplayer;
			}
		}

		private bool IsObjectExist {
			get { return props != null; }
		}

		public void SpawnObject () {
			props = Instantiate<PropsObject> (GamestryLab.System.Random.RandomFromList<PropsObject> (availablePropsPrefabs));
			props.transform.SetParent (this.transform);
			props.transform.localPosition = Vector3.zero;
			props.transform.localEulerAngles = Vector3.zero;
			props.Spawn ();
			props.OnHit += ShowAnswer;
			smokeParticle.Play ();
		}

		public void SetUpAnswer (AnswerBox box) {
			answerBox = box;
			answerBox.transform.SetParent (props.AnswerTransform);
			answerBox.gameObject.SetActive (true);
			answerBox.transform.localPosition = Vector3.zero;
			answerBox.transform.localEulerAngles = Vector3.zero;
			answerBox.transform.localScale = Vector3.one;
			answerBox.Reset ();
		}

		private void ShowAnswer () {
			smokeParticle.Play ();
			props.OnHit -= ShowAnswer;
			if (answerBox == null) return;
			answerBox.transform.SetParent (answerShowOffTransform, true);
			float moveDuration = 1f;
			answerBox.transform.DOScale (1f, moveDuration)
				.SetEase(Ease.OutBack);
			answerBox.transform.DOLocalRotate (Vector3.zero, moveDuration)
				.SetEase (Ease.OutQuad);
			answerBox.transform.DOLocalMove (Vector3.zero, moveDuration)
				.SetEase (Ease.OutQuad)
				.OnComplete (() => {
					answerBox.MoveToOrigin (1f);
				});
			Audio.PlayOneShot ("event:/BubbleAppear");
		}

		public void DestroyObject () {
			if (!IsObjectExist) return;
			props.Destroy ();
			props = null;
			answerBox = null;
		}
	}
}