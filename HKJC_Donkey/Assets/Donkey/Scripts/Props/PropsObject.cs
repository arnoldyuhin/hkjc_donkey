﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace HKJC.Donkey.Props {
	public class PropsObject : MonoBehaviour {

		public enum ObjectState { Off, Ready, Hit }
		private ObjectState state = ObjectState.Off;
		public ObjectState State {
			get { return state; }
			set {
				state = value;
				switch (state) {
				case ObjectState.Ready:
					rBody.constraints = RigidbodyConstraints.FreezeAll;
					break;
				case ObjectState.Hit:
					rBody.constraints = RigidbodyConstraints.None;
					break;
				}
			}
		}

		[SerializeField] private Transform answerTransform;
		public Transform AnswerTransform {
			get { return answerTransform; }
		}
		[SerializeField] private Rigidbody rBody;
		[SerializeField] private float hitPower = 200f;

		public Action OnHit;

		public void Hit () {
			if (State != ObjectState.Ready) return;
			State = ObjectState.Hit;
			Vector3 hitDirection = UnityEngine.Random.onUnitSphere * hitPower;
			hitDirection.y = Mathf.Abs (hitDirection.y);
			rBody.AddForce (hitDirection, ForceMode.Force);
			OnHit?.Invoke ();
		}

		public void Spawn () {
			State = ObjectState.Off;
			this.transform.DOKill ();
			this.transform.localScale = Vector3.zero;
			float spawnDuration = 0.5f;
			this.transform.DOScale (1f, spawnDuration)
				.SetEase (Ease.OutQuad)
				.OnComplete (() => {
					State = ObjectState.Ready;
				});
		}

		public void Destroy () {
			float destroyDuration = 0.5f;
			State = ObjectState.Off;
			this.transform.DOKill ();
			this.transform.DOScale (0f, destroyDuration)
				.SetEase (Ease.InBack)
				.OnComplete (() => {

				});
		}

	}
}