﻿using System.Collections;
using System.Collections.Generic;
using HKJC.Donkey.Answer;
using UnityEngine;

namespace HKJC.Donkey.Props {
	public class PropsMaster : MonoBehaviour {
		[SerializeField] private List<PropsSlot> slots = new List<PropsSlot> ();
		[SerializeField] private int spawnAmount = 6;

		private List<PropsSlot> slotsWithProps = new List<PropsSlot> ();


		public void SpawnProps () {
			slotsWithProps.Clear ();
			slotsWithProps = GamestryLab.System.Random.RandomAmountFromList<PropsSlot> (slots, spawnAmount);
			for (int i = 0; i < slotsWithProps.Count; i++) {
				slotsWithProps [i].SpawnObject ();
			}
		}

		public void SetUpAnswerBoxes (List<AnswerBox> answerBoxes) {
			List<PropsSlot> answerSlots = GamestryLab.System.Random.RandomAmountFromList<PropsSlot> (slotsWithProps, answerBoxes.Count);
			for (int i = 0; i < answerSlots.Count; i++) {
				answerSlots [i].SetUpAnswer (answerBoxes [i]);
			}
		}

		public void ClearProps () {
			for (int i = 0; i < slotsWithProps.Count; i++) {
				slotsWithProps [i].DestroyObject ();
			}
		}
	}
}