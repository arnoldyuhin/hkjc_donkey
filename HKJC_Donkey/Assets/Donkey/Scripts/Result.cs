﻿using System.Collections;
using System.Collections.Generic;
using HKJC.Donkey.Answer;
using UnityEngine;
using UnityEngine.UI;

namespace HKJC.Donkey {
    public class Result : MonoBehaviour {
        [SerializeField] private Image resultImage;
        [SerializeField] private AnswerMachine answerMachine;
        [SerializeField] private Sprite resultASprite;
        [SerializeField] private Sprite resultBSprite;
        [SerializeField] private Sprite resultCSprite;
		[SerializeField] private Sprite resultAEnglishSprite;
        [SerializeField] private Sprite resultBEnglishSprite;
        [SerializeField] private Sprite resultCEnglishSprite;

        public void SetResultImage() {
			switch (answerMachine.SelectedAnswerId) {
            case "A":
				if (!LanguageMaster.Instance.IsEnglish) {
					resultImage.sprite = resultASprite;
				} else {
                    resultImage.sprite = resultAEnglishSprite;
                }
                break;
            case "B":
                if (!LanguageMaster.Instance.IsEnglish) {
                    resultImage.sprite = resultBSprite;
                } else {
                    resultImage.sprite = resultBEnglishSprite;
                }
                break;
            case "C":
                if (!LanguageMaster.Instance.IsEnglish) {
                    resultImage.sprite = resultCSprite;
                } else {
                    resultImage.sprite = resultCEnglishSprite;
                }
                break;
            }
		}

    }
}