﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HKJC.Donkey {
	public class VRTester : MonoBehaviour {

		[SerializeField] private bool isEnabled = true;

		[SerializeField] private float height = 1.5f;
		[SerializeField] private float moveSpeed = 3f;
		[SerializeField] private float rotateSpeed = 5f;

		[SerializeField] private GameObject controller;
		[SerializeField] private Transform controllerTransform;

		private void Start () {
			if (!isEnabled) return;
			this.transform.localPosition += Vector3.up * height;
			Cursor.lockState = CursorLockMode.Locked;
			PlaceController ();
		}

		private void Update () {
#if UNITY_EDITOR
			if (!isEnabled) return;
			Move ();
			Rotate ();
#endif
		}

		private void Move () {
			Vector3 direction = Vector3.zero;
			if (Input.GetKey (KeyCode.W)) {
				direction += transform.forward;
			}
			if (Input.GetKey (KeyCode.S)) {
				direction += -transform.forward;
			}
			if (Input.GetKey (KeyCode.A)) {
				direction += -transform.right;
			}
			if (Input.GetKey (KeyCode.D)) {
				direction += transform.right;
			}
			if (Input.GetKey (KeyCode.Space)) {
				direction += transform.up;
			}
			if (Input.GetKey (KeyCode.X)) {
				direction += -transform.up;
			}
			this.transform.position += moveSpeed * Time.deltaTime * direction;
		}

		private void Rotate () {
			Vector3 lookOffset = new Vector3 (-Input.GetAxis ("Mouse Y"), Input.GetAxis ("Mouse X"), 0f);
			this.transform.localEulerAngles += rotateSpeed * Time.deltaTime * lookOffset;
		}

		private void PlaceController () {
			controller.transform.SetParent (controllerTransform);
			controller.transform.localPosition = Vector3.zero;
			controller.transform.localEulerAngles = Vector3.zero;
			controller.transform.localScale = Vector3.one;
		}
	}
}