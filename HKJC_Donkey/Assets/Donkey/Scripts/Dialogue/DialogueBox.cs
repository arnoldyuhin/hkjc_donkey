﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using GamestryLab.Audio;
using GamestryLab.Utilities;

namespace HKJC.Donkey {
	public class DialogueBox : MonoBehaviour {
		[SerializeField] private TextMeshProUGUI dialogueText;

		private AudioPlayer audioplayer;
		public AudioPlayer Audio {
			get {
				if (audioplayer == null) {
					audioplayer = ServiceLocator.GetService<AudioPlayer> () as AudioPlayer;
				}
				return audioplayer;
			}
		}

		public void SetDialogue (string message) {
			dialogueText.text = message;
		}

		public void Show (float duration) {
			this.transform.DOKill ();
			if (!this.gameObject.activeSelf) {
				this.gameObject.SetActive (true);
			}
			this.transform.localScale = Vector3.zero;
			this.transform.DOScale (1f, duration)
				.SetEase (Ease.OutBack);
			Audio.PlayOneShot ("event:/BubbleAppear");
		}

		public void Hide (float duration) {
			this.transform.DOKill ();
			this.transform.DOScale (0f, duration)
				.SetEase (Ease.InBack)
				.OnComplete (() => {
					this.gameObject.SetActive (false);
				});
			Audio.PlayOneShot ("event:/BubbleDisappear");
		}
	}
}