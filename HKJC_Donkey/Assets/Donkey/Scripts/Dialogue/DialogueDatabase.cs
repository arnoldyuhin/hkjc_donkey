﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HKJC.Donkey.Dialogue {
	public class DialogueDatabase : ScriptableObject {
		public List<DialogueData> Datas = new List<DialogueData> ();

		public DialogueData GetData (string dialogueId) {
			return Datas.FirstOrDefault (d => d.Id == dialogueId);
		}
	}
}