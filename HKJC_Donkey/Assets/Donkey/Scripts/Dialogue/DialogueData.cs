﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HKJC.Donkey.Dialogue {
	public class DialogueData : ScriptableObject {
		public string Id;
		public string Message;
		public string EnglishMessage;
		public string AnimationClip;
		public string SoundId;
		public string ReactAnimationClip;
	}
}
