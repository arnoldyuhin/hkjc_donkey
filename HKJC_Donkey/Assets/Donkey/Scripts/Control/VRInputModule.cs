﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;

namespace HKJC.Donkey.Control {
	public class VRInputModule : BaseInputModule {
		//[SerializeField] private Camera pointerCamera;

		[SerializeField] private List<Camera> pointerCameras = new List<Camera> ();
		[SerializeField] private List<Canvas> pointableCanvases = new List<Canvas> ();

		[SerializeField] private SteamVR_Input_Sources targetSource;
		[SerializeField] private SteamVR_Action_Boolean clickAction;

		private GameObject currentObject = null;
		private PointerEventData pointerData = null;

		protected override void Awake () {
			base.Awake ();
			pointerData = new PointerEventData (eventSystem);

		}

		public override void Process () {
			//for (int i = 0; i < pointerCameras.Count; i++) {
			//	//set canvas to listen specific camera
			//	for (int j = 0; j < pointableCanvases.Count; j++) {
			//		pointableCanvases [j].worldCamera = pointerCameras [i];
			//	}
			Camera pointerCamera = pointerCameras [0];
				//reset data, set camera
				pointerData.Reset ();
				pointerData.position = new Vector2 (pointerCamera.pixelWidth / 2, pointerCamera.pixelHeight / 2);
				//raycast
				eventSystem.RaycastAll (pointerData, m_RaycastResultCache);
				pointerData.pointerCurrentRaycast = FindFirstRaycast (m_RaycastResultCache);
				currentObject = pointerData.pointerCurrentRaycast.gameObject;
				//clear
				m_RaycastResultCache.Clear ();
				//hover
				HandlePointerExitAndEnter (pointerData, currentObject);
				//press
				if (clickAction.GetStateDown (targetSource)) {
					ProcessPress (pointerData);
				}
				if (clickAction.GetStateUp (targetSource)) {
					ProcessRelease (pointerData);
				}
			//}

			////reset data, set camera
			//pointerData.Reset ();
			//pointerData.position = new Vector2 (pointerCamera.pixelWidth / 2, pointerCamera.pixelHeight / 2);
			////raycast
			//eventSystem.RaycastAll (pointerData, m_RaycastResultCache);
			//pointerData.pointerCurrentRaycast = FindFirstRaycast (m_RaycastResultCache);
			//currentObject = pointerData.pointerCurrentRaycast.gameObject;
			////clear
			//m_RaycastResultCache.Clear ();
			////hover
			//HandlePointerExitAndEnter (pointerData, currentObject);
			////press
			//if (clickAction.GetStateDown (targetSource)) {
			//	ProcessPress (pointerData);
			//}
			//if (clickAction.GetStateUp (targetSource)) {
			//	ProcessRelease (pointerData);
			//}
		}

		public PointerEventData GetData () {
			return pointerData;
		}

		private void ProcessPress (PointerEventData data) {
			//set raycast
			data.pointerPressRaycast = data.pointerCurrentRaycast;
			//check for object hit, get the down handler, call
			GameObject newPointerPress = ExecuteEvents.ExecuteHierarchy (currentObject, data, ExecuteEvents.pointerDownHandler);

			//If no down handler, try and get click handler
			if (newPointerPress == null) {
				newPointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler> (currentObject);
			}
			//set data
			data.pressPosition = data.position;
			data.pointerPress = newPointerPress;
			data.rawPointerPress = currentObject;
		}

		private void ProcessRelease (PointerEventData data) {
			//execute pointer up
			ExecuteEvents.Execute (data.pointerPress, data, ExecuteEvents.pointerUpHandler);
			//check for click handler
			GameObject pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler> (currentObject);
			//check if actual
			if (data.pointerPress == pointerUpHandler) {
				ExecuteEvents.Execute (data.pointerPress, data, ExecuteEvents.pointerClickHandler);
			}
			//clear selected gameobject
			eventSystem.SetSelectedGameObject (null);
			//reset data
			data.pressPosition = Vector2.zero;
			data.pointerPress = null;
			data.rawPointerPress = null;
		}


	}
}