﻿using System.Collections;
using System.Collections.Generic;
using HKJC.Donkey.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace HKJC.Donkey.Control {
	public class Pointer : MonoBehaviour {
		[SerializeField] private float defaultLength = 5f;
		[SerializeField] private GameObject origin;
		[SerializeField] private GameObject dot;
		[SerializeField] private LineRenderer lineRenderer;

		[SerializeField] private VRInputModule inputModule;

		private InteractableObject pointingObject;

		private void Update () {
			PointerEventData data = inputModule.GetData ();
			float targetLength = data.pointerCurrentRaycast.distance == 0 ? defaultLength : data.pointerCurrentRaycast.distance;
			//raycast
			RaycastHit hit = CreateRaycast (targetLength);
			//Arnold - process 3D raycasthit
			if (hit.collider != null) {
				ProcessRaycast (hit);
			} else {
				if (pointingObject != null) {
					pointingObject.OnPointerExit ();
					pointingObject = null;
				}
			}
			//default
			Vector3 endPos = this.transform.position + this.transform.forward * targetLength;
			//or based on hit
			if (hit.collider != null) {
				endPos = hit.point;
			}
			//set position of the dot
			dot.transform.position = endPos;
			//set linerenderer
			lineRenderer.SetPosition (0, origin.transform.position);
			lineRenderer.SetPosition (1, endPos);
		}

		private RaycastHit CreateRaycast (float length) {
			RaycastHit hit;
			Ray ray = new Ray (this.transform.position, this.transform.forward);
			Physics.Raycast (ray, out hit, length);
			return hit;
		}

		private void ProcessRaycast (RaycastHit hit) {
			InteractableObject obj = hit.collider.GetComponent<InteractableObject> ();
			if (obj == null) {
				if (pointingObject != null) {
					pointingObject.OnPointerExit ();
				}
				pointingObject = null;
				return;
			}
			if (pointingObject == obj) {
				if (!pointingObject.IsPointerHover) {
					pointingObject.OnPointerEnter ();
					return;
				}
			}
			if (pointingObject == null) {
				pointingObject = obj;
				pointingObject.OnPointerEnter ();
				return;
			}
			if (pointingObject != obj) {
				pointingObject.OnPointerExit ();
				pointingObject = obj;
				pointingObject.OnPointerEnter ();
			}
		}
	}
}