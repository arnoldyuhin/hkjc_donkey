﻿using System.Collections;
using System.Collections.Generic;
using HKJC.Donkey.Objects;
using UnityEngine;
using Valve.VR;
using static HKJC.Donkey.Objects.Carrot;
using UnityEngine.Events;

namespace HKJC.Donkey.Control {
	public class CarrotShooter : MonoBehaviour {

		[SerializeField] private bool IsEnable = false;
		[SerializeField] private SteamVR_Input_Sources source;
		[SerializeField] private List<SteamVR_Action_Boolean> clickActions = new List<SteamVR_Action_Boolean> ();
		[SerializeField] private Pointer pointer;
		[SerializeField] private SingleObjectPool carrotPool;

		[SerializeField] private float shootingPower = 1f;

		[SerializeField] private Transform carrotTransform;

		private Carrot currentCarrot;

		public UnityEvent CarrotShot;


		private void Update () {
			for (int i = 0; i < clickActions.Count; i++) {
				if (clickActions [i].GetStateDown (source)) {
					Click ();
				}
			}
#if UNITY_EDITOR
			if (Input.GetMouseButtonDown (0)) {
				Click ();
			}
#endif
			SpawnCurrentCarrot ();
		}

		public void Enable (bool flag) {
			IsEnable = flag;
			if (flag == false) {
				if (currentCarrot != null) {
					currentCarrot.Disappear ();
					currentCarrot = null;
				}
			}
		}

		private void SpawnCurrentCarrot () {
			if (!IsEnable) return;
			if (currentCarrot != null) return;
			currentCarrot = carrotPool.GetObject () as Carrot;
			currentCarrot.transform.SetParent (carrotTransform);
			currentCarrot.transform.localPosition = Vector3.zero;
			currentCarrot.transform.localEulerAngles = Vector3.zero;
			currentCarrot.transform.localScale = Vector3.zero;
			currentCarrot.CanEmitParticle = false;	
			currentCarrot.Spawn ();
		}

		private void Click () {
			if (!IsEnable) return;
			if (currentCarrot.State != CarrotState.Ready) return;
			currentCarrot.transform.SetParent (null, true);
			currentCarrot.Activate ();
			currentCarrot.CanEmitParticle = true;
			currentCarrot.Rigidbody.AddForce (pointer.transform.forward * shootingPower);
			currentCarrot = null;
			CarrotShot?.Invoke ();
		}
	}
}