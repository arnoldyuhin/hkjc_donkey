﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace HKJC.Donkey.UI {
	public class InteractableUI : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerClickHandler {

		public enum InteractableUIState { Idle, Hover, Down, Click }
		private InteractableUIState state;
		public InteractableUIState State {
			get { return state; }
			set {
				state = value;
				switch (state) {
				case InteractableUIState.Idle:
					this.transform.DOKill ();
					this.transform.DOScale (normalScale, shrinkDuration)
						.SetEase (Ease.InQuad);
					Unhovered?.Invoke ();
					break;
				case InteractableUIState.Hover:
					this.transform.DOKill ();
					this.transform.DOScale (hoverScale, hoverDuration)
						.SetEase (Ease.OutQuad);
					Hovered?.Invoke ();
					break;
				case InteractableUIState.Down:
					this.transform.DOKill ();
					this.transform.DOScale (downScale, downDuration)
						.SetEase (Ease.OutQuad);
					Hovered?.Invoke ();
					break;
				case InteractableUIState.Click:
					this.transform.DOKill ();
					float scaleUpDuration = clickDuration * 0.2f;
					float scaleDownDuration = clickDuration * 0.8f;
					this.transform.DOScale (clickScale, scaleUpDuration)
						.SetEase (Ease.OutQuad);
					this.transform.DOScale (normalScale, scaleDownDuration)
						.SetEase (Ease.InQuad)
						.SetDelay (scaleUpDuration)
						.OnComplete (() => {
							if (isPointerHover) {
								if (isPointerDown) {
									State = InteractableUIState.Down;
								} else {
									State = InteractableUIState.Hover;
								}
							} else {
								State = InteractableUIState.Idle;
							}
							Clicked?.Invoke ();
						});
					break;
				}
			}
		}


		[Header ("Normal")]
		[SerializeField] private float shrinkDuration = 0.2f;
		[SerializeField] private float normalScale = 1f;

		[Header ("Hover")]
		[SerializeField] private float hoverDuration = 0.2f;
		[SerializeField] private float hoverScale = 1.2f;

		[Header ("Down")]
		[SerializeField] private float downDuration = 0.2f;
		[SerializeField] private float downScale = 1.35f;

		[Header ("Click")]
		[SerializeField] private float clickDuration = 0.3f;
		[SerializeField] private float clickScale = 1.5f;

		public UnityEvent Unhovered;
		public UnityEvent Hovered;
		public UnityEvent Clicked;

		private bool isPointerHover = false;
		private bool isPointerDown = false;

		public void OnPointerEnter (PointerEventData eventData) {
			Debug.Log ("PointerEnter");
			isPointerHover = true;
			if (State == InteractableUIState.Click) return;
			if (isPointerDown) {
				State = InteractableUIState.Down;
			} else {
				State = InteractableUIState.Hover;
			}
		}

		public void OnPointerExit (PointerEventData eventData) {
			Debug.Log ("PointerExit");
			isPointerHover = false;
			if (State == InteractableUIState.Click) return;
			State = InteractableUIState.Idle;
		}

		public void OnPointerDown (PointerEventData eventData) {
			Debug.Log ("PointerDown");
			isPointerDown = true;
			if (State == InteractableUIState.Click) return;
			State = InteractableUIState.Down;
		}

		public void OnPointerUp (PointerEventData eventData) {
			Debug.Log ("PointerUp");
			isPointerDown = false;
			if (State == InteractableUIState.Click) return;
		}

		public void OnPointerClick (PointerEventData eventData) {
			Debug.Log ("PointerClick");
			if (State == InteractableUIState.Click) return;
			State = InteractableUIState.Click;
		}
	}
}
