﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using HighlightingSystem;

namespace HKJC.Donkey.UI {
	public class InteractableObject : MonoBehaviour {

		[SerializeField] private Vector3 hoverOffset = new Vector3 (0f, 0.5f, 0.4f);
		[SerializeField] private Collider collider;
		private Vector3 originalPos = Vector3.zero;

		private Tweener scaleTween;
		private Tweener offsetTween;

		public enum InteractableUIState { Off, Idle, Hover, Down, Click, Disable }
		private InteractableUIState state = InteractableUIState.Off;
		public InteractableUIState State {
			get { return state; }
			set {
				switch (state) {
				case InteractableUIState.Off:
					collider.isTrigger = false;
					break;
				}
				state = value;
				switch (state) {
				case InteractableUIState.Off:
					IsPointerHover = false;
					isPointerDown = false;
					collider.isTrigger = true;
					TurnHighlighterOn (false);
					break;
				case InteractableUIState.Idle:
					scaleTween.Kill ();
					scaleTween = this.transform.DOScale (normalScale, shrinkDuration)
						.SetEase (Ease.InQuad);
					offsetTween.Kill ();
					offsetTween = this.transform.DOLocalMove (originalPos, shrinkDuration)
						.SetEase (Ease.InQuad);
					Unhovered?.Invoke ();
					TurnHighlighterOn (false);
					break;
				case InteractableUIState.Hover:
					scaleTween.Kill ();
					scaleTween = this.transform.DOScale (hoverScale, hoverDuration)
						.SetEase (Ease.OutQuad);
					offsetTween.Kill ();
					offsetTween = this.transform.DOLocalMove (originalPos + hoverOffset, hoverDuration)
						.SetEase (Ease.OutQuad);
					Hovered?.Invoke ();
					TurnHighlighterOn (true);
					break;
				case InteractableUIState.Down:
					scaleTween.Kill ();
					scaleTween = this.transform.DOScale (downScale, downDuration)
						.SetEase (Ease.OutQuad);
					offsetTween.Kill ();
					offsetTween = this.transform.DOLocalMove (originalPos + hoverOffset, downDuration)
						.SetEase (Ease.OutQuad);
					Hovered?.Invoke ();
					break;
				case InteractableUIState.Click:
					scaleTween.Kill ();
					float scaleUpDuration = clickDuration * 0.2f;
					float scaleDownDuration = clickDuration * 0.8f;
					scaleTween = this.transform.DOScale (clickScale, scaleUpDuration)
						.SetEase (Ease.OutQuad);
					scaleTween = this.transform.DOScale (normalScale, scaleDownDuration)
						.SetEase (Ease.InQuad)
						.SetDelay (scaleUpDuration)
						.OnComplete (() => {
							//if (isPointerHover) {
							//	if (isPointerDown) {
							//		State = InteractableUIState.Down;
							//	} else {
							//		State = InteractableUIState.Hover;
							//	}
							//} else {
							if (State != InteractableUIState.Disable) {
								State = InteractableUIState.Idle;
							}
							//}
							Debug.Log ("ClickFinished");
							Clicked?.Invoke ();
						});
					Clicking?.Invoke ();
					offsetTween.Kill ();
					offsetTween = this.transform.DOLocalMove (originalPos, scaleDownDuration)
						.SetEase (Ease.OutQuad)
						.SetDelay (scaleUpDuration)
						.OnComplete (() => {
							this.transform.localPosition = originalPos;
						});
					break;
				case InteractableUIState.Disable:
					IsPointerHover = false;
					isPointerDown = false;
					TurnHighlighterOn (false);
					break;
				}
			}
		}

		[SerializeField] private SteamVR_Input_Sources source;
		[SerializeField] private List<SteamVR_Action_Boolean> clickActions = new List<SteamVR_Action_Boolean> ();


		[Header ("Normal")]
		[SerializeField] private float shrinkDuration = 0.2f;
		[SerializeField] private float normalScale = 1f;

		[Header ("Hover")]
		[SerializeField] private float hoverDuration = 0.2f;
		[SerializeField] private float hoverScale = 1.2f;

		[Header ("Down")]
		[SerializeField] private float downDuration = 0.2f;
		[SerializeField] private float downScale = 1.35f;

		[Header ("Click")]
		[SerializeField] private float clickDuration = 0.3f;
		[SerializeField] private float clickScale = 1.5f;

		public UnityEvent Unhovered;
		public UnityEvent Hovered;
		public UnityEvent Downed;
		public UnityEvent Clicking;
		public UnityEvent Clicked;

		public bool IsPointerHover = false;
		private bool isPointerDown = false;

		[SerializeField] private Highlighter highlighter;

		private void Awake () {
			originalPos = this.transform.localPosition;
		}

		private void Update () {
			if (State == InteractableUIState.Off) return;
			if (State == InteractableUIState.Disable) return;
			if (State == InteractableUIState.Click) return;
			if (!isPointerDown) {
				//not clicking
				if (!IsPointerHover) return; //not pointing
											 //pointing and check click down
				for (int i = 0; i < clickActions.Count; i++) {
					if (clickActions [i].GetStateDown (source)) {
						OnPointerDown ();
					}
				}
			} else {
				//is clicking
				for (int i = 0; i < clickActions.Count; i++) {
					if (clickActions [i].GetStateUp (source)) {
						Debug.Log ("Pointer Up from VR");
						OnPointerUp ();
						if (IsPointerHover) {
							//is hovering
							OnPointerClick ();
						}
					}
				}
			}
#if UNITY_EDITOR
			if (!isPointerDown) {
				if (!IsPointerHover) return;
				if (Input.GetMouseButtonDown (0)) {
					OnPointerDown ();
				}
			} else {
				if (Input.GetMouseButtonUp (0)) {
					OnPointerUp ();
					if (IsPointerHover) {
						OnPointerClick ();
					}
				}
			}
#endif
		}

		public void Enable (bool flag) {
			if (flag) {
				State = InteractableUIState.Idle;
			} else {
				State = InteractableUIState.Off;
			}
		}

		public void OnPointerEnter () {
			//Debug.Log ("PointerEnter");
			if (State == InteractableUIState.Off) return;
			if (State == InteractableUIState.Disable) return;
			IsPointerHover = true;
			if (State == InteractableUIState.Click) return;
			if (isPointerDown) {
				State = InteractableUIState.Down;
			} else {
				State = InteractableUIState.Hover;
			}
		}

		public void OnPointerExit () {
			Debug.Log ("PointerExit");
			if (State == InteractableUIState.Off) return;
			if (State == InteractableUIState.Disable) return;
			IsPointerHover = false;
			if (State == InteractableUIState.Click) return;
			State = InteractableUIState.Idle;
		}

		public void OnPointerDown () {
			Debug.Log ("PointerDown");
			if (State == InteractableUIState.Off) return;
			if (State == InteractableUIState.Disable) return;
			isPointerDown = true;
			if (State == InteractableUIState.Click) return;
			State = InteractableUIState.Down;
		}

		public void OnPointerUp () {
			Debug.Log ("PointerUp");
			if (State == InteractableUIState.Off) return;
			if (State == InteractableUIState.Disable) return;
			if (State == InteractableUIState.Click) return;
			isPointerDown = false;
		}

		public void OnPointerClick () {
			Debug.Log ("PointerClick");
			if (State == InteractableUIState.Off) return;
			if (State == InteractableUIState.Disable) return;
			if (State == InteractableUIState.Click) return;
			State = InteractableUIState.Click;
		}

		private void TurnHighlighterOn (bool flag) {
			if (highlighter == null) return;
			float duration = 0.2f;
			if (flag == true) {
				highlighter.ConstantOn (duration);
			} else {
				highlighter.ConstantOff (duration);
			}
		}
	}
}
