﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HKJC.Donkey {
	public class CameraReset : MonoBehaviour {
		private void Awake () {
			this.transform.localPosition = Vector3.zero;
		}
	}
}