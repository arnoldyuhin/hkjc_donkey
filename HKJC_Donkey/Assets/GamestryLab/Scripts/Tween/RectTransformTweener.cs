﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace GamestryLab.Tween {
	public class RectTransformTweener : MonoBehaviour {
		[Header ("Transforms")]
		[SerializeField] private RectTransform tweenObject;
		[SerializeField] private RectTransform targetTransform;
		[SerializeField] private Vector2 targetVector;
		[Header ("TweenerType")]
		[SerializeField] private bool TweenAnchoredPos;
		[SerializeField] private bool TweenSizeDelta;
		[SerializeField] private bool TweenEulerAngles;
		[SerializeField] private bool TweenScale;
		[SerializeField] private bool PunchScale;
		[SerializeField] private bool TweenAnchorMinMax;
		[Header ("Time")]
		[SerializeField] private float duration;
		[SerializeField] private float delay;
		[Header ("Ease")]
		[SerializeField] private Ease ease;

		[ContextMenu ("StartTween")]
		public void StartTween () {
			tweenObject.DOKill ();
			if (targetTransform != null) {
				if (TweenAnchoredPos) {
					tweenObject.DOAnchorPos (targetTransform.anchoredPosition, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (TweenSizeDelta) {
					tweenObject.DOSizeDelta (targetTransform.sizeDelta, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (TweenEulerAngles) {
					tweenObject.DORotate (targetTransform.eulerAngles, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (TweenScale) {
					tweenObject.DOScale (targetTransform.localScale, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (PunchScale) {
					tweenObject.DOPunchScale (targetTransform.localScale, duration, 2)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (TweenAnchorMinMax) {
					tweenObject.DOAnchorMax (targetTransform.anchorMax, duration)
						.SetDelay (delay)
						.SetEase (ease);
					tweenObject.DOAnchorMin (targetTransform.anchorMin, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
			} else {
				if (TweenAnchoredPos) {
					tweenObject.DOAnchorPos (targetVector, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (TweenSizeDelta) {
					tweenObject.DOSizeDelta (targetVector, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (TweenEulerAngles) {
					tweenObject.DORotate (targetVector, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (TweenScale) {
					tweenObject.DOScale (targetVector, duration)
						.SetDelay (delay)
						.SetEase (ease);
				}
				if (PunchScale) {
					tweenObject.DOPunchScale (targetVector, duration, 2)
						.SetDelay (delay)
						.SetEase (ease);
				}
			}
		}

		[ContextMenu ("EndTween")]
		public void EndTween () {
			tweenObject.DOKill ();
			if (targetTransform != null) {
				if (TweenAnchoredPos) {
					tweenObject.anchoredPosition = targetTransform.anchoredPosition;
				}
				if (TweenSizeDelta) {
					tweenObject.sizeDelta = targetTransform.sizeDelta;
				}
				if (TweenEulerAngles) {
					tweenObject.eulerAngles = targetTransform.eulerAngles;
				}
				if (TweenScale || PunchScale) {
					tweenObject.localScale = targetTransform.localScale;
				}
				if (TweenAnchorMinMax) {
					tweenObject.anchorMin = targetTransform.anchorMin;
					tweenObject.anchorMax = targetTransform.anchorMax;
				}
			} else {
				if (TweenAnchoredPos) {
					tweenObject.anchoredPosition = targetVector;
				}
				if (TweenSizeDelta) {
					tweenObject.sizeDelta = targetVector;
				}
				if (TweenEulerAngles) {
					tweenObject.eulerAngles = targetVector;
				}
				if (TweenScale || PunchScale) {
					tweenObject.localScale = targetVector;
				}
			}
		}

	}
}