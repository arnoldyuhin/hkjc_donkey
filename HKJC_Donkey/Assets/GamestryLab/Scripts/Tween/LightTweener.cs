﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace GamestryLab.Tween {
	public class LightTweener : MonoBehaviour {
		[Header ("Transform")]
		[SerializeField] private Light tweenLight;
		[Header ("TweenerType")]
		[SerializeField] private bool TweenIntensity;
		[SerializeField] private bool TweenColor;
		[Header ("Parameter")]
		[SerializeField] private float intensity;
		[SerializeField] private Color color;

		[Header ("Time")]
		[SerializeField] private float duration;
		[SerializeField] private float delay;
		[Header ("Ease")]
		[SerializeField] private Ease ease;

		private Tweener intensityTween;
		private Tweener colorTween;

		[ContextMenu ("StartTween")]
		public void StartTween () {
			intensityTween.Kill ();
			colorTween.Kill ();
			if (TweenIntensity) {
				intensityTween = tweenLight.DOIntensity (intensity, duration)
					.SetDelay (delay)
					.SetEase (ease);
			}
			if (TweenColor) {
				colorTween = tweenLight.DOColor (color, duration)
					.SetDelay (delay)
					.SetEase (ease);
			}
		}

		[ContextMenu ("EndTween")]
		public void EndTween () {
			intensityTween.Kill ();
			colorTween.Kill ();
			if (TweenIntensity) {
				tweenLight.intensity = intensity;
			}
			if (TweenColor) {
				tweenLight.color = color;
			}
		}
	}
}