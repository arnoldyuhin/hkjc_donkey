﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace GamestryLab.Tween {
	public class RectTransformBackPunchTweener : MonoBehaviour {
		[Header ("Transforms")]
		[SerializeField] private RectTransform tweenObject;
		[SerializeField] private RectTransform targetTransform;
		[SerializeField] private Vector2 targetVector;
		[Header ("TweenerType")]
		[SerializeField] private bool TweenAnchoredPos;
		[SerializeField] private bool TweenSizeDelta;
		[SerializeField] private bool TweenEulerAngles;
		[SerializeField] private bool TweenScale;
		[Header ("Time")]
		[SerializeField] private float duration;
		[SerializeField] private float delay;
		[Header ("Ease")]
		[SerializeField] private Ease ease;
		[Header ("Punch")]
		[SerializeField] private int punchTime = 1;

		[ContextMenu ("StartTween")]
		public void StartTween () {
			tweenObject.DOKill ();
			if (targetTransform != null) {
				if (TweenAnchoredPos) {
					tweenObject.DOAnchorPos (targetTransform.anchoredPosition, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
				if (TweenSizeDelta) {
					tweenObject.DOSizeDelta (targetTransform.sizeDelta, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
				if (TweenEulerAngles) {
					tweenObject.DORotate (targetTransform.eulerAngles, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
				if (TweenScale) {
					tweenObject.DOScale (targetTransform.localScale, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
			} else {
				if (TweenAnchoredPos) {
					tweenObject.DOAnchorPos (targetVector, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
				if (TweenSizeDelta) {
					tweenObject.DOSizeDelta (targetVector, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
				if (TweenEulerAngles) {
					tweenObject.DORotate (targetVector, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
				if (TweenScale) {
					tweenObject.DOScale (targetVector, duration * 0.5f * (1f / punchTime))
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (punchTime * 2, LoopType.Yoyo);
				}
			}
		}

		[ContextMenu ("EndTween")]
		public void EndTween () {
			tweenObject.DOKill ();
			if (targetTransform != null) {
				if (TweenAnchoredPos) {
					tweenObject.anchoredPosition = targetTransform.anchoredPosition;
				}
				if (TweenSizeDelta) {
					tweenObject.sizeDelta = targetTransform.sizeDelta;
				}
				if (TweenEulerAngles) {
					tweenObject.eulerAngles = targetTransform.eulerAngles;
				}
				if (TweenScale) {
					tweenObject.localScale = targetTransform.localScale;
				}
			} else {
				if (TweenAnchoredPos) {
					tweenObject.anchoredPosition = targetVector;
				}
				if (TweenSizeDelta) {
					tweenObject.sizeDelta = targetVector;
				}
				if (TweenEulerAngles) {
					tweenObject.eulerAngles = targetVector;
				}
				if (TweenScale) {
					tweenObject.localScale = targetVector;
				}
			}
		}

	}
}