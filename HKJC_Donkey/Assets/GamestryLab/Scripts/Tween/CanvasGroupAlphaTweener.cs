﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace GamestryLab.Tween {
	public class CanvasGroupAlphaTweener : MonoBehaviour {
		[Header ("CanvasGroup")]
		[SerializeField] private CanvasGroup tweenCanvasGroup;
		[Header ("Alpha")]
		[SerializeField] private float targetAlpha;
		[Header ("Time")]
		[SerializeField] private float duration;
		[SerializeField] private float delay;
		[Header ("Ease")]
		[SerializeField] private Ease ease;

		[ContextMenu ("StartTween")]
		public void StartTween () {
			tweenCanvasGroup.DOKill ();
			tweenCanvasGroup.DOFade (targetAlpha, duration)
				.SetEase (ease)
				.SetDelay (delay);
		}

		[ContextMenu ("EndTween")]
		public void EndTween () {
			tweenCanvasGroup.DOKill ();
			tweenCanvasGroup.alpha = targetAlpha;
		}
	}
}