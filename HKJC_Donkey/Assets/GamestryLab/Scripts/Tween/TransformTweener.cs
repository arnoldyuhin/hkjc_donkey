﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace GamestryLab.Tween {
	public class TransformTweener : MonoBehaviour {
		[Header ("Transforms")]
		[SerializeField] private Transform tweenObject;
		[SerializeField] private Transform targetTransform;
		[SerializeField] private Vector3 targetVector;
		[Header ("TweenerType")]
		[SerializeField] private bool TweenPosition;
		[SerializeField] private bool TweenEulerAngles;
		[SerializeField] private bool TweenScale;
		[Header ("Time")]
		[SerializeField] private float duration;
		[SerializeField] private float delay;
		[Header ("Ease")]
		[SerializeField] private Ease ease;
		[Header ("Loops")]
		[SerializeField] private int loops;
		[SerializeField] private LoopType loopType;

		[ContextMenu ("StartTween")]
		public void StartTween () {
			tweenObject.DOKill ();
			if (targetTransform != null) {
				if (TweenPosition) {
					tweenObject.DOMove (targetTransform.position, duration)
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (loops, loopType);
				}
				if (TweenEulerAngles) {
					tweenObject.DORotate (targetTransform.eulerAngles, duration)
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (loops, loopType);
				}
				if (TweenScale) {
					tweenObject.DOScale (targetTransform.localScale, duration)
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (loops, loopType);
				}
			} else {
				if (TweenPosition) {
					tweenObject.DOMove (targetVector, duration)
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (loops, loopType);
				}
				if (TweenEulerAngles) {
					tweenObject.DORotate (targetVector, duration)
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (loops, loopType);
				}
				if (TweenScale) {
					tweenObject.DOScale (targetVector, duration)
						.SetDelay (delay)
						.SetEase (ease)
						.SetLoops (loops, loopType);
				}
			}
		}

		[ContextMenu ("EndTween")]
		public void EndTween () {
			tweenObject.DOKill ();
			if (targetTransform != null) {
				if (TweenPosition) {
					tweenObject.position = targetTransform.position;
				}
				if (TweenEulerAngles) {
					tweenObject.eulerAngles = targetTransform.eulerAngles;
				}
				if (TweenScale) {
					tweenObject.localScale = targetTransform.localScale;
				}
			} else {
				if (TweenPosition) {
					tweenObject.position = targetVector;
				}
				if (TweenEulerAngles) {
					tweenObject.eulerAngles = targetVector;
				}
				if (TweenScale) {
					tweenObject.localScale = targetVector;
				}
			}
		}

	}
}