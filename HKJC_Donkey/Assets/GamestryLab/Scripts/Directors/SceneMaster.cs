﻿using System;
using System.Collections;
using System.Collections.Generic;
using GamestryLab.Debugger;
using UnityEngine;

public class SceneMaster : CreatedObject {

	public StringPhaseDirectorDictionary phases = new StringPhaseDirectorDictionary ();

	public string startPhase;
	private PhaseDirector currentPhase;

	public override void OnPreload () {
		base.OnPreload ();
		List<string> phaseKeys = new List<string> (phases.Keys);
		for (int i = 0; i < phaseKeys.Count; i++) {
			phases[phaseKeys[i]].OnPreload ();
		}
		// 	bool isPreloading = true;
		// while (isPreloading) {
		// 	isPreloading = false;
		// 	for (int i = 0; i < directors.Length; i++) {
		// 		if (directors[i].IsPreloading ()) {
		// 			isPreloading = true;
		// 			break;
		// 		}
		// 	}
		// 	yield return null;
		// }
	}

	public override void OnInitialize () {
		base.OnInitialize ();
		if (startPhase == null) {
			DebugLog.Log (Color.red, "Start phase is missing.");
			return;
		}
		if (!phases.ContainsKey (startPhase)) {
			DebugLog.Log (Color.red, "Start phase id is wrong.");
			return;
		}
		ChangePhase (startPhase);

	}

	public override void OnFixedUpdate () {
		base.OnFixedUpdate ();
		if (currentPhase == null) {
			DebugLog.Log (Color.red, "Current phase is missing.");
			return;
		}
		currentPhase.OnFixedUpdate ();
	}

	public override void OnUpdate () {
		base.OnUpdate ();
		if (currentPhase == null) {
			DebugLog.Log (Color.red, "Current phase is missing.");
			return;
		}
		currentPhase.OnUpdate ();
	}

	public override void OnTickUpdate () {
		base.OnTickUpdate ();
		if (startPhase == null) {
			DebugLog.Log (Color.red, "Current phase is missing.");
			return;
		}
		currentPhase.OnTickUpdate ();
	}

	public override void OnTerminate () {
		base.OnTerminate ();
		if (startPhase == null) {
			DebugLog.Log (Color.red, "Current phase is missing.");
			return;
		}
		currentPhase.OnTerminate ();
		List<string> phaseKeys = new List<string> (phases.Keys);
		for (int i = 0; i < phaseKeys.Count; i++) {
			phases[phaseKeys[i]].OnUnload ();
		}
	}

	public void ChangePhase (PhaseDirector _phase) {
		if (currentPhase != null) {
			currentPhase.OnTerminate ();
		}
		currentPhase = _phase;
		currentPhase.OnInitialize ();
		DebugLog.Log ("{0}: Phase changed to {1}.", GetDebugName (), currentPhase.GetType ().ToString ());
	}

	public void ChangePhase (string _phaseID) {
		if (!phases.ContainsKey (_phaseID)) {
			DebugLog.Log (Color.red, "Phase with ID: {0} is missing.", _phaseID);
			return;
		}
		if (currentPhase != null) {
			currentPhase.OnTerminate ();
		}
		currentPhase = phases[_phaseID];
		currentPhase.OnInitialize ();
		DebugLog.Log ("{0}: Phase changed to {1}.", GetDebugName (), currentPhase.GetType ().ToString ());
	}

}

[Serializable]
public class StringPhaseDirectorDictionary : SerializableDictionary<string, PhaseDirector> { };