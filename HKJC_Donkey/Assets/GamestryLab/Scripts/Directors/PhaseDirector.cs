﻿using System;
using System.Collections;
using UnityEngine;
using GamestryLab.Debugger;

public class PhaseDirector : MonoBehaviour {

	[SerializeField]
	protected bool isShowingPhaseLog = false;

	protected bool isPreloading = false;

	public Action<PhaseDirector> preloadHandler;
	public Action<PhaseDirector> initializeHandler;
	public Action<PhaseDirector> fixedUpdateHandler;
	public Action<PhaseDirector> updateHandler;
	public Action<PhaseDirector> tickUpdateHandler;
	public Action<PhaseDirector> terminateHandler;
	public Action<PhaseDirector> resetHandler;
	public Action<PhaseDirector> unloadHandler;

	public virtual void OnPreload () {
		if (isShowingPhaseLog) {
			DebugLog.Log("OnPreload");
		}
		if (preloadHandler != null) {
			preloadHandler (this);
		}
	}

	public virtual void OnInitialize () {
		if (isShowingPhaseLog) {
			DebugLog.Log("OnInitialize");
		}
		if (initializeHandler != null) {
			initializeHandler (this);
		}
	}

	public virtual void OnUpdate () {
		if (isShowingPhaseLog) {
			DebugLog.Log("OnUpdate");
		}
		if (updateHandler != null) {
			updateHandler (this);
		}
	}

	public virtual void OnFixedUpdate () {
		if (isShowingPhaseLog) {
			DebugLog.Log("OnFixedUpdate");
		}
		if (fixedUpdateHandler != null) {
			fixedUpdateHandler (this);
		}
	}

	public virtual void OnTickUpdate () {
		if (isShowingPhaseLog) {
			DebugLog.Log("OnTickUpdate");
		}
		if (tickUpdateHandler != null) {
			tickUpdateHandler (this);
		}
	}

	public virtual void OnTerminate () {
		if (isShowingPhaseLog) {
			DebugLog.Log("OnTerminate");
		}
		if (terminateHandler != null) {
			terminateHandler (this);
		}
	}

	public virtual void OnReset () {
		if (isShowingPhaseLog) {
			DebugLog.Log(")OnReset");
		}
		if (resetHandler != null) {
			resetHandler (this);
		}
	}

	public virtual void OnUnload () {
		if (isShowingPhaseLog) {
			DebugLog.Log("OnUnload");
		}
		if (unloadHandler != null) {
			unloadHandler (this);
		}
	}

	public virtual bool IsPreloading () {
		return isPreloading;
	}

	// public virtual void DebugLog.Logstring _message) {
	// 	Debug.Log (GetDebugName () + " : " + _message);
	// }

	public virtual string GetDebugName () {
		return this.name + "(" + this.GetType () + ")";
	}
}