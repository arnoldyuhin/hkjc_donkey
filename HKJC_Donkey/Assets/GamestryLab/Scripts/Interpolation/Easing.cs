﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Interpolation {
	/// <summary>
	/// A set of easing methods, to see a visual representation you can check out:
	/// https://msdn.microsoft.com/en-us/library/vstudio/Ee308751%28v=VS.100%29.aspx
	/// </summary>
	public static class Easing {
		private const float _2PI = 6.28318530717959f;
		private const float _HALF_PI = 1.5707963267949f;

		#region Back Ease
		public static float BackEaseIn (float t, float b, float c, float d) {
			return BackEaseInFull (t, b, c, d);
		}
		public static float BackEaseIn (float from, float to, float rate) {
			return BackEaseInFull (from, to, rate);
		}
		public static Vector3 BackEaseIn (Vector3 from, Vector3 to, float rate) {
			return new Vector3 (BackEaseIn (from.x, to.x, rate), BackEaseIn (from.y, to.y, rate), BackEaseIn (from.z, to.z, rate));
		}
		public static float BackEaseOut (float t, float b, float c, float d) {
			return BackEaseOutFull (t, b, c, d);
		}
		public static float BackEaseOut (float from, float to, float rate) {
			return BackEaseOutFull (from, to, rate);
		}
		public static Vector3 BackEaseOut (Vector3 from, Vector3 to, float rate) {
			return new Vector3 (BackEaseOut (from.x, to.x, rate), BackEaseOut (from.y, to.y, rate), BackEaseOut (from.z, to.z, rate));
		}
		public static float BackEaseInOut (float t, float b, float c, float d) {
			return BackEaseInOutFull (t, b, c, d);
		}
		public static float BackEaseInFull (float t, float b, float c, float d, float s = 1.70158f) {
			return c * (t /= d) * t * ((s + 1) * t - s) + b;
		}
		public static float BackEaseInFull (float from, float to, float rate, float pullback = 1.70158f) {
			return (to - from) * rate * rate * ((pullback + 1) * rate - pullback) + from;
		}
		public static float BackEaseOutFull (float t, float b, float c, float d, float s = 1.70158f) {
			return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
		}
		public static float BackEaseOutFull (float from, float to, float rate, float pullback = 1.70158f) {
			return (to - from) * ((rate - 1) * rate * ((pullback + 1) * rate + pullback) + 1) + from;
		}
		public static float BackEaseInOutFull (float t, float b, float c, float d, float s = 1.70158f) {
			if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525f)) + 1) * t - s)) + b;
			return c / 2 * ((t -= 2) * t * (((s *= (1.525f)) + 1) * t + s) + 2) + b;
		}
		#endregion

		#region Bounce Ease
		public static float BounceEaseOut (float t, float b, float c, float d) {
			if ((t /= d) < (1 / 2.75f)) {
				return c * (7.5625f * t * t) + b;
			} else if (t < (2 / 2.75)) {
				return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f) + b;
			} else if (t < (2.5f / 2.75f)) {
				return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f) + b;
			} else {
				return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f) + b;
			}
		}
		public static float BounceEaseIn (float t, float b, float c, float d) {
			return c - BounceEaseOut (d - t, 0, c, d) + b;
		}
		public static float BounceEaseInOut (float t, float b, float c, float d) {
			if (t < d / 2) return BounceEaseIn (t * 2, 0, c, d) * .5f + b;
			else return BounceEaseOut (t * 2 - d, 0, c, d) * .5f + c * .5f + b;
		}
		#endregion

		#region Circle Ease
		public static float CircleEaseIn (float t, float b, float c, float d) {
			return -c * ((float)Math.Sqrt (1 - (t /= d) * t) - 1) + b;
		}
		public static float CircleEaseOut (float t, float b, float c, float d) {
			return c * (float)Math.Sqrt (1 - (t = t / d - 1) * t) + b;
		}
		public static float CircleEaseInOut (float t, float b, float c, float d) {
			if ((t /= d / 2) < 1) return -c / 2 * ((float)Math.Sqrt (1 - t * t) - 1) + b;
			return c / 2 * ((float)Math.Sqrt (1 - (t -= 2) * t) + 1) + b;
		}
		#endregion

		#region Cubic Ease

		public static float CubicEaseIn (float t, float b, float c, float d) {
			return c * (t /= d) * t * t + b;
		}
		public static float CubicEaseOut (float t, float b, float c, float d) {
			return c * ((t = t / d - 1) * t * t + 1) + b;
		}
		public static float CubicEaseInOut (float t, float b, float c, float d) {
			if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
			return c / 2 * ((t -= 2) * t * t + 2) + b;
		}

		#endregion

		#region Elastic Ease
		public static float ElasticEaseIn (float t, float b, float c, float d) {
			return ElasticEaseInFull (t, b, c, d, 0, 0);
		}

		public static float ElasticEaseOut (float t, float b, float c, float d) {
			return ElasticEaseOutFull (t, b, c, d, 0, 0);
		}

		public static float ElasticEaseInOut (float t, float b, float c, float d) {
			return ElasticEaseInOutFull (t, b, c, d, 0, 0);
		}

		public static float ElasticEaseInFull (float t, float b, float c, float d, float a, float p) {
			float s;
			if (t == 0f) return b; if ((t /= d) == 1) return b + c;
			if (p == 0f) p = d * 0.3f;
			if (a == 0f || a < Math.Abs (c)) { a = c; s = p / 4; } else s = p / _2PI * (float)Math.Asin (c / a);
			return -(a * (float)Math.Pow (2, 10 * (t -= 1)) * (float)Math.Sin ((t * d - s) * _2PI / p)) + b;
		}
		public static float ElasticEaseOutFull (float t, float b, float c, float d, float a = 0, float p = 0) {
			float s;
			if (t == 0f) return b;
			if ((t /= d) == 1) return b + c;
			if (p == 0f) p = d * 0.3f;
			if (a == 0f || a < Math.Abs (c)) { a = c; s = p / 4; } else s = p / _2PI * (float)Math.Asin (c / a);
			return (a * (float)Math.Pow (2, -10 * t) * (float)Math.Sin ((t * d - s) * _2PI / p) + c + b);
		}
		public static float ElasticEaseInOutFull (float t, float b, float c, float d, float a = 0, float p = 0) {
			float s;
			if (t == 0f) return b; if ((t /= d / 2) == 2) return b + c;
			if (p == 0f) p = d * (0.3f * 1.5f);
			if (a == 0f || a < Math.Abs (c)) { a = c; s = p / 4; } else s = p / _2PI * (float)Math.Asin (c / a);
			if (t < 1) return -.5f * (a * (float)Math.Pow (2, 10 * (t -= 1)) * (float)Math.Sin ((t * d - s) * _2PI / p)) + b;
			return a * (float)Math.Pow (2, -10 * (t -= 1)) * (float)Math.Sin ((t * d - s) * _2PI / p) * .5f + c + b;
		}

		#endregion

		#region Expo Ease
		public static float ExpoEaseIn (float t, float b, float c, float d) {
			return (t == 0) ? b : c * (float)Math.Pow (2, 10 * (t / d - 1)) + b - c * 0.001f;
		}
		public static float ExpoEaseOut (float t, float b, float c, float d) {
			return (t == d) ? b + c : c * (-(float)Math.Pow (2, -10 * t / d) + 1) + b;
		}
		public static float ExpoEaseInOut (float t, float b, float c, float d) {
			if (t == 0) return b;
			if (t == d) return b + c;
			if ((t /= d / 2) < 1) return c / 2 * (float)Math.Pow (2, 10 * (t - 1)) + b;
			return c / 2 * (-(float)Math.Pow (2, -10 * --t) + 2) + b;
		}
		#endregion

		#region Linear Ease
		public static float LinearEaseNone (float t, float b, float c, float d) {
			return c * t / d + b;
		}
		public static float LinearEaseNone (float from, float to, float rate) {
			return (to - from) * rate + from;
		}
		public static Vector3 LinearEaseNone(Vector3 from, Vector3 to, float rate) {
			return new Vector3 (LinearEaseNone (from.x, to.x, rate), LinearEaseNone (from.y, to.y, rate), LinearEaseNone (from.z, to.z, rate));
		}
		public static float LinearEaseIn (float t, float b, float c, float d) {
			return c * t / d + b;
		}
		public static float LinearEaseOut (float t, float b, float c, float d) {
			return c * t / d + b;
		}
		public static float LinearEaseInOut (float t, float b, float c, float d) {
			return c * t / d + b;
		}
		#endregion

		#region Quad Ease
		public static float QuadEaseIn (float t, float b, float c, float d) {
			return c * (t /= d) * t + b;
		}
		public static float QuadEaseIn (float from, float to, float rate) {
			return (to - from) * rate * rate + from;
		}
		public static Vector3 QuadEaseIn (Vector3 from, Vector3 to, float rate) {
			return new Vector3 (QuadEaseIn (from.x, to.x, rate), QuadEaseIn (from.y, to.y, rate), QuadEaseIn (from.z, to.z, rate));
		}
		public static float QuadEaseOut (float t, float b, float c, float d) {
			return -c * (t /= d) * (t - 2) + b;
		}
		public static float QuadEaseOut (float from, float to, float rate) {
			return -(to - from) * rate * (rate - 2f) + from;
		}
		public static Vector3 QuadEaseOut (Vector3 from, Vector3 to, float rate) {
			return new Vector3 (QuadEaseOut (from.x, to.x, rate), QuadEaseOut (from.y, to.y, rate), QuadEaseOut (from.z, to.z, rate));
		}
		public static float QuadEaseInOut (float t, float b, float c, float d) {
			if ((t /= d / 2) < 1) return c / 2 * t * t + b;
			return -c / 2 * ((--t) * (t - 2) - 1) + b;
		}
		public static float QuadEaseInOut (float from, float to, float rate) {
			if ((rate / 2) < 1) return (to - from) / 2 * rate * rate + from;
			return -(to - from) / 2 * ((--rate) * (rate - 2) - 1) + from;
		}
		public static Vector3 QuadEaseInOut (Vector3 from, Vector3 to, float rate) {
			return new Vector3 (QuadEaseInOut (from.x, to.x, rate), QuadEaseInOut (from.y, to.y, rate), QuadEaseInOut (from.z, to.z, rate));
		}
		#endregion

		#region Quart Ease
		public static float QuartEaseIn (float t, float b, float c, float d) {
			return c * (t /= d) * t * t * t + b;
		}
		public static float QuartEaseOut (float t, float b, float c, float d) {
			return -c * ((t = t / d - 1) * t * t * t - 1) + b;
		}
		public static float QuartEaseInOut (float t, float b, float c, float d) {
			if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
			return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
		}
		#endregion

		#region Quint Ease
		public static float QuintEaseIn (float t, float b, float c, float d) {
			return c * (t /= d) * t * t * t * t + b;
		}
		public static float QuintEaseIn (float from, float to, float rate) {
			return (to - from) * ((rate - 1) * rate * rate * rate * rate + 1) + from;
		}
		public static Vector3 QuintEaseIn (Vector3 from, Vector3 to, float rate) {
			return new Vector3 (QuintEaseIn (from.x, to.x, rate), QuintEaseIn (from.y, to.y, rate), QuintEaseIn (from.z, to.z, rate));
		}
		public static float QuintEaseOut (float t, float b, float c, float d) {
			return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
		}
		public static float QuintEaseOut (float from, float to, float rate) {
			return (to - from) * ((rate - 1) * rate * rate * rate * rate + 1) + from;
		}
		public static Vector3 QuintEaseOut (Vector3 from, Vector3 to, float rate) {
			return new Vector3 (QuintEaseOut (from.x, to.x, rate), QuintEaseOut (from.y, to.y, rate), QuintEaseOut (from.z, to.z, rate));
		}
		public static float QuintEaseInOut (float t, float b, float c, float d) {
			if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
			return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
		}
		#endregion

		#region Sine Ease
		public static float SineEaseIn (float t, float b, float c, float d) {
			return -c * (float)Math.Cos (t / d * _HALF_PI) + c + b;
		}
		public static float SineEaseOut (float t, float b, float c, float d) {
			return c * (float)Math.Sin (t / d * _HALF_PI) + b;
		}
		public static float SineEaseInOut (float t, float b, float c, float d) {
			return -c / 2 * ((float)Math.Cos (Math.PI * t / d) - 1) + b;
		}
		#endregion

		#region Strong Ease
		public static float StrongEaseIn (float t, float b, float c, float d) {
			return c * (t /= d) * t * t * t * t + b;
		}
		public static float StrongEaseOut (float t, float b, float c, float d) {
			return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
		}
		public static float StrongEaseInOut (float t, float b, float c, float d) {
			if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
			return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
		}
		#endregion
	}
}