﻿using DG.Tweening;
using GamestryLab.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BalanceBreakers.Common
{
    public class TransitionScreenBackup : MonoBehaviour, IService
    {
        [SerializeField] private Animator animator;
        [SerializeField] private Transform transformOfContent;
        [SerializeField] private Transform transformOfBars;
        [SerializeField] private Image[] bars;
        [SerializeField] private Image logo;

        public bool IsClosed;

        private Color[] colors = { new Color(1f, 0.2f, 0.46f), new Color(0.095f, 0.875f, 0.945f) };

        public event Action<object, string> AnimationCompleted;

        public const string AnimationStateClose = "Close";
        public const string AnimationStateOpen = "Open";

        public void OnAnimationCompleted(string stateName)
        {
            switch (stateName)
            {
                case AnimationStateClose:
                    IsClosed = true;

                    Debug.Log("Close Completed");

                    transformOfContent.DOShakePosition(0.5f, 40f);
                    break;

                case AnimationStateOpen:
                    Debug.Log("Open Completed");
                    break;
            }

            if (AnimationCompleted != null)
            {
                AnimationCompleted(this, stateName);
            }
        }

        //private void Update()
        //{
        //    if (UnityEngine.Input.GetKeyDown(KeyCode.Keypad1))
        //    {
        //        Close();
        //    }
        //    else if (UnityEngine.Input.GetKeyDown(KeyCode.Keypad3))
        //    {
        //        Open();
        //    }
        //}

        [ContextMenu("Open")]
        public void Open()
        {
            animator.Play(AnimationStateOpen, -1, 0f);
        }

        [ContextMenu("Close")]
        public void Close()
        {
            IsClosed = false;

            (transformOfBars as RectTransform).localEulerAngles = new Vector3(0f, 0f, UnityEngine.Random.Range(-10f, 10f));

            int index = UnityEngine.Random.Range(0, colors.Length);
            Color color = colors[index];

            foreach (Image image in bars)
            {
                image.color = color;
            }

            animator.Play(AnimationStateClose, -1, 0f);
        }
    }
}