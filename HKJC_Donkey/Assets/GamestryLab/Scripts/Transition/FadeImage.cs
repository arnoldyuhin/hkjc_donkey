﻿using System.Collections;
using System.Collections.Generic;
using GamestryLab.UI;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace GamestryLab.Transition {
	public class FadeImage : UIContainer {
		[SerializeField] private Image overlayImage;

		public void FadeInOverlay(float time) {
			//overlayImage.color = new Color (overlayImage.color.r, overlayImage.color.g, overlayImage.color.b, 0f);
			overlayImage.DOFade (1f, time);
		}

		public void FadeInOverlayEnd() {
			overlayImage.DOKill ();
			overlayImage.color = new Color (overlayImage.color.r, overlayImage.color.g, overlayImage.color.b, 1f);
		}

		public void FadeOutOverlay (float time) {
			//overlayImage.color = new Color (overlayImage.color.r, overlayImage.color.g, overlayImage.color.b, 1f);
			overlayImage.DOFade (0f, time);
		}

		public void FadeOutOverlayEnd () {
			overlayImage.DOKill ();
			overlayImage.color = new Color (overlayImage.color.r, overlayImage.color.g, overlayImage.color.b, 0f);
		}
	}
}
