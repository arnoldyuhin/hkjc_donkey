﻿using System;
using System.Collections;
using System.Collections.Generic;
using GamestryLab.Operations;
using GamestryLab.System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GamestryLab.Transition {
	public class TransitionScreen : MonoBehaviour {
		[SerializeField] private Workflow showTransitionFlow;
		[SerializeField] private Workflow hideTransitionFlow;
		[SerializeField] private Slider progressBar;

		public Action ShowEnded;
		public Action HideEnded;

		public void Show () {
			showTransitionFlow.Debug_Begin ();
			showTransitionFlow.Ended.AddListener (OnShowEnded);
		}

		private void OnShowEnded (object obj) {
			showTransitionFlow.Ended.RemoveListener (OnShowEnded);
			ShowEnded?.Invoke ();
		}

		public void Hide () {
			hideTransitionFlow.Debug_Begin ();
			hideTransitionFlow.Ended.AddListener (OnHideEnded);
		}

		private void OnHideEnded (object obj) {
			hideTransitionFlow.Ended.RemoveListener (OnHideEnded);
			HideEnded?.Invoke ();
			Destroy (this.gameObject);
		}

		public void EnableProgressBar (bool flag) {
			if (progressBar == null) return;
			if (flag) {
				AppManager.Instance.Updated += UpdateProgressBar;
			} else {
				AppManager.Instance.Updated -= UpdateProgressBar;
			}
			progressBar.gameObject.SetActive (flag);
		}

		public void UpdateProgressBar () {
			progressBar.value = AppManager.Instance.AsyncLoad.progress;
		}
	}
}
