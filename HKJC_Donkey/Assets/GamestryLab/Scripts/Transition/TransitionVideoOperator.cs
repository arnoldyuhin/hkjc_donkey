﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

namespace GamestryLab.Transition {
	public class TransitionVideoOperator : MonoBehaviour {
		[SerializeField] private VideoPlayer videoPlayer;
		[SerializeField] private float fadeOutTime = 1f;

		public UnityEvent videoPrepared;
		public UnityEvent videoReadyToHide;

		private void Awake () {
			videoPlayer.Prepare ();
		}

		public void CheckVideoPreparation () {
			if (videoPlayer.isPrepared) {
				videoPrepared?.Invoke ();
			}
		}

		public void CheckVideoRemain () {
			if (videoPlayer.frame > videoPlayer.frameCount - 20f) {
				videoReadyToHide?.Invoke ();
				return;
			}
#if UNITY_EDITOR
			if (UnityEngine.Input.GetKeyDown (KeyCode.Space)) {
				videoReadyToHide?.Invoke ();
				return;
			}
#endif
		}
	}
}