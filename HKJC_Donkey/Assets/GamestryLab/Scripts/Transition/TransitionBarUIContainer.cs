﻿using System.Collections;
using System.Collections.Generic;
using GamestryLab.Audio;
using GamestryLab.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace GamestryLab.Transition {
	public class TransitionBarUIContainer : MonoBehaviour {
		[SerializeField] private Transform transformOfBars;
		[SerializeField] private Image [] bars;
		[SerializeField] private Image logo;

		private Color [] colors = { new Color (1f, 0.2f, 0.46f), new Color (0.095f, 0.875f, 0.945f) };


		public void SetRandomBarColor () {
			int index = UnityEngine.Random.Range (0, colors.Length);
			Color color = colors [index];

			foreach (Image image in bars) {
				image.color = color;
			}
		}

		public void SetRandomAngle () {
			(transformOfBars as RectTransform).localEulerAngles = new Vector3 (0f, 0f, UnityEngine.Random.Range (-10f, 10f));

		}
	}
}
