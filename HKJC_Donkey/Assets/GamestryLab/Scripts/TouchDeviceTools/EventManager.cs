﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour {

	private Dictionary<string, UnityEvent> eventDictionary = new Dictionary<string, UnityEvent>();
	private static EventManager eventManager;
	public static EventManager instance {
		get {
			if (!eventManager) {
				eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

				if (!eventManager) {
					Debug.Log("There needs to be one active EventManager script on a GameObject in scene.");
				}
			}
			return eventManager;
		}
	}

	public static void StartListening(string _eventName, UnityAction _listener) {
		UnityEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue(_eventName, out thisEvent)) {
			thisEvent.AddListener(_listener);
		} else {
			thisEvent = new UnityEvent();
			thisEvent.AddListener(_listener);
			instance.eventDictionary.Add(_eventName, thisEvent);
		}
	}

	public static void StopListening(string _eventName, UnityAction _listener) {
		if (eventManager == null) return;
		UnityEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue(_eventName, out thisEvent)) {
			thisEvent.RemoveListener(_listener);
		}
	}

	public static void TriggerEvent(string _eventName) {
		UnityEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue(_eventName, out thisEvent)) {
			thisEvent.Invoke();
		}
	}
}
