﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITouchable {

	int objectLayer { 
		get; 
		set;
	}

	TouchPosType touchPosType {
		get;
		set;
	}

	void HoverStart(Vector3 _startPos);
	void HoverUpdate(Vector3 _holdPos);
	void HoverEnd(Vector3 _endPos);
	void ClickStart(Vector3 _startPos);
	void ClickUpdate(Vector3 _holdPos);
	void ClickEnd(Vector3 _endPos);
	void ClickFail(Vector3 _endPos);
	void ClickSuccess(Vector3 _endPos);
	void Swipe(Vector3 _direction, Vector3 _endPos);
}
