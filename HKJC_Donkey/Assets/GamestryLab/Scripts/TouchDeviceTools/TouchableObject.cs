﻿using System;
using System.Collections;
using UnityEngine;
using GamestryLab.Debugger;

public class TouchableObject : SpriteObject, ITouchable {

	public bool isShowingTouchableObjectLog = false;

	public int objectLayerIndex = 2;
	public int objectLayer {
		get { return objectLayerIndex; }
		set { objectLayerIndex = value; }
	}

	public TouchPosType touchPosTypeSelected;
	public TouchPosType touchPosType {
		get { return touchPosTypeSelected; }
		set { touchPosTypeSelected = value; }
	}

	protected bool isClicking = false;
	protected Vector3 clickingPos = Vector3.zero;

	public Action<TouchableObject, Vector3> HoverStarted;
	public Action<TouchableObject, Vector3> HoverUpdated;
	public Action<TouchableObject, Vector3> HoverEnded;
	public Action<TouchableObject, Vector3> ClickStarted;
	public Action<TouchableObject, Vector3> ClickUpdated;
	public Action<TouchableObject, Vector3> ClickEnded;
	public Action<TouchableObject, Vector3> ClickSucceed;
	public Action<TouchableObject, Vector3> ClickFailed;
	public Action<TouchableObject, Vector3, Vector3> Swiped;

	public virtual void HoverStart (Vector3 _startPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("HoverStarted (" + _startPos.ToString () + ")");
		}
		if (HoverStarted != null) {
			HoverStarted (this, _startPos);
		}
	}

	public virtual void HoverUpdate (Vector3 _holdPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("HoverUpdate (" + _holdPos.ToString () + ")");
		}
		if (HoverUpdated != null) {
			HoverUpdated (this, _holdPos);
		}
	}
	public virtual void HoverEnd (Vector3 _endPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("HoverEnd (" + _endPos.ToString () + ")");
		}
		if (HoverEnded != null) {
			HoverEnded (this, _endPos);
		}
	}

	public virtual void ClickStart (Vector3 _startPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("ClickStart (" + _startPos.ToString () + ")");
		}
		isClicking = true;
		clickingPos = _startPos;
		if (ClickStarted != null) {
			ClickStarted (this, _startPos);
		}
	}

	public virtual void ClickUpdate (Vector3 _holdPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("ClickUpdate (" + _holdPos.ToString () + ")");
		}
		clickingPos = _holdPos;
		if (ClickUpdated != null) {
			ClickUpdated (this, _holdPos);
		}
	}

	public virtual void ClickEnd (Vector3 _endPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("ClickEnd (" + _endPos.ToString () + ")");
		}
		isClicking = false;
		clickingPos = _endPos;
		if (ClickEnded != null) {
			ClickEnded (this, _endPos);
		}
	}

	public virtual void ClickFail (Vector3 _endPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("ClickFail (" + _endPos.ToString () + ")");
		}
		if (ClickFailed != null) {
			ClickFailed (this, _endPos);
		}
	}

	public virtual void ClickSuccess (Vector3 _endPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("ClickSuccess (" + _endPos.ToString () + ")");
		}
		clickingPos = _endPos;
		if (ClickSucceed != null) {
			ClickSucceed (this, _endPos);
		}
	}

	public virtual void Swipe (Vector3 _direction, Vector3 _endPos) {
		if (isShowingTouchableObjectLog) {
			DebugLog.Log("Swipe (" + _direction.ToString () + ", " + _endPos.ToString () + ")");
		}
		clickingPos = _endPos;
		if (Swiped != null) {
			Swiped (this, _direction, _endPos);
		}
	}
}