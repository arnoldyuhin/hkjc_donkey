﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour {

	public static TouchManager instance;

	public int maxTouchAmount = 1;
	public bool isShowingTouchRay = false;

	public List<Camera> touchingCameras = new List<Camera> ();

	private List<ITouchable> hoveringObjects = new List<ITouchable> ();
	private List<InteractingObject> reachedObjects = new List<InteractingObject> ();
	private List<InteractingObject> affectingObjects = new List<InteractingObject> ();

	public bool isHoverEnabled = true;

	public void Awake () {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (this);
		} else {
			GameObject.DestroyImmediate (this.gameObject);
		}
		if (touchingCameras.Count == 0) {
			touchingCameras.Add (Camera.main);
		}

	}

	public void Update () {
		if (isHoverEnabled) {
			//only avaliable for mouse
			HoverHandler (Input.mousePosition);
		}
		foreach (Touch touch in Input.touches) {
			if (touch.fingerId < maxTouchAmount) {
				TouchHandler (touch.fingerId, touch.position, touch.phase);
				//				Debug.Log (touch.fingerId);
			}
		}
		if (Input.touchCount == 0) {
			if (Input.GetMouseButtonDown (0)) {
				TouchHandler (33, Input.mousePosition, TouchPhase.Began);
			}
			if (Input.GetMouseButton (0)) {
				TouchHandler (33, Input.mousePosition, TouchPhase.Moved);
			}
			if (Input.GetMouseButtonUp (0)) {
				TouchHandler (33, Input.mousePosition, TouchPhase.Ended);
			}
		}

		//		Debug.Log (Input.touches.Length + "," + transform.name);
	}

	private void TouchHandler (int _touchFingerId, Vector3 _touchPosition, TouchPhase _touchPhase) {
		//debug ray
		if (isShowingTouchRay) {
			for (int i = 0; i < touchingCameras.Count; i++) {
				Ray ray = touchingCameras[i].ScreenPointToRay (_touchPosition);
				Debug.DrawRay (ray.origin, ray.direction * 100f, Color.red, Time.deltaTime);
			}
		}

		switch (_touchPhase) {
			case TouchPhase.Began:
				reachedObjects.Clear ();
				for (int i = 0; i < touchingCameras.Count; i++) {
					Ray downRay = touchingCameras[i].ScreenPointToRay (_touchPosition);
					RaycastHit[] downHits = Physics.RaycastAll (downRay);
					if (downHits.Length > 0) {
						for (int j = 0; j < downHits.Length; j++) {
							ITouchable[] touchableObjects = downHits[j].transform.GetComponents<ITouchable> ();
							if (touchableObjects.Length > 0) {
								for (int k = 0; k < touchableObjects.Length; k++) {
									reachedObjects.Add (new InteractingObject (touchingCameras[i], j, _touchFingerId, touchableObjects[k]));
								}
							}
						}
					}
					RaycastHit2D[] downHits2D = Physics2D.GetRayIntersectionAll (downRay);
					if (downHits2D.Length > 0) {
						for (int j = 0; j < downHits2D.Length; j++) {
							ITouchable[] touchable2DObjects = downHits2D[j].transform.GetComponents<ITouchable> ();
							if (touchable2DObjects.Length > 0) {
								for (int k = 0; k < touchable2DObjects.Length; k++) {
									reachedObjects.Add (new InteractingObject (touchingCameras[i], j, _touchFingerId, touchable2DObjects[k]));
								}
							}
						}
					}
				}

				reachedObjects.Sort ();
				int topLayer = 0;
				for (int i = 0; i < reachedObjects.Count; i++) {
					if (reachedObjects[i].objectLayer < 0) {
						continue;
					}
					if (reachedObjects[i].objectLayer == 0) {
						reachedObjects[i].OnClickDown ();
						affectingObjects.Add (reachedObjects[i]);
					}
					if (reachedObjects[i].objectLayer > 0) {
						if (topLayer == 0) {
							topLayer = reachedObjects[i].objectLayer;
						}
						if (reachedObjects[i].objectLayer == topLayer) {
							reachedObjects[i].OnClickDown ();
							affectingObjects.Add (reachedObjects[i]);
						} else {
							break;
						}
					}
				}
				break;

			case TouchPhase.Moved:
				for (int i = 0; i < affectingObjects.Count; i++) {
					if (affectingObjects[i].fingerId == _touchFingerId) {
						affectingObjects[i].OnClickUpdate ();
					}
				}
				break;

			case TouchPhase.Ended:
				if (affectingObjects.Count > 0) {
					List<InteractingObject> removingObjects = new List<InteractingObject> ();
					for (int i = 0; i < affectingObjects.Count; i++) {
						if (affectingObjects[i].fingerId == _touchFingerId) {
							affectingObjects[i].OnClickUp ();
							removingObjects.Add (affectingObjects[i]);
						}
					}
					for (int i = 0; i < removingObjects.Count; i++) {
						affectingObjects.Remove (removingObjects[i]);
					}
				}
				break;
		}
	}

	private void HoverHandler (Vector3 _mousePos) {
		List<ITouchable> currentHoveringObjects = new List<ITouchable> ();
		// hoveringObjects.Clear ();
		for (int i = 0; i < touchingCameras.Count; i++) {
			Ray downRay = touchingCameras[i].ScreenPointToRay (_mousePos);
			RaycastHit[] downHits = Physics.RaycastAll (downRay);
			if (downHits.Length > 0) {
				for (int j = 0; j < downHits.Length; j++) {
					ITouchable[] touchableObjects = downHits[j].transform.GetComponents<ITouchable> ();
					if (touchableObjects.Length > 0) {
						for (int k = 0; k < touchableObjects.Length; k++) {
							if (!currentHoveringObjects.Contains (touchableObjects[k])) {
								currentHoveringObjects.Add (currentHoveringObjects[k]);
							}
						}
					}
				}
			}
			RaycastHit2D[] downHits2D = Physics2D.GetRayIntersectionAll (downRay);
			if (downHits2D.Length > 0) {
				for (int j = 0; j < downHits2D.Length; j++) {
					ITouchable[] touchable2DObjects = downHits2D[j].transform.GetComponents<ITouchable> ();
					if (touchable2DObjects.Length > 0) {
						for (int k = 0; k < touchable2DObjects.Length; k++) {
							if (!currentHoveringObjects.Contains (touchable2DObjects[k])) {
								currentHoveringObjects.Add (touchable2DObjects[k]);
							}
						}
					}
				}
			}
		}

		//hover settlement
		for (int i = 0; i < hoveringObjects.Count; i++) {
			if (currentHoveringObjects.Contains (hoveringObjects[i])) {
				//hover the exist hovering object
				hoveringObjects[i].HoverUpdate (_mousePos);
				currentHoveringObjects.Remove (hoveringObjects[i]);
			} else {
				//no longer hover the exist hovering object
				hoveringObjects[i].HoverEnd (_mousePos);
				hoveringObjects.RemoveAt(i);
				i--;
			}
		}
		for (int i = 0; i < currentHoveringObjects.Count; i++) {
			if (!hoveringObjects.Contains (currentHoveringObjects[i])) {
				//hover new object
				hoveringObjects.Add (currentHoveringObjects[i]);
				currentHoveringObjects[i].HoverStart (_mousePos);
			}
		}

	}

}