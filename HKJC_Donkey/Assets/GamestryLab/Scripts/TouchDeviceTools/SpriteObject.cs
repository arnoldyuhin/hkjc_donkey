﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpriteObject : CreatedObject {

	protected SpriteRenderer spriteRenderer;

	public override void OnPreload() {
		base.OnPreload();
		spriteRenderer = this.GetComponent<SpriteRenderer>();
		if (spriteRenderer == null) {
			spriteRenderer = this.gameObject.AddComponent<SpriteRenderer>();
		}
	}

	public virtual void SetSprite(string _path) {
		spriteRenderer.sprite = Resources.Load<Sprite>(_path);
	}

	public virtual void SetSprite(string _path, string _name) {
		Sprite[] sprites = Resources.LoadAll<Sprite>(_path);
		for (int i = 0; i < sprites.Length; i++) {
			if (sprites[i].name == _name) {
				spriteRenderer.sprite = sprites[i];
				break;
			}
		}
	}

	public virtual void SetSpriteSize(Vector3 _size) {
		spriteRenderer.transform.localScale = _size;
	}

	public virtual void SetSpriteColor(Color _color) {
		spriteRenderer.color = _color;
	}

	public virtual void SetSpriteSortingLayer(int _layer) {
		spriteRenderer.sortingOrder = _layer;
	}
}
