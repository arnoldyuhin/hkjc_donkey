﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ImageObject : CreatedObject {

	protected Image image;

	public override void OnPreload() {
		base.OnPreload();
		image = this.GetComponent<Image>();
		if (image == null) {
			image = this.gameObject.AddComponent<Image>();
		}
	}

	public virtual void SetImage(string _path) {
		image.sprite = Resources.Load<Sprite>(_path);
	}

	public virtual void SetImage(string _path, string _name) {
		Sprite[] sprites = Resources.LoadAll<Sprite>(_path);
		for (int i = 0; i < sprites.Length; i++) {
			if (sprites[i].name == _name) {
				image.sprite = sprites[i];
				break;
			}
		}
	}

	public virtual void SetImageSize(Vector3 _size) {
		image.transform.localScale = _size;
	}

	public virtual void SetImageColor(Color _color) {
		image.color = _color;
	}

//	public virtual void SetSpriteSortingLayer(int _layer) {
//		if (objectSprite != null) {
//			objectSprite.sortingOrder = _layer;
//		}
//	}
}
