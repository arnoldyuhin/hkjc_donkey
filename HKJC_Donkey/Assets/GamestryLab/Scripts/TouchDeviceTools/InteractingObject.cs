﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum TouchPosType { World, Screen};

public class InteractingObject : IComparable<InteractingObject> {

	public Camera touchingCamera;
	public int topIndex;
	public int fingerId;
	public ITouchable touchableObject;
	public int objectLayer; //0 is always, -1 is never, smaller number comes first


	public InteractingObject(Camera _camera, int _topIndex, int _fingerId, ITouchable _touchableObject) {
		touchingCamera = _camera;
		topIndex = _topIndex;
		fingerId = _fingerId;
		touchableObject = _touchableObject;
		objectLayer = touchableObject.objectLayer;
	}

	public int CompareTo(InteractingObject _other) {
		if (_other == null) {
			return 1;
		}
		if (_other.objectLayer > this.objectLayer) {
			return -1;
		}
		if (_other.objectLayer < this.objectLayer) {
			return 1;
		}
		if (_other.objectLayer == this.objectLayer) {
			if (_other.topIndex > this.topIndex) {
				return -1;
			}
			if (_other.topIndex < this.topIndex) {
				return 1;
			}
		}
		return 0;
	}

	private bool isClicking = false;

	private float totalTouchingTime = 0f;
	private float minSwipeDistance = 0.5f;
	private float maxSwipeTime = 0.07f;

	private Vector3 startPos = Vector3.zero;
	private Vector3 currentPos;
	private Vector3 previousPos;

	protected class SwipeMark {
		public float time = 0f;
		public Vector3 offset = Vector3.zero;
		public Vector3 pos = Vector3.zero;

		public SwipeMark(float _time, Vector3 _offset, Vector3 _pos) {
			time = _time;
			offset = _offset;
			pos = _pos;
		}
	}
	private List<SwipeMark> swipeMarks = new List<SwipeMark>();

	private float presetWorldDepth = 0f;

	public void OnClickDown() {
		if (!isClicking) {
			isClicking = true;
			startPos = GetTouchPos();
			currentPos = startPos;
			touchableObject.ClickStart(startPos);
		}
		swipeMarks.Clear();
	}

	public void OnClickUpdate() {
		touchableObject.ClickUpdate(GetTouchPos());
		totalTouchingTime += Time.deltaTime;
		currentPos = GetTouchPos();
		for (int i = 0; i < swipeMarks.Count; i++) {
			if (swipeMarks[i].time < totalTouchingTime - maxSwipeTime) {
				swipeMarks.RemoveAt(i);
			} else {
				break;
			}
		}
		swipeMarks.Add(new SwipeMark(totalTouchingTime, currentPos - previousPos, currentPos));
		previousPos = currentPos;
	}

	public void OnClickUp() {
		if (isClicking) {
			isClicking = false;
			currentPos = GetTouchPos();
			Ray endRay = new Ray();
			switch (touchableObject.touchPosType) {
			case TouchPosType.Screen:
				endRay = touchingCamera.ScreenPointToRay(currentPos);
				break;
			case TouchPosType.World:
				endRay = new Ray(touchingCamera.transform.position, currentPos - touchingCamera.transform.position);
				break;
			}
//			Debug.DrawRay(endRay.origin, endRay.direction * 100f, Color.red, 3f);
			bool isClickSuccess = false;
			RaycastHit[] endRayHits = Physics.RaycastAll(endRay);
			for (int i  = 0; i < endRayHits.Length; i++) {
				if (endRayHits[i].transform.gameObject.GetComponent<ITouchable>() != null) {
					if (endRayHits[i].transform.gameObject.GetComponent<ITouchable>() == touchableObject) {
						isClickSuccess = true;
						break;
					}
				}
			}
			RaycastHit2D[] endRayHits2D = Physics2D.GetRayIntersectionAll(endRay);
			for (int i  = 0; i < endRayHits2D.Length; i++) {
				if (endRayHits2D[i].transform.gameObject.GetComponent<ITouchable>() != null) {
					if (endRayHits2D[i].transform.gameObject.GetComponent<ITouchable>() == touchableObject) {
						isClickSuccess = true;
						break;
					}
				}
			}

			touchableObject.ClickEnd(currentPos);
			if (isClickSuccess) {
				touchableObject.ClickSuccess(currentPos);
			} else {
				touchableObject.ClickFail(currentPos);
			}

			//swipe
			bool isSwipe = false;
			for (int i = 0; i < swipeMarks.Count; i++) {
				if (Vector3.Distance(swipeMarks[i].pos, swipeMarks[0].pos) > minSwipeDistance) {
					isSwipe = true;
					break;
				}
			}
			if (isSwipe) {
				Vector3 totalSwipeDirection = Vector3.zero;
				for (int i = 0; i < swipeMarks.Count; i++) {
					totalSwipeDirection += swipeMarks[i].offset;
				}
				touchableObject.Swipe(Vector3.Normalize(totalSwipeDirection), currentPos);
			}
		}
	}

	private Vector3 GetTouchPos() {
		if (fingerId != 33) {
			for (int i = 0; i < Input.touchCount; i++) {
				if (Input.touches[i].fingerId == fingerId) {
					switch(touchableObject.touchPosType) {
					case TouchPosType.Screen:
						return Input.touches[i].position;
					case TouchPosType.World:
//						currentPos = Input.touches[i].position;
						Ray ray = touchingCamera.ScreenPointToRay(Input.touches[i].position);
						RaycastHit[] hits = Physics.RaycastAll(ray);
						for (int j = 0; j < hits.Length; j++) {
							if (hits[j].transform.GetComponent<ITouchable>() == touchableObject) {
								return hits[j].point;
							}
						}
						RaycastHit2D[] hits2D = Physics2D.GetRayIntersectionAll(ray);
						for (int j = 0; j < hits2D.Length; j++) {
							if (hits2D[j].transform.GetComponent<ITouchable>() == touchableObject) {
								return hits2D[j].point;
							}
						}
						return touchingCamera.ScreenToWorldPoint(new Vector3(Input.touches[i].position.x, Input.touches[i].position.y, presetWorldDepth - touchingCamera.transform.position.z));
					}
				}
			}
		} else {
			switch(touchableObject.touchPosType) {
			case TouchPosType.Screen:
				return Input.mousePosition;
			case TouchPosType.World:
//				currentPos = Input.mousePosition;
//				currentPos = _camera.ScreenToWorldPoint(Input.mousePosition);
				Ray ray = touchingCamera.ScreenPointToRay(Input.mousePosition);
				RaycastHit[] hits = Physics.RaycastAll(ray);
				for (int j = 0; j < hits.Length; j++) {
					if (hits[j].transform.GetComponent<ITouchable>() == touchableObject) {
						return hits[j].point;
					}
				}
				RaycastHit2D[] hits2D = Physics2D.GetRayIntersectionAll(ray);
				for (int j = 0; j < hits2D.Length; j++) {
					if (hits2D[j].transform.GetComponent<ITouchable>() == touchableObject) {
						return hits2D[j].point;
					}
				}
				return touchingCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, presetWorldDepth - touchingCamera.transform.position.z));
			}
		}
		return Vector3.one * -1f;
	}
}
