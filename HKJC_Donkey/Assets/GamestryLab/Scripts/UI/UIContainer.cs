﻿using UnityEngine;

namespace GamestryLab.UI {
    public abstract class UIContainer : MonoBehaviour {
        public bool isDontDestroy = false;
    }
}