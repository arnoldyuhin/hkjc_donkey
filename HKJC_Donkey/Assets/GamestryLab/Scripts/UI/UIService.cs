﻿using System.Collections;
using System.Collections.Generic;
using GamestryLab.Debugger;
using GamestryLab.System;
using GamestryLab.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace GamestryLab.UI {
	public class UIService : MonoBehaviour, IService, IManualUpdatable {
		private Dictionary<string, UIContainer> UIContainers = new Dictionary<string, UIContainer> ();

		private TaskManager taskManager;

		public void Awake () {
			LoadAllUIs ();
			taskManager = new TaskManager ();
		}

		public void ManualUpdate () {
			taskManager.ManualUpdate ();
		}

		private void LoadAllUIs () {
			Object [] presetUIs = FindObjectsOfTypeAll (typeof (UIContainer));
			for (int i = 0; i < presetUIs.Length; i++) {
				if (!UIContainers.ContainsKey (presetUIs [i].name)) {
					UIContainers.Add (presetUIs [i].name, (UIContainer)presetUIs [i]);
				}
			}
		}

		public T GetUI<T> (string id) where T : UIContainer {
			if (!UIContainers.ContainsKey (id)) {
				DebugLog.Log (Color.red, "UIContainer(id: {0}) is not exist.", id);
				return null;
			}
			return (T)UIContainers [id];
		}

		public void RefreshUIs () {
			RemoveUIs ();
			LoadAllUIs ();
		}

		public void RemoveUIs () {
			foreach (string id in UIContainers.Keys) {
				UIContainer containers = UIContainers [id];
				if (containers.isDontDestroy) {
					continue;
				}
				GameObject.Destroy (containers);
				UIContainers.Remove (id);
			}
		}

		private void RemoveUI (string id) {
			if (!UIContainers.ContainsKey (id)) {
				DebugLog.Log (Color.red, "UIContainer(id: {0}) is not exist.", id);
				return;
			}
			if (UIContainers [id].isDontDestroy) {
				DebugLog.Log (Color.red, "UIContainer(id: {0}) is not destroyable.", id);
				return;
			}
			GameObject.Destroy (UIContainers [id]);
			UIContainers.Remove (id);
		}
	}
}