﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldUIController : CreatedObject {

	public bool isFacingToCamera = true;
	public Camera renderingCamera;

	public override void OnInitialize() {
		base.OnInitialize ();
		renderingCamera = Camera.main;
		this.GetComponent<Canvas>().worldCamera = renderingCamera;
	}

	public override void OnUpdate() {
		base.OnUpdate();
		if (isFacingToCamera) {
			Vector3 delta = - renderingCamera.transform.position + this.transform.position;
			delta.x = delta.z = 0f;
			this.transform.rotation = Quaternion.LookRotation(delta, Vector3.up);
		}
	}
}
