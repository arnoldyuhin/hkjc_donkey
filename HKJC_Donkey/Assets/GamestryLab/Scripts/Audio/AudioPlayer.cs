﻿using GamestryLab.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Audio {
	public class AudioPlayer : MonoBehaviour, IService {
		private Dictionary<string, FMOD.Studio.EventInstance> evtInstances;

		private FMOD.Studio.Bus bgmBus;
		private FMOD.Studio.Bus sfxBus;
		//public bool BGMOnOff {
		//	get { return PlayerPrefs.GetInt ("BGMOnOff", 1) == 1; }
		//	set {
		//		PlayerPrefs.SetInt ("BGMOnOff", value == true ? 1 : 0);
		//		bgmBus.setMute (!BGMOnOff);
		//	}
		//}
		//public bool SEOnOff {
		//	get { return PlayerPrefs.GetInt ("SFXOnOff", 1) == 1; }
		//	set {
		//		PlayerPrefs.SetInt ("SFXOnOff", value == true ? 1 : 0);
		//		sfxBus.setMute (!SEOnOff);
		//	}
		//}
		public float BGMVolume {
			set {
				bgmBus.setVolume (value);
			}
		}
		public float SFXVolume {
			set {
				sfxBus.setVolume (value);
			}
		}

		private void Awake () {
			evtInstances = new Dictionary<string, FMOD.Studio.EventInstance> ();
			bgmBus = FMODUnity.RuntimeManager.GetBus ("bus:/Master/BGM");
			sfxBus = FMODUnity.RuntimeManager.GetBus ("bus:/Master/SFX");
			//bgmBus.setMute (!BGMOnOff);
			//sfxBus.setMute (!SEOnOff);
		}

		public void PlayOneShot (string path) {
			PlayOneShotAtPoint (path, Vector3.zero);
		}

		public void PlayOneShot (string path, GameObject parent) {
			FMODUnity.RuntimeManager.PlayOneShotAttached (path, parent);
		}

		public void PlayOneShotAtPoint (string path, Vector3 point) {
			FMODUnity.RuntimeManager.PlayOneShot (path, point);
		}

		public void Play (string name, string path) {
			FMOD.Studio.EventInstance evtInstance;
			if (evtInstances.TryGetValue (name, out evtInstance) == true) {
				evtInstance.setPaused (false);
			} else {
				evtInstance = FMODUnity.RuntimeManager.CreateInstance (path);
				evtInstance.start ();

				evtInstances.Add (name, evtInstance);
			}
		}

		public void Pause (string name) {
			FMOD.Studio.EventInstance evtInstance;
			if (evtInstances.TryGetValue (name, out evtInstance) == true) {
				evtInstance.setPaused (true);
			} else {
				// Event instance with specified name not existed!
			}
		}

		public void Stop (string name, FMOD.Studio.STOP_MODE mode = FMOD.Studio.STOP_MODE.IMMEDIATE) {
			FMOD.Studio.EventInstance evtInstance;
			if (evtInstances.TryGetValue (name, out evtInstance) == true) {
				evtInstance.stop (mode);
				evtInstance.release ();

				evtInstances.Remove (name);
			} else {
				// Event instance with specified name not existed!
			}
		}

		public bool IsPlaying (string name) {
			FMOD.Studio.EventInstance evtInstance;
			if (evtInstances.TryGetValue (name, out evtInstance) == true) {
				FMOD.Studio.PLAYBACK_STATE state;
				evtInstance.getPlaybackState (out state);

				return (state == FMOD.Studio.PLAYBACK_STATE.PLAYING);
			} else {
				// Event instance with specified name not existed!

				return false;
			}
		}

		public void Mute (bool flag) {
			FMODUnity.RuntimeManager.MuteAllEvents (flag);
		}
	}
}