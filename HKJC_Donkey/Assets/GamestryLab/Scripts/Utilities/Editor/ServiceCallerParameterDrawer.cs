﻿using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using GamestryLab.System;

namespace GamestryLab.Utilities
{
	[CustomPropertyDrawer(typeof(ServiceCallerArgument))]
	public class ServiceCallerParameterDrawer : PropertyDrawer
	{
		private bool initialized = false;
		public bool IsInitialized { get { return initialized; } }

		private float height;

		private void Initialize()
		{
			height = (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * 3f;

			initialized = true;
		}

		// Draw the property inside the given rect
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (IsInitialized == false)
			{
				Initialize();
			}

			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);

			Rect rect;

			SerializedProperty typeProp = property.FindPropertyRelative("type");
			ServiceCallerArgument.ArgumentType type = (ServiceCallerArgument.ArgumentType)typeProp.enumValueIndex;
			SerializedProperty nameProp = property.FindPropertyRelative("name");

			EditorGUI.BeginDisabledGroup(true);
			{
				rect = new Rect(position.x, position.y, position.width, (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing));
				EditorGUI.EnumPopup(rect, new GUIContent("Type"), type);

				rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
				EditorGUI.TextField(rect, new GUIContent("Name"), nameProp.stringValue);
			}
			EditorGUI.EndDisabledGroup();

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);

			SerializedProperty valueProp;
			switch (type)
			{
				case ServiceCallerArgument.ArgumentType.Void:
					break;

				case ServiceCallerArgument.ArgumentType.Object:
					valueProp = property.FindPropertyRelative("objectArgument");
					Type objectArgumentType = Type.GetType(property.FindPropertyRelative("typeAssemblyQualifiedName").stringValue);
					valueProp.objectReferenceValue = EditorGUI.ObjectField(rect, new GUIContent("Value"), valueProp.objectReferenceValue, objectArgumentType, true);
					break;

				case ServiceCallerArgument.ArgumentType.Int:
					valueProp = property.FindPropertyRelative("intArgument");
					valueProp.intValue = EditorGUI.IntField(rect, new GUIContent("Value"), valueProp.intValue);
					break;

				case ServiceCallerArgument.ArgumentType.Float:
					valueProp = property.FindPropertyRelative("floatArgument");
					valueProp.floatValue = EditorGUI.FloatField(rect, new GUIContent("Value"), valueProp.floatValue);
					break;

                case ServiceCallerArgument.ArgumentType.Vector2:
                    valueProp = property.FindPropertyRelative("vector2Argument");
                    valueProp.vector2Value = EditorGUI.Vector2Field(rect, new GUIContent("Value"), valueProp.vector2Value);
                    break;

                case ServiceCallerArgument.ArgumentType.Vector3:
                    valueProp = property.FindPropertyRelative("vector3Argument");
                    valueProp.vector3Value = EditorGUI.Vector3Field(rect, new GUIContent("Value"), valueProp.vector3Value);
                    break;

                case ServiceCallerArgument.ArgumentType.String:
					valueProp = property.FindPropertyRelative("stringArgument");
					valueProp.stringValue = EditorGUI.TextField(rect, new GUIContent("Value"), valueProp.stringValue);
					break;

				case ServiceCallerArgument.ArgumentType.Bool:
					valueProp = property.FindPropertyRelative("boolArgument");
					valueProp.boolValue = EditorGUI.Toggle(rect, new GUIContent("Value"), valueProp.boolValue);
					break;
			}

			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return height;
		}
	}
}