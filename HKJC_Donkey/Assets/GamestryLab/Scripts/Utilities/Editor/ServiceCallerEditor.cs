﻿using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Reflection;

namespace GamestryLab.Utilities
{
    [CustomEditor(typeof(ServiceCaller))]
    public class ServiceCallerEditor : Editor
    {
        private Type[] types;
        private string[] typeAssemblyQualifiedNames;
        private string[] typeFullNames;

        private List<MethodInfo> methods;
        private string[] methodNames;
        private string[] methodInformations;

        private bool[] foldouts;

        private SerializedProperty typeAssemblyQualifiedNameProp;
        private SerializedProperty typeFullNameProp;
        private SerializedProperty methodNameProp;
		private SerializedProperty methodInformationProp;
		private SerializedProperty argumentsProp;

        private const int Invalid = -1;
        private const string NotYetBeenAssignedErrorMessageFormat = "Field {0} has not yet been assigned! Please assign it before start.";
        private const string NoAvailableItemWarningMessage = "No Available Item!";

        void OnEnable()
        {
            types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IService).IsAssignableFrom(p)).ToArray();

            typeAssemblyQualifiedNames = new string[types.Length];
            typeFullNames = new string[types.Length];
            for (int i = 0; i < types.Length; i++)
            {
                typeAssemblyQualifiedNames[i] = types[i].AssemblyQualifiedName;
                typeFullNames[i] = types[i].FullName;
                typeFullNames[i] = typeFullNames[i].Replace('.', '/');
            }

            methods = new List<MethodInfo>();
            methodNames = new string[0];
            methodInformations = new string[0];

            foldouts = new bool[1];

            // Setup the SerializedProperties
            typeAssemblyQualifiedNameProp = serializedObject.FindProperty("typeAssemblyQualifiedName");
            typeFullNameProp = serializedObject.FindProperty("typeFullName");
            methodNameProp = serializedObject.FindProperty("methodName");
			methodInformationProp = serializedObject.FindProperty("methodInformation");
			argumentsProp = serializedObject.FindProperty("arguments");

			OnChanged();
		}

        public override void OnInspectorGUI()
        {
            // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
            serializedObject.Update();

            if (types.Length == 0)
            {
                EditorGUILayout.HelpBox(NoAvailableItemWarningMessage, MessageType.Warning);
            }
            else
            {
                int index;
                int length;

				bool typeHasChanged = false;
				bool methodHasChanged = false;

				#region Type
				{
					EditorGUI.BeginChangeCheck();
					{
						index = Array.FindIndex(types, (t => t.AssemblyQualifiedName == typeAssemblyQualifiedNameProp.stringValue));
						index = EditorGUILayout.Popup("Type", index, typeFullNames);
                    }
					if (EditorGUI.EndChangeCheck() == true)
					{
						typeHasChanged = true;

						typeAssemblyQualifiedNameProp.stringValue = types[index].AssemblyQualifiedName;
                        typeFullNameProp.stringValue = types[index].FullName;
                    }

                    if (index == Invalid)
                    {
                        EditorGUILayout.HelpBox(string.Format(NotYetBeenAssignedErrorMessageFormat, "type"), MessageType.Error);
                    }
                }
				#endregion

				#region Method
				{
					if (typeHasChanged == true)
					{
						OnChanged();
					}

					EditorGUI.BeginChangeCheck();
					{
						index = methods.FindIndex(m => m.ToString() == methodInformationProp.stringValue);
                        index = EditorGUILayout.Popup("Method", index, methodInformations); 
					}
					if (EditorGUI.EndChangeCheck() == true)
					{
						methodHasChanged = true;

						methodNameProp.stringValue = methodNames[index];
						methodInformationProp.stringValue = methods[index].ToString();
					}

                    if (index == Invalid)
                    {
                        EditorGUILayout.HelpBox(string.Format(NotYetBeenAssignedErrorMessageFormat, "method"), MessageType.Error);
                    }
                }
				#endregion

				#region Arguments
				{
					if (methodHasChanged == true)
					{
						OnChanged();
					}

					foldouts[0] = EditorGUILayout.Foldout(foldouts[0], "Arguments");
					if (foldouts[0] == true)
					{
						length = argumentsProp.arraySize;
						for (int i = 0; i < length; i++)
						{
							EditorGUI.indentLevel++;
							{
								EditorGUILayout.LabelField(new GUIContent(string.Format("Element {0}", i)));

								EditorGUI.indentLevel++;
								{
									SerializedProperty argumentProp = argumentsProp.GetArrayElementAtIndex(i);
									EditorGUILayout.PropertyField(argumentProp);
								}
								EditorGUI.indentLevel--;
							}
							EditorGUI.indentLevel--;
						}
					}
				}
				#endregion
			}

			// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
			serializedObject.ApplyModifiedProperties();
        }

		private void OnChanged()
		{
			int index;
			int length;

			index = Invalid;
			length = Invalid;

			bool typeHasSelected = false;
			bool methodHasSelected = false;

			#region Type
			{
				index = Array.FindIndex(types, (t => t.AssemblyQualifiedName == typeAssemblyQualifiedNameProp.stringValue));
				if (index != Invalid)
				{
					typeHasSelected = true;
				}
			}
			#endregion

			#region Method
			{
				if (typeHasSelected == true) 
				{
					Type type = types[index];
					methods = type.GetMethods().ToList();

					length = methods.Count;
					for (int i = length - 1; i >= 0; i--)
					{
						if (methods[i].IsPrivate == true || 
							methods[i].IsGenericMethod == true)
						{
							methods.RemoveAt(i);
							continue;
						}

						if (methods[i].Name.IndexOf("get_") != Invalid)
						{
							methods.RemoveAt(i);
							continue;
						}

						if (methods[i].ReturnType != typeof(void))
						{
							methods.RemoveAt(i);
							continue;
						}

						//ParameterInfo[] parameters = methods[i].GetParameters();
						//foreach (ParameterInfo parameter in parameters)
						//{
						//	if (parameter.ParameterType.IsArray == true)
						//	{
						//		methods.RemoveAt(i);
						//		break;
						//	}

						//	if (parameter.ParameterType.IsClass == true && parameter.ParameterType.IsAssignableFrom(typeof(Component)) == false)
						//	{
						//		methods.RemoveAt(i);
						//		break;
						//	}
						//}
					}

					length = methods.Count;
					methodNames = new string[length];
					methodInformations = new string[length];

					for (int i = length - 1; i >= 0; i--)
					{
						methodNames[i] = methods[i].Name;
						methodInformations[i] = methods[i].ToString();
					}

					//for (int i = 0; i < length; i++)
					//{
					//    Debug.LogFormat("Method Information: {0}, Declaring Type: {1}, Parametor Length: {2}", methods[i].ToString(), methods[i].DeclaringType, methods[i].GetParameters().Length);
					//}
				}

				index = methods.FindIndex(m => m.ToString() == methodInformationProp.stringValue);
				if (index != Invalid)
				{
					methodHasSelected = true;
				}
			}
			#endregion

			#region Arguments
			{
				if (methodHasSelected == true)
				{
					ParameterInfo[] parameters = methods[index].GetParameters();

					length = parameters.Length;

					argumentsProp.arraySize = length;
					for (int i = 0; i < length; i++)
					{
						SerializedProperty parameterProp = argumentsProp.GetArrayElementAtIndex(i);
						switch (parameters[i].ParameterType)
						{
							case Type t when t == typeof(void):
								parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.Void;
								break;

							case Type t when
							t == typeof(byte) || t == typeof(sbyte) ||
							t == typeof(short) || t == typeof(ushort) ||
							t == typeof(int) || t == typeof(uint) ||
							t == typeof(long) || t == typeof(ulong):
								parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.Int;
								break;

							case Type t when
							t == typeof(float) ||
							t == typeof(double) ||
							t == typeof(decimal):
								parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.Float;
								break;

                            case Type t when t == typeof(Vector2):
                                parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.Vector2;
                                break;

                            case Type t when t == typeof(Vector3):
                                parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.Vector3;
                                break;

                            case Type t when t == typeof(string):
								parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.String;
								break;

							case Type t when t == typeof(bool):
								parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.Bool;
								break;

							default:
                                parameterProp.FindPropertyRelative("type").enumValueIndex = (int)ServiceCallerArgument.ArgumentType.Object;
								break;
						}
                        parameterProp.FindPropertyRelative("typeAssemblyQualifiedName").stringValue = parameters[i].ParameterType.AssemblyQualifiedName;
                        parameterProp.FindPropertyRelative("typeFullName").stringValue = parameters[i].ParameterType.FullName;
                        parameterProp.FindPropertyRelative("name").stringValue = parameters[i].Name;
					}
				}
			}
			#endregion
		}
	}
}