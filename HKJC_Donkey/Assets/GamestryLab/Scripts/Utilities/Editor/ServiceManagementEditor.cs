﻿using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

namespace GamestryLab.Utilities
{
    [CustomEditor(typeof(ServiceManagement))]
    public class ServiceManagementEditor : Editor
    {
        private Type[] types;
        private string[] fullPaths;

        private bool[] foldouts;

        private List<int> toBeRemoves;

        private SerializedProperty registerOnAwakeFullNamesProp;
        private SerializedProperty unregisterOnDestroyFullNamesProp;

        private const int Invalid = -1;
        private const string ServiceNotFoundErrorMessage = "Service not found from above item! Please select another service before start to play.";
        private const string NoAvailableItemWarningMessage = "No Available Item!";
        private const string HasDuplicateItemsWarningMessage = "Has Duplicate Items!";

        void OnEnable()
        {
            ServiceManagement registrar = (target as ServiceManagement);

            types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IService).IsAssignableFrom(p)).ToArray();

            fullPaths = new string[types.Length];
            for (int i = 0; i < fullPaths.Length; i++)
            {
                fullPaths[i] = types[i].FullName;
                fullPaths[i] = fullPaths[i].Replace('.', '/');
            }

            foldouts = new bool[2];

            toBeRemoves = new List<int>();

            // Setup the SerializedProperties
            registerOnAwakeFullNamesProp = serializedObject.FindProperty("registerOnAwakeFullNames");
            unregisterOnDestroyFullNamesProp = serializedObject.FindProperty("unregisterOnDestroyFullNames");
        }

        public override void OnInspectorGUI()
        {
            // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
            serializedObject.Update();

            ServiceManagement management = (target as ServiceManagement);

            if (types.Length == 0)
            {
                EditorGUILayout.HelpBox(NoAvailableItemWarningMessage, MessageType.Warning);
            }
            else
            {
                #region Register on Awake
                foldouts[0] = EditorGUILayout.Foldout(foldouts[0], "Register on Awake");
                if (foldouts[0] == true)
                {
                    DrawServicesProperty(registerOnAwakeFullNamesProp);
                }
                #endregion

                #region Unregister on Destroy
                foldouts[1] = EditorGUILayout.Foldout(foldouts[1], "Unregister on Destroy");
                if (foldouts[1] == true)
                {
                    DrawServicesProperty(unregisterOnDestroyFullNamesProp);
                }
                #endregion

                if (management.HasDuplicateItems == true)
                {
                    EditorGUILayout.HelpBox(HasDuplicateItemsWarningMessage, MessageType.Warning);
                }
            }

            // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawServicesProperty(SerializedProperty prop)
        {
            if (prop.isArray == true)
            {
                int length = -1;
                int index = -1;

                toBeRemoves.Clear();

                length = prop.arraySize;
                for (int i = 0; i < length; i++)
                {
                    EditorGUILayout.BeginHorizontal(GUI.skin.box);
                    {
                        EditorGUIUtility.labelWidth = 80f;
                        EditorGUILayout.PrefixLabel(string.Format("Element {0}", i), new GUIStyle(GUI.skin.label) { fixedWidth = 80f, fixedHeight = 20f, stretchWidth = false, alignment = TextAnchor.MiddleLeft });
                        EditorGUIUtility.labelWidth = 0f;

                        SerializedProperty fullNameProp = prop.GetArrayElementAtIndex(i);

                        index = Array.FindIndex(types, (t => t.FullName == fullNameProp.stringValue));
                        index = EditorGUILayout.Popup(index, fullPaths, new GUIStyle("popup") { fixedHeight = 20f, stretchWidth = true, alignment = TextAnchor.MiddleLeft });

                        if (index != Invalid)
                        {
                            fullNameProp.stringValue = types[index].FullName;
                        }

                        if (GUILayout.Button("-", new GUIStyle(GUI.skin.button) { fixedWidth = 40f, fixedHeight = 20f, stretchWidth = false, alignment = TextAnchor.MiddleCenter }))
                        {
                            toBeRemoves.Add(i);
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                    if (index == Invalid)
                    {
                        EditorGUILayout.HelpBox(ServiceNotFoundErrorMessage, MessageType.Error);
                    }
                }

                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.FlexibleSpace();
                    if (GUILayout.Button("+", GUILayout.Width(40f), GUILayout.Height(20f), GUILayout.ExpandWidth(false)))
                    {
                        prop.InsertArrayElementAtIndex(length);
                        prop.GetArrayElementAtIndex(length).stringValue = types[0].FullName;
                    }
                }
                EditorGUILayout.EndHorizontal();

                length = toBeRemoves.Count;
                for (int i = 0; i < length; i++)
                {
                    index = toBeRemoves[i];
                    prop.DeleteArrayElementAtIndex(index);
                }
            }
        }
    }
}