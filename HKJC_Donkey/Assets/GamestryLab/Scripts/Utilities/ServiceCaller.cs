﻿using GamestryLab.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

namespace GamestryLab.Utilities
{
    public class ServiceCaller : MonoBehaviour
    {
        [SerializeField] private string typeAssemblyQualifiedName;
        [SerializeField] private string typeFullName;
        [SerializeField] private string methodName;
        [SerializeField] private string methodInformation;
        [SerializeField] private ServiceCallerArgument[] arguments;

        private string key;

        private void Start()
        {
            if (string.IsNullOrEmpty(typeAssemblyQualifiedName) == true ||
                string.IsNullOrEmpty(typeAssemblyQualifiedName) == true ||
                string.IsNullOrEmpty(methodName) == true ||
                string.IsNullOrEmpty(methodInformation) == true)
            {
                // todo: Warning
                return;
            }

            key = string.Format("{0}.{1}", typeFullName, methodInformation);

            Type type = Type.GetType(typeAssemblyQualifiedName);

            int length = arguments.Length;

            Type[] argumentTypes = new Type[length];
            for (int i = 0; i < length; i++)
            {
                argumentTypes[i] = Type.GetType(arguments[i].TypeAssemblyQualifiedName);
            }

			IService firstArgument = ServiceLocator.GetService(type);
            MethodInfo method = type.GetMethod(methodName, argumentTypes);

            Delegate @delegate = null; 
            Delegate castedDelegate = null;
            switch (arguments.Length)
            {
                case 0:
                    @delegate = Delegate.CreateDelegate(typeof(Action), firstArgument, method);
                    castedDelegate = (Delegate)typeof(DelegateFactory).GetMethod("CastAction", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] { @delegate });
                    break;

                case 1:
                    @delegate = Delegate.CreateDelegate(typeof(Action<>).MakeGenericType(argumentTypes), firstArgument, method);
                    castedDelegate = (Delegate)typeof(DelegateFactory).GetMethod("CastActionHasOneArg", BindingFlags.Static | BindingFlags.NonPublic).MakeGenericMethod(argumentTypes).Invoke(null, new object[] { @delegate });
                    break;

                case 2:
                    @delegate = Delegate.CreateDelegate(typeof(Action<,>).MakeGenericType(argumentTypes), firstArgument, method);
                    castedDelegate = (Delegate)typeof(DelegateFactory).GetMethod("CastActionHasTwoArgs", BindingFlags.Static | BindingFlags.NonPublic).MakeGenericMethod(argumentTypes).Invoke(null, new object[] { @delegate });
                    break;

                case 3:
                    @delegate = Delegate.CreateDelegate(typeof(Action<, ,>).MakeGenericType(argumentTypes), firstArgument, method);
                    castedDelegate = (Delegate)typeof(DelegateFactory).GetMethod("CastActionHasThreeArgs", BindingFlags.Static | BindingFlags.NonPublic).MakeGenericMethod(argumentTypes).Invoke(null, new object[] { @delegate });
                    break;
            }

            ServiceLocator.GetService<DelegateFactory>().Add(key, castedDelegate);
        }

		[ContextMenu("Execute")]
		public void Execute()
		{
            Delegate @delegate = null;
            if (ServiceLocator.GetService<DelegateFactory>().TryGetDelegate(key, out @delegate) == true)
            {
                switch (arguments.Length)
                {
                    case 0:
                        ((Action)@delegate).Invoke();
                        break;

                    case 1:
                        ((Action<object>)@delegate).Invoke(arguments[0].Value);
                        break;

                    case 2:
                        ((Action<object, object>)@delegate).Invoke(arguments[0].Value, arguments[1].Value);
                        break;

                    case 3:
                        ((Action<object, object, object>)@delegate).Invoke(arguments[0].Value, arguments[1].Value, arguments[2].Value);
                        break;
                }
            }
            else 
            {
                // todo: Warning
            }
        }

        //public void Backup()
        //{
        //    Type type = Type.GetType(typeAssemblyQualifiedName);

        //    int length = arguments.Length;

        //    Type[] argumentTypes = new Type[length];
        //    for (int i = 0; i < length; i++)
        //    {
        //        argumentTypes[i] = Type.GetType(arguments[i].TypeAssemblyQualifiedName);
        //    }

        //    MethodInfo method = type.GetMethod(methodName, argumentTypes);

        //    Delegate @delegate = Delegate.CreateDelegate(typeof(Action<>).MakeGenericType(argumentTypes), this, method);

        //    Action<object> action = (Action<object>)GetType()
        //        .GetMethod("CastAction", BindingFlags.Static | BindingFlags.NonPublic)
        //        .MakeGenericMethod(argumentTypes)
        //        .Invoke(null, new object[] { @delegate });

        //    Stopwatch stopwatch = Stopwatch.StartNew();

        //    stopwatch.Start();
        //    action.Invoke(null);
        //    stopwatch.Stop();

        //    UnityEngine.Debug.LogFormat("First Call: {0} tick(s)", stopwatch.ElapsedTicks);

        //    stopwatch.Reset();
        //    stopwatch.Start();
        //    for (var i = 0; i < Iterations; i++)
        //    {
        //        action.Invoke(null);
        //    }
        //    stopwatch.Stop();

        //    UnityEngine.Debug.LogFormat("Next {0} Calls: {1} tick(s)", Iterations, stopwatch.ElapsedTicks);
        //}
    }
}