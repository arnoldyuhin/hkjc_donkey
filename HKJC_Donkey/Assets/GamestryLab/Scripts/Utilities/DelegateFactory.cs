﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Utilities
{
    public class DelegateFactory : MonoBehaviour, IService
    {
        private Dictionary<string, Delegate> delegates;

        private void Awake()
        {
            delegates = new Dictionary<string, Delegate>();
        }

        public void Add(string key, Delegate @delegate)
        {
            if (delegates.ContainsKey(key) == true)
            {
                // Do-nothing
            }
            else
            {
                delegates.Add(key, @delegate);
            }
        }

        public void Remove(string key)
        {
            if (delegates.ContainsKey(key) == true)
            {
                delegates.Remove(key);
            }
            else
            {
                // Do-nothing
            }
        }

        public bool TryGetDelegate(string key, out Delegate @delegate)
        {
            return delegates.TryGetValue(key, out @delegate);
        }

        private static Action CastAction(Delegate @delegate)
        {
            Action action = (Action)@delegate;
            return () => action();
        }

        private static Action<object> CastActionHasOneArg<T>(Delegate @delegate)
        {
            Action<T> action = (Action<T>)@delegate;
            return (arg) => action((T)arg);
        }

        private static Action<object, object> CastActionHasTwoArgs<T1, T2>(Delegate @delegate)
        {
            Action<T1, T2> action = (Action<T1, T2>)@delegate;
            return (arg1, arg2) => action((T1)arg1, (T2)arg2);
        }

        private static Action<object, object, object> CastActionHasThreeArgs<T1, T2, T3>(Delegate @delegate)
        {
            Action<T1, T2, T3> action = (Action<T1, T2, T3>)@delegate;
            return (arg1, arg2, arg3) => action((T1)arg1, (T2)arg2, (T3)arg3);
        }
    }
}