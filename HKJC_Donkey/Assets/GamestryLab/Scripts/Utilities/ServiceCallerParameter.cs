﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GamestryLab.Utilities
{
	[Serializable]
    public class ServiceCallerArgument 
    {
		public enum ArgumentType
		{
			Void,
			Object,
			Int,
			Float,
            Vector2,
            Vector3,
			String,
			Bool
		}

		[SerializeField] private ArgumentType type;
        [SerializeField] private string typeAssemblyQualifiedName;
        [SerializeField] private string typeFullName;
        [SerializeField] private string name;

		[SerializeField] private UnityEngine.Object objectArgument;
		[SerializeField] private int intArgument;
		[SerializeField] private float floatArgument;
        [SerializeField] private Vector2 vector2Argument;
        [SerializeField] private Vector3 vector3Argument;
        [SerializeField] private string stringArgument;
		[SerializeField] private bool boolArgument;

        public string TypeAssemblyQualifiedName { get { return typeAssemblyQualifiedName; } }

        public object Value
        {
            get
            {
                switch (type)
                {
                    case ArgumentType.Int:
                        return intArgument;

                    case ArgumentType.Float:
                        return floatArgument;

                    case ArgumentType.Vector2:
                        return vector2Argument;

                    case ArgumentType.Vector3:
                        return vector3Argument;

                    case ArgumentType.String:
                        return stringArgument;

                    case ArgumentType.Bool:
                        return boolArgument;

                    default:
                        return objectArgument;
                }
            }

			set
			{
				switch (type)
				{
					case ArgumentType.Int:
						intArgument = (int)value;
						break;

					case ArgumentType.Float:
						floatArgument = (float)value;
						break;

					case ArgumentType.Vector2:
						vector2Argument = (Vector2)value;
						break;

					case ArgumentType.Vector3:
						vector3Argument = (Vector3)value;
						break;

					case ArgumentType.String:
						stringArgument = (string)value;
						break;

					case ArgumentType.Bool:
						boolArgument = (bool)value;
						break;

					default:
						objectArgument = (UnityEngine.Object)value;
						break;
				}
			}
        }
	}
}