﻿using System;
using UnityEngine;

namespace GamestryLab.Utilities
{
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        private static T instance;

        public event Action Enabled;
        public event Action Disabled;

        public static T Instance 
		{
            get 
			{ 
				if (instance == null) 
				{
					instance = GameObject.FindObjectOfType<T>();
					if (instance == null)
					{
						Debug.LogWarningFormat("Could not found MonoSingleton Class on Singleton Object ({0}).", typeof(T).FullName);

                        instance = new GameObject(typeof(T).Name).AddComponent<T>();
                        Debug.LogWarningFormat("System auto-generated a new Singleton Object ({0}) to prevent runtime error.", typeof(T).FullName);
                    }
				}

				return instance; 
			}
        }

        protected virtual void Awake()
        {
            instance = (T)this;
        }

		protected virtual void OnDestroy() {}

        public virtual void Enable()
        {
            gameObject.SetActive(true);

            if (Enabled != null)
			{
                Enabled();
            }
        }

        public virtual void Disable()
        {
            if (Disabled != null)
			{
                Disabled();
            }

            gameObject.SetActive(false);
        }

        public void DontDestroyOnLoad()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
