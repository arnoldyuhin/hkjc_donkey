﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace GamestryLab.Utilities
{
    public class ServiceManagement : MonoBehaviour
    {
        [SerializeField] private string[] registerOnAwakeFullNames;
        [SerializeField] private string[] unregisterOnDestroyFullNames;

        public bool HasDuplicateItems = false;

        private void Awake()
        {
            foreach (string fullName in registerOnAwakeFullNames)
            {
                Type type = Type.GetType(fullName);

                MethodInfo method = typeof(ServiceLocator).GetMethod("Register", new Type[0]);
                MethodInfo genericMethod = method.MakeGenericMethod(type);

                genericMethod.Invoke(this, null);
            }
        }

        private void OnDestroy()
        {
            foreach (string fullName in unregisterOnDestroyFullNames)
            {
                Type type = Type.GetType(fullName);

                MethodInfo method = typeof(ServiceLocator).GetMethod("Unregister", new Type[0]);
                MethodInfo genericMethod = method.MakeGenericMethod(type);

                genericMethod.Invoke(this, null);
            }
        }

        private void OnValidate()
        {
            HasDuplicateItems = false;
            if (registerOnAwakeFullNames != null && registerOnAwakeFullNames.GroupBy(x => x).Any(g => g.Count() > 1) == true) { HasDuplicateItems = true; }
            if (unregisterOnDestroyFullNames != null && unregisterOnDestroyFullNames.GroupBy(x => x).Any(g => g.Count() > 1) == true) { HasDuplicateItems = true; }
        }
    }
}