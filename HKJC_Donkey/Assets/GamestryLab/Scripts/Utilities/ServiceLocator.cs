﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamestryLab.System;

namespace GamestryLab.Utilities
{
    public class ServiceLocator : MonoSingleton<ServiceLocator>
    {
        private IDictionary<Type, IService> services;

        protected override void Awake()
        {
			DontDestroyOnLoad(this.gameObject);

            services = new Dictionary<Type, IService>();
        }

        protected void Update()
        {
            foreach (IService service in services.Values)
            {
                if (service is IManualUpdatable)
                {
                    (service as IManualUpdatable).ManualUpdate();
                }
            }
        }

        public static void Register(IService service) 
        {
            Type type = service.GetType();

            IService value;
            if (Instance.services.TryGetValue(type, out value) == false)
            {
                if (service is MonoBehaviour)
                {
                    (service as MonoBehaviour).transform.SetParent(Instance.transform, false);
                }

                Instance.services.Add(type, service);
            }
        }

        public static void Register<T>() where T : MonoBehaviour, IService
        {
            Type type = typeof(T);

            IService value;
            if (Instance.services.TryGetValue(type, out value) == false)
            {
                GameObject instance = new GameObject(typeof(T).FullName);
                instance.transform.SetParent(Instance.transform, false);

                value = instance.AddComponent<T>();
                Instance.services.Add(typeof(T), value);
            }
        }

        public static void Unregister(IService service) 
        {
            Type type = service.GetType();

            IService value;
            if (Instance.services.TryGetValue(type, out value) == true)
            {
                if (service is MonoBehaviour)
                {
                    Destroy((service as MonoBehaviour).gameObject);
                }

                Instance.services.Remove(type);
            }
        }

        public static void Unregister<T>() where T : MonoBehaviour, IService
        {
            Type type = typeof(T);

            IService value;
            if (Instance.services.TryGetValue(type, out value) == true)
            {
                if (value is MonoBehaviour)
                {
                    Destroy((value as MonoBehaviour).gameObject);
                }

                Instance.services.Remove(type);
            }
        }

        public static T GetService<T>() where T : IService
        {
            try
            {
                return (T)Instance.services[typeof(T)];
            }
            catch (KeyNotFoundException)
            {
                throw new ApplicationException(string.Format("The requested service ({0}) is not registered", typeof(T).FullName));
            }
        }

		public static IService GetService(Type type)
		{
			try
			{
				return Instance.services[type];
			}
			catch (KeyNotFoundException)
			{
				throw new ApplicationException(string.Format("The requested service ({0}) is not registered", type.FullName));
			}
		}
    }
}