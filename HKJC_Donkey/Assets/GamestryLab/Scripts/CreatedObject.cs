﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamestryLab.Debugger;

public class CreatedObject : MonoBehaviour {

	public bool isShowingCreatedObjectLog = false;

	public Action<CreatedObject> preloadHandler;
	public Action<CreatedObject> initializeHandler;
	public Action<CreatedObject> fixedUpdateHandler;
	public Action<CreatedObject> updateHandler;
	public Action<CreatedObject> tickUpdateHandler;
	public Action<CreatedObject> destroyHandler;
	public Action<CreatedObject> terminateHandler;
	public Action<CreatedObject> resetHandler;
	public Action<CreatedObject> unloadHandler;

	private Transform m_transform;
	public Transform trans {
		get {
			if (m_transform == null) {
				m_transform = this.transform;
			}
			return m_transform;
		}
		set {
			if (m_transform == null) {
				m_transform = this.transform;
			}
			m_transform = value;
		}
	}

	//essential
	public virtual void OnPreload () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnPreload");
		}
		if (preloadHandler != null) {
			preloadHandler (this);
		}
	}

	public virtual void OnInitialize () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnInitialize");
		}
		if (initializeHandler != null) {
			initializeHandler (this);
		}
	}

	public virtual void OnFixedUpdate () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnFixedUpdate");
		}
		if (fixedUpdateHandler != null) {
			fixedUpdateHandler (this);
		}
	}

	public virtual void OnUpdate () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnUpdate");
		}
		if (updateHandler != null) {
			updateHandler (this);
		}
	}

	public virtual void OnTickUpdate () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnTickUpdate");
		}
		if (tickUpdateHandler != null) {
			tickUpdateHandler (this);
		}
	}

	public virtual void OnDestroy () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnDestroy");
		}
		if (destroyHandler != null) {
			destroyHandler (this);
		}
	}

	public virtual void OnTerminate () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnTerminate");
		}
		if (terminateHandler != null) {
			terminateHandler (this);
		}
	}

	public virtual void OnReset () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnReset");
		}
		if (resetHandler != null) {
			resetHandler (this);
		}
	}

	public virtual void OnUnload () {
		if (isShowingCreatedObjectLog) {
			DebugLog.Log("OnUnload");
		}
		if (unloadHandler != null) {
			unloadHandler (this);
		}
	}

	// public virtual void DebugLog.Logstring _message) {
	// 	Debug.Log (this.GetDebugName () + " : " + _message);
	// }

	// public virtual void DebugLog.Logstring _message, string _color) {
	// 	Debug.Log ("<color=" + _color + ">" + this.GetDebugName () + " : " + _message + "</color>");
	// }

	public virtual string GetDebugName () {
		return this.name + "(" + this.GetType () + ")";
	}
}