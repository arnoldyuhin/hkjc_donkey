﻿using GamestryLab.Core;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamestryLab.Operations
{
    public class IntExpression : Expression 
    {
        [SerializeField] private IntParameter parameter;
        [SerializeField] private ComparisonOperator comparisonOperator;
        [SerializeField] private int targetValue;

        public override bool IsFulfilled
        {
            get
            {
                switch (comparisonOperator)
                {
                    case ComparisonOperator.EqualTo:
                        return (parameter.Value == targetValue);

                    case ComparisonOperator.NotEqualTo:
                        return (parameter.Value != targetValue);

                    case ComparisonOperator.GreaterThan:
                        return (parameter.Value > targetValue);

                    case ComparisonOperator.GreaterThanOrEqualTo:
                        return (parameter.Value >= targetValue);

                    case ComparisonOperator.LessThan:
                        return (parameter.Value < targetValue);

                    case ComparisonOperator.LessThanOrEqualTo:
                        return (parameter.Value <= targetValue);
                }
                return false;
            }
        }
    }
}