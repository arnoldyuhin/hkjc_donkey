﻿using GamestryLab.Core;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamestryLab.Operations
{
    public class IntParameter : Parameter
    {
        [SerializeField] private int value;
        public int Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public void Add(int amount)
        {
            Value += amount;
        }

        public void Substract(int amount)
        {
            Value -= amount;
        }

        public void Multiply(int amount)
        {
            Value *= amount;
        }

        public void Divide(int amount)
        {
            Value /= amount;
        }
    }
}