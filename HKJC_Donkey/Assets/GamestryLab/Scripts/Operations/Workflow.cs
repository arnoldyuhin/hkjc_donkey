﻿using GamestryLab.Core;
using GamestryLab.Debugger;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class Workflow : MonoBehaviour, IManualUpdatable
    {
        public UnityObjectEvent Began;
        public UnityObjectEvent Ended;
        public UnityObjectEvent Terminated;

        [SerializeField] private Parameter[] parameters;
        [SerializeField] private InitialNode initialNode;
        [SerializeField] private ActivityFinalNode finalNode;

        private List<Node> runningNodes;

        public bool IsRunning
        {
            get { return (runningNodes.Count > 0); }
        }

        public event Action ManualUpdated;

        private void Awake()
        {
            runningNodes = new List<Node>();
        }

        private void Start()
        {
            Validate();
        }

        private void OnDestroy()
        {
            if (IsRunning == true)
            {
                Terminate();
            }
        }

        [ContextMenu("Validate")]
        private void Validate()
        {
            //int count = 1;

            //List<Node> pendingNodes = new List<Node> { initialNode };
            //while (pendingNodes.Count > 0)
            //{
            //    Node node = pendingNodes[0];
            //    if (node is ForkNode)
            //    {
            //        foreach (Node outgoingNode in (node as ForkNode).OutgoingNodes)
            //        {
            //            count++;
            //            pendingNodes.Add(outgoingNode);
            //        }
            //    }
            //    else
            //    {
            //        if (node.NextNode == null)
            //        {
            //            if (node is FinalNode)
            //            {
            //                // Do-nothing
            //            }
            //            else
            //            {
            //                // Warning
            //            }
            //        }
            //        else
            //        {
            //            if (node.NextNode is JoinNode)
            //            {
            //                (node.NextNode as JoinNode).IncomingNodes.Add(node);

            //                bool exists = pendingNodes.Exists(n => n == node.NextNode);
            //                if (exists == false)
            //                {
            //                    count++;
            //                    pendingNodes.Add(node.NextNode);
            //                }
            //            }
            //            else
            //            {
            //                count++;
            //                pendingNodes.Add(node.NextNode);
            //            }
            //        }
            //    }

            //    pendingNodes.Remove(node);
            //}

            //Debug.LogFormat("#{0} [{1}] {2} node count: {3}.", Time.frameCount, GetType().FullName, name, count);
        }

        public void ManualUpdate()
        {
            OnManualUpdated();
        }

        private void OnManualUpdated()
        {
            ManualUpdated?.Invoke();
        }

        [ContextMenu("Begin")]
        public void Begin()
        {
            runningNodes.Clear();

            OnBegan();

            DebugLog.Log("#{0} [{1}] {2} Began.", Time.frameCount, GetType().FullName, name);

            finalNode.Exited += FinalNode_Exited;

            Transit(null, initialNode);
        }

        private void OnBegan()
        {
            Began?.Invoke(this);
        }

        [ContextMenu("End")]
        public void End()
        {
            int length = runningNodes.Count;
            for (int i = 0; i < length; i++)
            {
                Node node = runningNodes[0];
                runningNodes.Remove(node);

                if (node is IManualUpdatable)
                {
                    ManualUpdated -= (node as IManualUpdatable).ManualUpdate;
                }
                node.Exited -= RunningNode_Exited;

                node.Exit(null);
            }

            DebugLog.Log("#{0} [{1}] {2} Ended.", Time.frameCount, GetType().FullName, name);

            OnEnded();
        }

        private void OnEnded()
        {
            Ended?.Invoke(this);
        }

        public void Terminate()
        {
            int length = runningNodes.Count;
            for (int i = 0; i < length; i++)
            {
                Node node = runningNodes[0];
                runningNodes.Remove(node);

                if (node is IManualUpdatable)
                {
                    ManualUpdated -= (node as IManualUpdatable).ManualUpdate;
                }
                node.Exited -= RunningNode_Exited;

                node.Exit(null);
            }

            DebugLog.Log("#{0} [{1}] {2} Terminated.", Time.frameCount, GetType().FullName, name);
        }

        private void OnTerminated()
        {
            Terminated?.Invoke(this);
        }

        [ContextMenu("Fast Forward")]
        public void FastForward()
        {
            int loop = 0;
            while (IsRunning == true && loop < 100)
            {
                int length = runningNodes.Count;
                for (int i = length - 1; i >= 0; i--)
                {
                    Node node = runningNodes[i];
                    if (node is DelayNode || node is InputNode)
                    {
                        node.Exit();
                    }
                }

                loop++;
            }
        }

        private void FinalNode_Exited(object sender, Node toNode)
        {
            finalNode.Exited -= FinalNode_Exited;

            End();
        }

        private void Transit(Node fromNode, Node toNode)
        {
            if (fromNode == null)
            {
                // Do-nothing
            }
            else
            {
                bool exception = false;

                if (exception == false && fromNode.IsProcessing == false)
                { 
                    runningNodes.Remove(fromNode);

                    if (fromNode is IManualUpdatable)
                    {
                        ManualUpdated -= (fromNode as IManualUpdatable).ManualUpdate;
                    }
                    fromNode.Exited -= RunningNode_Exited;
                }
            }

            if (toNode == null)
            {
                // Do-nothing
            }
            else
            {
                bool exception = false;
                if (toNode is ActivityFinalNode)
                {
                    exception = true;
                }

                if (exception == false && toNode.IsProcessing == false)
                {
                    runningNodes.Add(toNode);

                    if (toNode is IManualUpdatable)
                    {
                        ManualUpdated += (toNode as IManualUpdatable).ManualUpdate;
                    }
                    toNode.Exited += RunningNode_Exited;

                    toNode.Reset();
                }

                toNode.Enter(fromNode);
            }
        }

        private void RunningNode_Exited(object sender, Node toNode)
        {
            Transit((sender as Node), toNode);
        }

        public Parameter GetParameter(string name)
        {
            return parameters.FirstOrDefault(p => p.name == name);
        }

        public void SetFloat(string name, float value)
        {
            //BoolParameter parameter = (parameters.FirstOrDefault(p => (p is BoolParameter && p.name == name)) as BoolParameter);
            //if (parameter == null)
            //{

            //}
            //else
            //{
            //    parameter.Value = value;
            //}
        }

        public void SetInt(string name, int value)
        {
            IntParameter parameter = (parameters.FirstOrDefault(p => (p is IntParameter && p.name == name)) as IntParameter);
            if (parameter == null)
            {
                // Do-nothing
            }
            else
            {
                parameter.Value = value;
            }
        }

        public void SetBool(string name, bool value)
        {
            BoolParameter parameter = (parameters.FirstOrDefault(p => (p is BoolParameter && p.name == name)) as BoolParameter);
            if (parameter == null)
            {
                // Do-nothing
            }
            else
            {
                parameter.Value = value;
            }
        }

        [ContextMenu("Debug Begin")]
        public void Debug_Begin()
        {
            Begin();

            Ended.AddListener(Debug_Ended);

            AppManager.Instance.Updated += ManualUpdate;
        }

        public void Debug_Ended(object sender)
        {
            Ended.RemoveListener(Debug_Ended);

            AppManager.Instance.Updated -= ManualUpdate;
        }
    }
}