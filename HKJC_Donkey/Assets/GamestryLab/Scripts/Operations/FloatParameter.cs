﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class FloatParameter : Parameter
    {
        [SerializeField] private float value;
        public float Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public void Add(float amount)
        {
            Value += amount;
        }

        public void Substract(float amount)
        {
            Value -= amount;
        }

        public void Multiply(float amount)
        {
            Value *= amount;
        }

        public void Divide(float amount)
        {
            Value /= amount;
        }
    }
}