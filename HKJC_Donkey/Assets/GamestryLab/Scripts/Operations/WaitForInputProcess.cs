﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GamestryLab.Operations
{
    public class WaitForInputProcess : Process
    {
        public override void Begin()
        {
            ManualUpdated += ReadUserInput;

			Debug.LogFormat("#{0} [{1}] ReadUserInput began.", Time.frameCount, GetType().FullName);

            base.Begin();
        }

        public override void End()
        {
            ManualUpdated -= ReadUserInput;

			Debug.LogFormat("#{0} [{1}] ReadUserInput ended.", Time.frameCount, GetType().FullName);

            base.End();
        }

        private void ReadUserInput()
        {
            if (Input.anyKey == true)
            {
                End();
            }
        }
    }
}