﻿using System;

namespace GamestryLab.Operations {
	public interface IProcessFlowUpdatable {
		Action FlowUpdated { get; set; }
	}
}