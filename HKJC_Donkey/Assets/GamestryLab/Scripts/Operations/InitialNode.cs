﻿using GamestryLab.Debugger;
using System;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class InitialNode : Node 
    {
        [SerializeField] private Node nextNode;

        private bool processing;

        public override bool IsProcessing
        {
            get { return processing; }
            protected set { processing = value; }
        }

        public override void Enter(Node fromNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Entering.", Time.frameCount, GetType().FullName, name);

            IsProcessing = true;

            base.Enter(fromNode);

            IsProcessing = false;

            Exit(nextNode);
        }

        public override void Exit(Node toNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Exiting to {3}.", Time.frameCount, GetType().FullName, name, (toNode == null) ? "null" : toNode.name);

            base.Exit(toNode);
        }
    }
}