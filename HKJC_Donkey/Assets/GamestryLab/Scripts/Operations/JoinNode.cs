﻿using GamestryLab.Debugger;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class JoinNode : Node
    {
        [SerializeField] private List<Node> incomingNodes;

        [SerializeField] private Node nextNode;

        private List<bool> flags;

        public override bool IsProcessing { get { return (flags.TrueForAll(f => f == false) || flags.TrueForAll(f => f == true)) == false; } }

        private void Awake()
        {
            int length = incomingNodes.Count;

            flags = new List<bool>();
            for (int i = 0; i < length; i++)
            {
                flags.Add(false);
            }
        }

        public override void Reset()
        {
            int length = incomingNodes.Count;

            for (int i = 0; i < length; i++)
            {
                flags[i] = false;
            }
        }

        public override void Enter(Node fromNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Entering from {3}.", Time.frameCount, GetType().FullName, name, (fromNode == null) ? "null" : fromNode.name);

            int index = incomingNodes.IndexOf(fromNode);
            flags[index] = true;

            base.Enter(fromNode);

            if (flags.TrueForAll(f => f == true))
            {
                Exit(nextNode);
            }
        }

        public override void Exit()
        {
            Exit(nextNode);
        }

        public override void Exit(Node toNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Exiting to {3}.", Time.frameCount, GetType().FullName, name, (toNode == null) ? "null" : toNode.name);

            base.Exit(toNode);
        }
    }
}