﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class Condition : MonoBehaviour
    {
        [SerializeField] private Expression[] expressions;

        [SerializeField] private Node nextNode;
        public Node NextNode { get { return nextNode; } }

        public bool Result { get { return Array.TrueForAll(expressions, e => e.IsFulfilled == true); } }
    }
}