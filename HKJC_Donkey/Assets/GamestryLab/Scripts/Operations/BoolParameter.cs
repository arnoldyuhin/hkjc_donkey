﻿using GamestryLab.Core;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamestryLab.Operations
{
    public class BoolParameter : Parameter
    {
        [SerializeField] private bool value;
        public bool Value
        {
            get { return value; }
            set { this.value = value; }
        }
    }
}