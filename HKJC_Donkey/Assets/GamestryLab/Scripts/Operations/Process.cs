﻿using GamestryLab.Core;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamestryLab.Operations
{
    public class Process : MonoBehaviour, IManualUpdatable
    {
        [FormerlySerializedAs("onBegin")] public UnityObjectEvent Began;
        [FormerlySerializedAs("onEnd")] public UnityObjectEvent Ended;

        [SerializeField] protected Process next;
        public virtual Process Next { get { return next; } }

        public event Action ManualUpdated;

        public virtual void Begin()
        {
			OnBegan();
        }

        protected virtual void OnBegan()
        {
            Began?.Invoke(this);
        }

        public virtual void End()
        {
			OnEnded();
        }

        protected virtual void OnEnded()
        {
            Ended?.Invoke(this);
        }

        public virtual void ManualUpdate()
        {
            OnManualUpdated();
        }

        protected virtual void OnManualUpdated()
        {
            ManualUpdated?.Invoke();
        }
    }
}