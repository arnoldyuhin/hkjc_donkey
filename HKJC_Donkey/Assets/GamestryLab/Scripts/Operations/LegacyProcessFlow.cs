﻿using GamestryLab.Core;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamestryLab.Operations {
	public class LegacyProcessFlow : MonoBehaviour, IManualUpdatable {
		[SerializeField] private Process [] processes;
		//[SerializeField] protected UnityEvent onOpen;
		//[SerializeField] protected UnityEvent onClose;

		public Process CurrentProcess {
			get { return (index >= 0 && index < processes.Length) ? processes [index] : null; }
		}

		private int index;
		public bool IsWorking { get { return (index >= 0 && index < processes.Length); } }

		public event Action ManualUpdated;

        [FormerlySerializedAs("onOpen")] public UnityObjectEvent Opened;
        [FormerlySerializedAs("onClose")] public UnityObjectEvent Closed;

		private const int Invalid = -1;

		private IProcessFlowUpdatable flowUpdater;

		private void Start ()
        {
			#region Subscribe events
			//foreach (Process process in processes) {
			//	process.Ended += Process_Ended;
			//}
            #endregion

            index = Invalid;
        }

        private void OnDestroy()
        {
            #region Unsubscribe events
            //foreach (Process process in processes) {
            //	process.Ended -= Process_Ended;
            //}
            #endregion
        }

		public void ManualUpdate () {
			OnManualUpdated ();
		}

		private void OnManualUpdated () {
			if (ManualUpdated != null) {
				ManualUpdated ();
			}
		}

        private void Process_Ended(object sender)
        {
            #region Unsubscribe events
            ManualUpdated -= (sender as Process).ManualUpdate;
            (sender as Process).Ended.RemoveListener(Process_Ended);
            #endregion

            if (index + 1 < processes.Length)
            {
                NextStep();
            }
            else
            {
                index = Invalid;
                Close();
            }
        }

        [ContextMenu("Open")]
        public void Open()
        {
            Debug.LogFormat("#{0} [{1}] {2} Open.", Time.frameCount, GetType().FullName, name);

            index = Invalid;

            //onOpen.Invoke();
            OnOpened();

            if (processes.Length > 0)
            {
                NextStep();
            }
            else
            {
                Close();
            }
        }

		public void Open (IProcessFlowUpdatable updater) {
			Open ();
			flowUpdater = updater;
			flowUpdater.FlowUpdated += ManualUpdate;
			Closed.AddListener(AutoClose);
		}

		private void AutoClose (object obj) {
			Closed.RemoveListener(AutoClose);
			if (flowUpdater != null) {
				flowUpdater.FlowUpdated -= ManualUpdate;
				flowUpdater = null;
			}
		}

        private void OnOpened()
        {
            Opened?.Invoke(this);
        }

        [ContextMenu("Close")]
        public void Close()
        {
            Debug.LogFormat("#{0} [{1}] {2} Close.", Time.frameCount, GetType().FullName, name);

            if (IsWorking == true)
            {
                #region Unsubscribe events
                ManualUpdated -= CurrentProcess.ManualUpdate;
                CurrentProcess.Ended.RemoveListener(Process_Ended);
                #endregion

                CurrentProcess.End();

                index = Invalid;
            }

            //onClose.Invoke();
            OnClosed();
        }

        private void OnClosed()
        {
            Closed?.Invoke(this);
        }

		[ContextMenu ("Force Next Step")]
		public void ForceNextStep () {
			if (IsWorking == true) {
				CurrentProcess.End ();
			}
		}

        private void NextStep()
        {
            index++;

            CurrentProcess.Begin();

            #region Subscribe events
            ManualUpdated += CurrentProcess.ManualUpdate;
            CurrentProcess.Ended.AddListener(Process_Ended);
            #endregion
        }

        [ContextMenu ("Skip All")]
		public void SkipAll () {
			while (IsWorking == true) {
				CurrentProcess.End ();
			}
		}

		[ContextMenu ("TestOpen")]
		public void TestOpen () {
			Open ();
			AppManager.Instance.Updated += ManualUpdate;
			Closed.AddListener(TestClose);
		}

		public void TestClose (object obj) {
			Closed.RemoveListener(TestClose);
			AppManager.Instance.Updated -= ManualUpdate;
		}
	}
}