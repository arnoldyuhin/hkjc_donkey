﻿using GamestryLab.Core;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamestryLab.Operations
{
    public class BoolExpression : Expression 
    {
        [SerializeField] private BoolParameter parameter;
        [SerializeField] private ComparisonOperator comparisonOperator;
        [SerializeField] private bool targetValue;

        public override bool IsFulfilled
        {
            get
            {
                switch (comparisonOperator)
                {
                    case ComparisonOperator.EqualTo:
                        return (parameter.Value == targetValue);

                    case ComparisonOperator.NotEqualTo:
                        return (parameter.Value != targetValue);
                }
                return false;
            }
        }
    }
}