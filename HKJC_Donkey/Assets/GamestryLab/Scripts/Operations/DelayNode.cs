﻿using GamestryLab.Debugger;
using GamestryLab.System;
using System;
using System.Linq;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class DelayNode : Node, IManualUpdatable
    {
        [SerializeField] private float duration;

        [SerializeField] private Node nextNode;

        private float timeElapsed;

        public Action ManualUpdated;

        public override void Enter(Node fromNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Entering.", Time.frameCount, GetType().FullName, name);

            timeElapsed = 0f;

            ManualUpdated += TimeElapse;

            base.Enter(fromNode);
		}

        public override void Exit()
        {
            Exit(nextNode);
        }

        public override void Exit(Node toNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Exiting.", Time.frameCount, GetType().FullName, name);

            ManualUpdated -= TimeElapse;

            base.Exit(toNode);
		}

        public void ManualUpdate()
        {
            OnManualUpdated();
        }

        private void OnManualUpdated()
        {
            ManualUpdated?.Invoke();
        }

        private void TimeElapse()
        {
            if (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;
            }
            else
            {
                Exit();
            }
        }
    }
}