﻿using GamestryLab.Debugger;
using System;
using System.Linq;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class DecisionNode : Node
    {
        [SerializeField] private Condition[] conditions;
        [SerializeField] private Node @else;

        private Node nextNode;

        public override void Enter(Node fromNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Entering.", Time.frameCount, GetType().FullName, name);

            Evaluate();

            base.Enter(fromNode);

            Exit(nextNode);
		}

        public override void Exit()
        {
            Exit(nextNode);
        }

        public override void Exit(Node toNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Exiting.", Time.frameCount, GetType().FullName, name);

            base.Exit(toNode);
		}

        private void Evaluate()
        {
            Condition condition = conditions.FirstOrDefault(c => c.Result == true);
            nextNode = (condition == null) ? @else : condition.NextNode;
        }
    }
}