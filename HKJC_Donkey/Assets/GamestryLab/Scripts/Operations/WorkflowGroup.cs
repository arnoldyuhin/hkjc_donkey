﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Operations {
	public class WorkflowGroup : MonoBehaviour {
		public List<Workflow> Workflows = new List<Workflow> ();
		[SerializeField] private int groupIndexChange = 0;
		public int GroupIndexChange {
			get { return groupIndexChange; }
			set { groupIndexChange = value; }
		}

		public void Begin() {
			if (Workflows.Count == 0) {
				return;
			}
			Workflows [0].Debug_Begin ();
		}

	}
}
