﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GamestryLab.Operations {
	public class WorkflowArrangement : MonoBehaviour {
		public List<WorkflowGroup> workflowGroups = new List<WorkflowGroup> ();
		private int currentGroupIndex = 0;

		public UnityEvent Ended;

		public void Begin () {
			if (workflowGroups.Count <= 0) {
				return;
			}
			currentGroupIndex = 0;
			for (int i = 0; i < workflowGroups.Count; i++) {
				workflowGroups [i].Workflows [workflowGroups [i].Workflows.Count - 1].Ended.AddListener (ProcessNext);
			}
			workflowGroups [0].Begin ();
		}

		public void ProcessNext (object obj) {
			currentGroupIndex += workflowGroups [currentGroupIndex].GroupIndexChange;
			if (currentGroupIndex >= workflowGroups.Count) {
				End ();
				return;
			}
			workflowGroups [currentGroupIndex].Begin ();
		}

		public void End() {
			for (int i = 0; i < workflowGroups.Count; i++) {
				workflowGroups [i].Workflows [workflowGroups [i].Workflows.Count - 1].Ended.RemoveListener (ProcessNext);
			}
			Ended?.Invoke ();
		}
	}
}