﻿using GamestryLab.Core;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamestryLab.Operations
{
    public abstract class Node : MonoBehaviour
    {
        public Action<object, Node> Entered;
        public Action<object, Node> Exited;

        public virtual bool IsProcessing
        {
            get { return false; }
            protected set { }
        }

        public virtual void Reset() { }

        public virtual void Enter(Node fromNode)
        {
            OnEntered(fromNode);
        }

        protected virtual void OnEntered(Node fromNode)
        {
            Entered?.Invoke(this, fromNode);
        }

        public virtual void Exit()
        {
            Exit(null);
        }

        public virtual void Exit(Node toNode)
        {
            OnExited(toNode);
        }

        protected virtual void OnExited(Node toNode)
        {
            Exited?.Invoke(this, toNode);
        }
    }
}