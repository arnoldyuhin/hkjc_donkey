﻿using GamestryLab.Debugger;
using GamestryLab.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class InputNode : Node, IManualUpdatable
    {
        //[SerializeField] private RewiredInputEventListener listener;

        private bool processing;

        public override bool IsProcessing
        {
            get { return processing; }
            protected set { processing = value; }
        }

        [SerializeField] private Node nextNode;

        public Action ManualUpdated;

        public override void Enter(Node fromNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Entering from {3}.", Time.frameCount, GetType().FullName, name, (fromNode == null) ? "null" : fromNode.name);

            IsProcessing = true;

            //ManualUpdated += listener.ManualUpdate;

            base.Enter(fromNode);
        }

        public override void Exit()
        {
            Exit(nextNode);
        }

        public override void Exit(Node toNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Exiting to {3}.", Time.frameCount, GetType().FullName, name, (toNode == null) ? "null" : toNode.name);

            IsProcessing = false;

            //ManualUpdated -= listener.ManualUpdate;

            base.Exit(toNode);
        }

        public void ManualUpdate()
        {
            OnManualUpdated();
        }

        private void OnManualUpdated()
        {
            ManualUpdated?.Invoke();
        }
    }
}