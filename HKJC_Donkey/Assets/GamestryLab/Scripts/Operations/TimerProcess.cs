﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GamestryLab.Operations
{
    public class TimerProcess : Process
    {
        [SerializeField] private float duration;

        private float time;

        public override void Begin()
        {
            time = 0f;
            ManualUpdated += Countdown;

            Debug.LogFormat("#{0} [{1}] {2} countdown began. ({3} secs)", Time.frameCount, GetType().FullName, name, duration);

            base.Begin();
		}

        public override void End()
        {
            ManualUpdated -= Countdown;

			Debug.LogFormat("#{0} [{1}] {2} countdown ended.", Time.frameCount, GetType().FullName, name);

            base.End();
		}

        private void Countdown()
        {
            if (time < duration)
            {
                time += Time.deltaTime;
            }
            else
            {
                End();
            }
        }
    }
}