﻿using System;
using UnityEngine.Events;

namespace GamestryLab.Operations
{
    [Serializable]
    public class UnityObjectNodeEvent : UnityEvent<object, Node> { }
}