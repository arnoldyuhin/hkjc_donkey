﻿using GamestryLab.Debugger;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Operations
{
    public class ActivityFinalNode : Node 
    {
        private bool processing;

        public override bool IsProcessing
        {
            get { return processing; }
            protected set { processing = value; }
        }

        public override void Enter(Node fromNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Entering from {3}.", Time.frameCount, GetType().FullName, name, (fromNode == null) ? "null" : fromNode.name);

            IsProcessing = true;

            base.Enter(fromNode);

            IsProcessing = false;

            Exit(null);
        }

        public override void Exit(Node toNode)
        {
            DebugLog.Log("#{0} [{1}] {2} Exiting.", Time.frameCount, GetType().FullName, name);

            base.Exit(toNode);
        }
    }
}