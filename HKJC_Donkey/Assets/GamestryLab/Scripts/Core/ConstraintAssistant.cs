﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

namespace GamestryLab.Core
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(IConstraint))]
    public class ConstraintAssistant : MonoBehaviour
    {
        private IConstraint constraint;
        public IConstraint Constraint
        {
            get { return constraint; }
            private set { constraint = value; }
        }

        [SerializeField] private bool[] foldouts;

        [SerializeField] private bool linkToRootOnStart;
        [SerializeField] private Transform target;
        [SerializeField] private string objectName;
        [SerializeField] private HumanBodyBones humanBodyBone;

        private void OnEnable()
        {
            Constraint = GetComponent<IConstraint>();
        }

        private void Start()
        {
            if (Application.isPlaying == true)
            {
                // Only execute in Playmode 
                if (linkToRootOnStart == true)
                {
                    Constraint.AddSource(new ConstraintSource
                    {
                        sourceTransform = transform.root,
                        weight = 1f
                    });
                }
            }
        }
    }
}