﻿using System;
using UnityEngine;

namespace GamestryLab.Core
{
	[Serializable]
	public class ShapeModule 
	{
		public ShapeType Type;
        [Range(0f, 360f)] public float Arc;
		public ShapeMultiModeValue ArcMode;
		[Range(0f, 1f)] public float ArcSpread;
		public float Angle;
		public float Radius;
        [Range(0f, 1f)] public float RadiusThickness;
        public float Length;
		public Vector3 Position;
		public Vector3 Rotation;
		public Vector3 Scale;

		public void Reset()
		{
			Type = ShapeType.Sphere;
			Arc = 360f;
			ArcMode = ShapeMultiModeValue.Random;
			ArcSpread = 0f;
			Angle = 25f;
			Radius = 1f;
            RadiusThickness = 1f;
            Length = 5f;
			Position = Vector3.zero;
			Rotation = Vector3.zero;
			Scale = Vector3.one;
		}
	}

	public enum ShapeType
	{
		Sphere,
		Hemisphere,
		Cone,		
		Box,
		Circle,
		Edge
	}

	public enum ShapeMultiModeValue
	{
		Random,
		Loop,
		PingPong,
		BurstSpread
	}
}