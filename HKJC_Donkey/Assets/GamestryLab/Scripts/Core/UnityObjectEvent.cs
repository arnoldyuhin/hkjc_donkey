﻿using System;
using UnityEngine.Events;

namespace GamestryLab.Core
{
    [Serializable]
    public class UnityObjectEvent : UnityEvent<object> { }
}