﻿using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Core
{
	public class Numeral3D : MonoBehaviour
	{
		[SerializeField] private GameObject refDigit;

		private List<Digit3D> digits;

		private int value;
		public int Value
		{
			get { return value; }
			set
			{
				this.value = value;

				int length = (value == 0) ? 1 : Mathf.FloorToInt(Mathf.Log10(value) + 1);
				while (digits.Count < length)
				{
					digits.Add(CreateDigit());
				}
				
				for (int i = length; i < digits.Count; i++)
				{
					digits[i].gameObject.SetActive(false);
				}

				float xMin = -(length - 1) * Half;

				for (int i = 0; i < length; i++)
				{
					digits[i].gameObject.SetActive(true);
					digits[i].Value = value % 10;

					Vector3 localPosition = Vector3.zero;
					localPosition.x = xMin + i;
					digits[i].transform.localPosition = localPosition;

					value /= 10;
				}
			}
		}

		private const float Half = 0.5f;

		private Material material;
		public Material Material {
			get {
				return material;
			}
			set {
				material = value;
				for (int i = 0; i < digits.Count; i++) {
					digits [i].Material = material;
				}
			}
		}

		[SerializeField]
		private List<Material> presetMaterials = new List<Material> ();

		private void Awake()
		{
			digits = new List<Digit3D>();
		}

		private Digit3D CreateDigit()
		{
			int index = digits.Count;
			GameObject go = GameObject.Instantiate(refDigit);
			go.name = string.Format("{0}[{1}]", typeof(Digit3D).FullName, index);
			go.transform.SetParent(transform, false);

			Digit3D digit = go.GetComponent<Digit3D>();
			digit.gameObject.SetActive(true);
			if (material != null)
			{
				digit.Material = material;
			}
			return digit;
		}

		public void SetTeamMaterial(byte teamId) {
			if ((int)teamId < presetMaterials.Count) {
				Material = presetMaterials [(int)(teamId+1)];
			}
		}

		[ContextMenu("Test 12340")]
		public void Test12340()
		{
			Value = 12340;
		}

		[ContextMenu("Test 0")]
		public void Test0()
		{
			Value = 0;
		}
	}
}