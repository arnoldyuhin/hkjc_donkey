﻿using UnityEngine;

namespace BalanceBreakers.Gameplay
{
    public class Vector3MonoParams : MonoBehaviour
    {
        public bool IsRelative = false;
        public Vector3 Value = Vector3.zero;
    }
}