﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Core
{
    public class AttachToHumanBone : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private HumanBodyBones bone;
        [SerializeField] private bool worldPositionStays = true;

        private void Start()
        {
            if (animator == null)
            {
                Debug.LogWarningFormat("#{0} [{1}] Animator field is not assigned.", Time.frameCount, GetType().FullName);
                return;
            }

            if (animator.isHuman == false)
            {
                Debug.LogWarningFormat("#{0} [{1}] Target is not Human.", Time.frameCount, GetType().FullName);
                return;
            }

            Transform parent = animator.GetBoneTransform(bone);
            if (parent == null)
            {
                Debug.LogWarningFormat("#{0} [{1}] HumanBodyBone ({2}) is not found.", Time.frameCount, GetType().FullName, bone);
            }
            else
            {
                transform.SetParent(parent, worldPositionStays);
            }
        }
    }
}