﻿using System;
using UnityEngine.Events;

namespace GamestryLab.Core
{
    [Serializable]
    public class UnityIntEvent : UnityEvent<int> { }
}