﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GamestryLab.Core
{
	public static class ComponentExtension 
	{
		public static bool CompareTags(this Component component, string[] tags)
		{
			return tags.Any(t => (component.CompareTag(t) == true));
		}
	}
}