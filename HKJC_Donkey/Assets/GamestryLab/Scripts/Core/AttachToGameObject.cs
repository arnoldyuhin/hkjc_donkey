﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Core
{
    public class AttachToGameObject : MonoBehaviour
    {
        [SerializeField] private GameObject target;
        [SerializeField] private bool worldPositionStays = true;

        private void Start()
        {
            if (target != null)
            {
                transform.SetParent(target.transform, worldPositionStays);

                Animator animator = target.GetComponentInParent<Animator>();
                if (animator != null)
                {
                    animator.Rebind();
                }
            }
        }
    }
}