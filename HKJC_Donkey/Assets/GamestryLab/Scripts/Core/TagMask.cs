﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class TagMask 
{
	[SerializeField] private string[] values;
	public string[] Values { get { return values; } }

	public TagMask()
	{
		values = new string[] { "Untagged" };
	}
}
