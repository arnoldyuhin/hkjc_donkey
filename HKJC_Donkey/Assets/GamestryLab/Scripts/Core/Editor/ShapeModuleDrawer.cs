﻿using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using GamestryLab.System;

namespace GamestryLab.Core
{
	[CustomPropertyDrawer(typeof(ShapeModule))]
	public class ShapeModuleDrawer : PropertyDrawer
	{
		private string[] relativePropertyPaths;
		private string[] labels;

		private bool initialized = false;
		public bool IsInitialized { get { return initialized; } }

		private float height;

		// Draw the property inside the given rect
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (IsInitialized == false)
			{
				OnShapeTypeChanged((ShapeType)property.FindPropertyRelative("Type").enumValueIndex);

				initialized = true;
			}

			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);

			Rect rect = new Rect(position.x, position.y, position.width, (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing));

			EditorGUI.BeginChangeCheck();

			SerializedProperty shapeTypeProp = property.FindPropertyRelative("Type");
			EditorGUI.PropertyField(rect, shapeTypeProp, new GUIContent("Shape"));

			if (EditorGUI.EndChangeCheck() == true)
			{
				OnShapeTypeChanged((ShapeType)shapeTypeProp.enumValueIndex);
			}

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);

			int length = relativePropertyPaths.Length;
			for (int i = 0; i < length; i++)
			{
				SerializedProperty prop = property.FindPropertyRelative(relativePropertyPaths[i]);
				EditorGUI.PropertyField(rect, prop, new GUIContent(labels[i]));

				rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			}

			EditorGUI.EndProperty();
		}

		public void OnShapeTypeChanged(ShapeType value)
		{
			switch (value)
			{
				case ShapeType.Sphere:
					relativePropertyPaths = new string[] { "Radius", "Arc", "ArcMode", "ArcSpread", "Position", "Rotation", "Scale" };
					labels = new string[] { "Radius", "Arc", "Arc/Mode", "Arc/Spread", "Position", "Rotation", "Scale" };
					break;

				case ShapeType.Hemisphere:
					relativePropertyPaths = new string[] { "Radius", "Arc", "ArcMode", "ArcSpread", "Position", "Rotation", "Scale" };
					labels = new string[] { "Radius", "Arc", "Arc/Mode", "Arc/Spread", "Position", "Rotation", "Scale" };
					break;

				case ShapeType.Cone:
					relativePropertyPaths = new string[] { "Angle", "Radius", "RadiusThickness", "Arc", "ArcMode", "ArcSpread", "Position", "Rotation", "Scale" };
					labels = new string[] { "Angle", "Radius", "Radius Thickness", "Arc", "Arc/Mode", "Arc/Spread", "Position", "Rotation", "Scale" };
					break;

				case ShapeType.Circle:
					relativePropertyPaths = new string[] { "Radius", "RadiusThickness", "Arc", "ArcMode", "ArcSpread", "Position", "Rotation", "Scale" };
					labels = new string[] { "Radius", "Radius Thickness", "Arc", "Arc/Mode", "Arc/Spread", "Position", "Rotation", "Scale" };
					break;

				default:
					relativePropertyPaths = new string[] { "Position", "Rotation", "Scale" };
					labels = new string[] { "Position", "Rotation", "Scale" };
					break;
			}

			height = (relativePropertyPaths.Length + 1) * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return height;
		}
	}
}