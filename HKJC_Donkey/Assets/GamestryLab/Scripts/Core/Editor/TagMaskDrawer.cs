﻿using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using GamestryLab.System;

namespace GamestryLab.Core
{
	[CustomPropertyDrawer(typeof(TagMask))]
	public class TagMaskDrawer : PropertyDrawer
	{
		private bool initialized = false;
		public bool IsInitialized { get { return initialized; } }

		private string[] tags;

		private bool foldout;

		private float height;

		private void Initialize()
		{
			int length = UnityEditorInternal.InternalEditorUtility.tags.Length;

			tags = new string[length];
			Array.Copy(UnityEditorInternal.InternalEditorUtility.tags, tags, length);

			foldout = false;

			height = (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);

			initialized = true;
		}

		// Draw the property inside the given rect
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (IsInitialized == false)
			{
				Initialize();
			}

			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);

			Rect foldoutRect = new Rect(position.x, position.y, position.width, (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing));

			foldout = EditorGUI.Foldout(foldoutRect, foldout, label);
			if (foldout == true)
			{
				SerializedProperty valuesProp = property.FindPropertyRelative("values");

				Rect valueRect = new Rect(foldoutRect);
				valueRect.x += EditorGUIUtility.labelWidth;
				valueRect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
				
				int length = tags.Length;
				for (int i = 0; i < length; i++)
				{
					int index = valuesProp.IndexOf(tags[i]);
					bool toggle = (index != -1);

					EditorGUI.BeginChangeCheck();
					toggle = EditorGUI.ToggleLeft(valueRect, tags[i], toggle);
					if (EditorGUI.EndChangeCheck() == true)
					{
						switch (toggle)
						{
							case true:
								valuesProp.InsertArrayElementAtIndex(valuesProp.arraySize);
								valuesProp.GetArrayElementAtIndex(valuesProp.arraySize - 1).stringValue = tags[i];
								break;

							case false:
								valuesProp.DeleteArrayElementAtIndex(index);
								break;
						}
					}

					valueRect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
				}
			}

			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			if (foldout == true)
			{
				height += tags.Length * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			}

			return height;
		}
	}
}