﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Animations;
using Object = System.Object;

namespace GamestryLab.Core
{
	[CustomEditor(typeof(ConstraintAssistant))]
	[CanEditMultipleObjects]
	public class ConstraintAssistantEditor : Editor
	{
        private SerializedProperty foldoutsProp;
        private SerializedProperty linkToRootOnStartProp;
        private SerializedProperty targetRoot;
        private SerializedProperty objectNameProp;
        private SerializedProperty humanBodyBoneProp;

        private void OnEnable()
		{
            foldoutsProp = serializedObject.FindProperty("foldouts");
            linkToRootOnStartProp = serializedObject.FindProperty("linkToRootOnStart");
            targetRoot = serializedObject.FindProperty("target");
            objectNameProp = serializedObject.FindProperty("objectName");
            humanBodyBoneProp = serializedObject.FindProperty("humanBodyBone");
        }

		public override void OnInspectorGUI()
		{
            ConstraintAssistant assistant = (target as ConstraintAssistant);

			serializedObject.Update();

            #region Linking to GameObjects
            GetFoldout(0).boolValue = EditorGUILayout.Foldout(GetFoldout(0).boolValue, new GUIContent("Linking to GameObjects"));
            if (GetFoldout(0).boolValue == true)
            {
                EditorGUI.indentLevel++;

                EditorGUILayout.PropertyField(linkToRootOnStartProp, new GUIContent("Is Link to Root on Start"));

                #region Search
                GetFoldout(1).boolValue = EditorGUILayout.Foldout(GetFoldout(1).boolValue, new GUIContent("Search"));
                if (GetFoldout(1).boolValue == true)
                {
                    EditorGUI.indentLevel++;

                    EditorGUILayout.PropertyField(targetRoot);

                    #region by Object name
                    GetFoldout(2).boolValue = EditorGUILayout.Foldout(GetFoldout(2).boolValue, new GUIContent("by Object name"));
                    if (GetFoldout(2).boolValue == true)
                    {
                        EditorGUI.indentLevel++;

                        EditorGUILayout.PropertyField(objectNameProp, new GUIContent("Value"));
                        EditorGUILayout.BeginHorizontal();
                        {
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button(new GUIContent("Search and Link to"), GUILayout.Width(160f)) == true)
                            {
                                bool valid = true;

                                if (targetRoot.objectReferenceValue == null)
                                {
                                    Debug.LogWarningFormat("#{0} [{1}] Root field cannot be null!", Time.frameCount, GetType().FullName);
                                    valid = false;
                                }

                                if (string.IsNullOrEmpty(objectNameProp.stringValue) == true)
                                {
                                    Debug.LogWarningFormat("#{0} [{1}] Value field cannot be empty!", Time.frameCount, GetType().FullName);
                                    valid = false;
                                }

                                if (valid == true)
                                {
                                    Transform target = (targetRoot.objectReferenceValue as Transform).Find(objectNameProp.stringValue, true);
                                    if (target == null)
                                    {
                                        Debug.LogWarningFormat("#{0} [{1}] Child ({2}) cannot be found!", Time.frameCount, GetType().FullName, objectNameProp.stringValue);
                                    }
                                    else
                                    {
                                        assistant.Constraint.AddSource(new ConstraintSource
                                        {
                                            sourceTransform = target,
                                            weight = 1f
                                        });
                                    }
                                }
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUI.indentLevel--;
                    }
                    #endregion

                    #region by HumanBodyBone
                    GetFoldout(3).boolValue = EditorGUILayout.Foldout(GetFoldout(3).boolValue, new GUIContent("by HumanBodyBone"));
                    if (GetFoldout(3).boolValue == true)
                    {
                        EditorGUI.indentLevel++;

                        EditorGUILayout.PropertyField(humanBodyBoneProp, new GUIContent("Value"));
                        EditorGUILayout.BeginHorizontal();
                        {
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button(new GUIContent("Search and Link to"), GUILayout.Width(160f)) == true)
                            {
                                bool valid = true;

                                if (targetRoot.objectReferenceValue == null)
                                {
                                    Debug.LogWarningFormat("#{0} [{1}] Root field cannot be null!", Time.frameCount, GetType().FullName);
                                    valid = false;
                                }

                                Animator animator = (targetRoot.objectReferenceValue as Transform).GetComponent<Animator>();
                                if (animator == null)
                                {
                                    Debug.LogWarningFormat("#{0} [{1}] Not able to find Animator Component in {2}!", Time.frameCount, GetType().FullName, targetRoot.objectReferenceValue.name);
                                    valid = false;
                                }
                                else
                                {
                                    if (animator.isHuman == false)
                                    {
                                        Debug.LogWarningFormat("#{0} [{1}] {2} is not Human!", Time.frameCount, GetType().FullName, targetRoot.objectReferenceValue.name);
                                        valid = false;
                                    }
                                }

                                if (valid == true)
                                {
                                    Transform target = animator.GetBoneTransform((HumanBodyBones)humanBodyBoneProp.enumValueIndex);
                                    if (target == null)
                                    {
                                        Debug.LogWarningFormat("#{0} [{1}] HumanBodyBone ({2}) cannot be found!", Time.frameCount, GetType().FullName, objectNameProp.stringValue);
                                    }
                                    else
                                    {
                                        assistant.Constraint.AddSource(new ConstraintSource
                                        {
                                            sourceTransform = target,
                                            weight = 1f
                                        });
                                    }
                                }
                            }
                        }
                        EditorGUILayout.EndHorizontal();

                        EditorGUI.indentLevel--;
                    }
                    #endregion

                    EditorGUI.indentLevel--;
                }
                #endregion

                EditorGUI.indentLevel--;
            }
            #endregion

            //EditorGUILayout.PropertyField(hitboxesProp, true);
            //if (GUILayout.Button(new GUIContent("Find all Hitboxes in Children")) == true)
            //{
            //	assistant.GetComponentsInChildren<Hitbox>().CopyTo(hitboxesProp);
            //}

            //EditorGUILayout.PropertyField(hurtboxesProp, true);
            //if (GUILayout.Button(new GUIContent("Find all Hurtboxes in Children")) == true)
            //{
            //	assistant.GetComponentsInChildren<Hurtbox>().CopyTo(hurtboxesProp);
            //}

            //EditorGUILayout.PropertyField(sensorsProp, true);
            //if (GUILayout.Button(new GUIContent("Find all Sensors in Children")) == true)
            //{
            //	assistant.GetComponentsInChildren<Sensor>().CopyTo(sensorsProp);
            //}

            //EditorGUILayout.PropertyField(trackersProp, true);
            //if (GUILayout.Button(new GUIContent("Find all Trackers in Children")) == true)
            //{
            //	assistant.GetComponentsInChildren<Tracker>().CopyTo(trackersProp);
            //}

            //EditorGUILayout.PropertyField(lootRangeProp);
            //EditorGUILayout.PropertyField(activeSkillsProp, true);
            //EditorGUILayout.PropertyField(basicYawRateProp);
            //EditorGUILayout.PropertyField(basicMoveSpeedProp);
            //EditorGUILayout.PropertyField(statusEffectParticleSystemsProp);
            //EditorGUILayout.PropertyField(indicatorProp);
            //EditorGUILayout.PropertyField(databoardProp);
            //EditorGUILayout.PropertyField(traitsProp, true);
            //EditorGUILayout.PropertyField(professionsProp, true);
            //EditorGUILayout.PropertyField(brainProp);

            serializedObject.ApplyModifiedProperties();
		}

        private SerializedProperty GetFoldout(int index)
        {
            int length = index + 1;
            for (int i = foldoutsProp.arraySize; i < length; i++)
            {
                foldoutsProp.InsertArrayElementAtIndex(i);
                foldoutsProp.GetArrayElementAtIndex(i).boolValue = false;
            }
            return foldoutsProp.GetArrayElementAtIndex(index);
        }
	}
}