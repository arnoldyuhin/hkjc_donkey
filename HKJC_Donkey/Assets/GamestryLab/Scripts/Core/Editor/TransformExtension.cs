﻿using UnityEngine;

namespace GamestryLab.Core
{
	public static class TransformExtension
	{
		public static Transform Find(this Transform transform, string n, bool recursive)
		{
			if (recursive == false)
			{
				return transform.Find(n);
			}
			else
			{
				foreach (Transform child in transform)
				{
					if (child.name == n)
					{
						return child;
					}

					Transform target = child.Find(n, true);
					if (target != null)
					{
						return target;
					}
				}
			}
			return null;
		}
	}
}