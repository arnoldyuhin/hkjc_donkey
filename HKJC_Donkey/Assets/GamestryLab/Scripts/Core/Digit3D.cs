﻿using UnityEngine;

namespace GamestryLab.Core
{
	public class Digit3D : MonoBehaviour
	{
		[SerializeField] private MeshFilter meshFilter;
		[SerializeField] private MeshRenderer meshRenderer;
		[SerializeField] private Mesh[] refMeshes;

		private int value;
		public int Value
		{
			get { return value; }
			set
			{
				value = Mathf.Clamp(value, 0, 10);

				this.value = value;
				meshFilter.mesh = refMeshes[value];
			}
		}

		public Material Material {
			set
			{
				if (meshRenderer != null)
				{
					meshRenderer.material = value;
				}
			}
		}
	}
}