﻿using System;
using System.Collections;
using System.Collections.Generic;
using GamestryLab.System;
using UnityEngine;

public class TaskManager {

	public TaskManager () { }

	public TaskManager (float _timeInterval) {
		isUsingTickInterval = true;
		updateDeltaTime = _timeInterval;
	}

	private bool isUsingTickInterval = false;
	public float updateDeltaTime = 0f;

	public List<Task> tasks = new List<Task> ();

    public event Action<object> Emptied;

	public bool isShowingTaskManagerLog = false;

	public Task GetTask (string name) {
		for (int i = 0; i < tasks.Count; i++) {
			if (tasks [i].name == name) {
				return tasks [i];
			}
		}
		return null;
	}

	public List<Task> GetTasksWithAttribute (string _attribute) {
		List<Task> selectedTasks = new List<Task> ();
		for (int i = 0; i < tasks.Count; i++) {
			if (tasks [i].attributes.ContainsKey (_attribute)) {
				selectedTasks.Add (tasks [i]);
			}
		}
		return selectedTasks;
	}

    public void ManualUpdate()
    {
        if (isShowingTaskManagerLog)
        {
            Debug.Log(this.GetType() + ": ManualUpdate");
        }
        if (!isUsingTickInterval)
        {
            updateDeltaTime = AppManager.Instance.deltaTime;
        }
        if (tasks.Count > 0)
        {
            for (int i = 0; i < tasks.Count; i++)
            {
                if (tasks[i].currentTaskPhase == Task.Phase.End)
                {
                    KillTask(tasks[i]);
                }
            }
            for (int i = 0; i < tasks.Count; i++)
            {
                switch (tasks[i].currentTaskPhase)
                {
                    case Task.Phase.Initialize:
                        tasks[i].ManualInitialize();
                        break;
                    case Task.Phase.Update:
                        tasks[i].ManualUpdate(updateDeltaTime);
                        break;
                    case Task.Phase.Terminate:
                        tasks[i].ManualTerminate();
                        break;
                }

                if (tasks[i].blockType != Task.BlockType.BlockAfter && tasks[i].blockType != Task.BlockType.BlockAll)
                {
                    if (i + 1 < tasks.Count)
                    {
                        if (tasks[i + 1].blockType != Task.BlockType.BlockBefore && tasks[i + 1].blockType != Task.BlockType.BlockAll)
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }

    public void Close () {
		if (isShowingTaskManagerLog) {
			Debug.Log (this.GetType () + ": OnTerminate");
		}
		KillAllTasks ();
	}

	public void AddTask (Task task) {
		if (task == null) {
			return;
		}
		AddTask (task, tasks.Count);
	}

	public void AddTaskAtFront (Task task) {
		if (task == null) {
			return;
		}
		AddTask (task, 0);
	}

	public void AddTask (Task task, bool withPriority) {
		if (withPriority) {
			int index = tasks.Count;
			for (int i = tasks.Count - 1; i >= 0; i--) {
				if (task.priority <= tasks [i].priority) {
					index = i;
					break;
				}
			}
			AddTask (task, index);
		}
	}

	public void AddTaskAtFront (Task task, bool withPriority) {
		if (withPriority) {
			int index = 0;
			for (int i = tasks.Count - 1; i >= 0; i--) {
				if (task.priority < tasks [i].priority) {
					index = i;
					break;
				}
			}
			AddTask (task, index);
		}
	}

	private void AddTask (Task task, int index) {
		task.Killed += KillTask;
		tasks.Insert (index, task);
	}

	public void KillTask (Task task) {
		task.Killed -= KillTask;
		if (tasks.Contains (task)) {
			tasks.Remove (task);
		}

        if (tasks.Count == 0)
        {
            OnEmptied();
        }
	}

    public void OnEmptied()
    {
        if (Emptied != null)
        {
            Emptied(this);
        }
    }

	public void KillAllTasks () {
		for (int i = tasks.Count - 1; i >= 0; i--) {
			KillTask (tasks [i]);
		}
		tasks.Clear ();
	}

	public void EndTask (Task _task) {
		_task.End ();
	}

	public void EndAllTasks () {
		for (int i = 0; i < tasks.Count; i++) {
			tasks [i].End ();
		}
	}
}