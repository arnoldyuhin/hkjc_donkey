﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task {

	public enum Phase { Initialize, Update, Terminate, End }
	public enum Priority { Immediate, Land, Attack, Default, TurnBase }
	public enum BlockType { Free, BlockBefore, BlockAfter, BlockAll }

	public string name;

	public Phase currentTaskPhase;
	public Priority priority = Priority.Default;
	public BlockType blockType = BlockType.Free;

	public Dictionary<string, string> attributes = new Dictionary<string, string> ();

	public float taskTime = 0f;
	public float currentTime = 0f;
	public float tickTime = 0f;
	private float nextTickTime = 0f;

	public Action<Task> Initialized;
	public Action<Task> Updated;
	public Action<Task> Ticked;
	public Action<Task> Terminated;
	public Action<Task> Completed;
	public Action<Task> Killed;

	public Task () {

	}

	public Task (string taskName) {
		name = taskName;
	}

	public Task (float duration) {
		taskTime = duration;
		currentTime = 0f;
	}

	public Task SetTaskTime (float duration) {
		taskTime = duration;
		currentTime = 0f;
		return this;
	}

	public Task SetBlockType (BlockType type) {
		blockType = type;
		return this;
	}

	public Task SetTickTime (float duration) {
		tickTime = duration;
		nextTickTime += tickTime;
		return this;
	}

	public void SetNextTickTime (float nextTick) {
		nextTickTime = nextTick;
	}

	public Task OnInitialize (Action<Task> callback) {
		Initialized += callback;
		return this;
	}

	public Task OnUpdate (Action<Task> callback) {
		Updated += callback;
		return this;
	}

	public Task OnTerminate (Action<Task> callback) {
		Terminated += callback;
		return this;
	}

	public Task OnComplete (Action<Task> callback) {
		Completed += callback;
		return this;
	}

	public Task OnTick (Action<Task> callback) {
		Ticked += callback;
		return this;
	}

	public Task OnKill (Action<Task> callback) {
		Killed += callback;
		return this;
	}

	public void ManualInitialize () {
		if (Initialized != null) {
			Initialized (this);
		}
		CompletePhase ();
	}

	public void ManualUpdate (float deltaTime) {
		currentTime += deltaTime;

		if (Updated != null) {
			Updated (this);
		}

		if (tickTime > 0f) {
			if (currentTime >= nextTickTime) {
				nextTickTime += tickTime;
				Tick ();
			}
		}

		if (taskTime > 0f) {
			if (currentTime >= taskTime) {
				Complete ();
			}
		}
	}

	public void ManualTerminate () {
		if (Terminated != null) {
			Terminated (this);
		}
		CompletePhase ();
	}

	private void Tick () {
		if (Ticked != null) {
			Ticked (this);
		}
	}

	private void Complete () {
		if (Completed != null) {
			Completed (this);
		}
		End ();
	}

	private void CompletePhase () {
		currentTaskPhase = (Phase)(((int)currentTaskPhase + 1) % System.Enum.GetValues (typeof (Phase)).Length);
	}

	public void End () {
		currentTaskPhase = Phase.Terminate;
	}

	public void Kill () {
		if (Killed != null) {
			Killed (this);
		}
	}

}