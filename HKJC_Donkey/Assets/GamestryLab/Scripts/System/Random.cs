﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.System {
	public class Random {
		public static T RandomFromList<T> (List<T> objectList) {
			if (objectList.Count == 0) return default (T);
			int randomIndex = UnityEngine.Random.Range (0, objectList.Count);
			return objectList [randomIndex];
		}

		public static List<T> RandomAmountFromList<T> (List<T> objectList, int amount) {
			if (objectList.Count == 0) return null;
			List<T> remainList = new List<T> (objectList);
			List<T> resultList = new List<T> ();
			for (int i = 0; i < amount; i++) {
				int randomIndex = UnityEngine.Random.Range (0, remainList.Count);
				resultList.Add (remainList [randomIndex]);
				remainList.RemoveAt (randomIndex);
			}
			return resultList;
		}

		public static List<T> RandomRearrange<T> (List<T> objectList) {
			if (objectList.Count == 0) return null;
			List<T> remainList = new List<T> (objectList);
			List<T> resultList = new List<T> ();
			int length = remainList.Count;
			for (int i = 0; i < length; i++) {
				int randomIndex = UnityEngine.Random.Range (0, remainList.Count);
				resultList.Add (remainList [randomIndex]);
				remainList.RemoveAt (randomIndex);
			}
			return resultList;
		}
	}
}