﻿using GamestryLab.Transition;
using GamestryLab.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GamestryLab.System
{
    public class AppManager : MonoSingleton<AppManager>
    {
		public event Action Updated;

        public float deltaTime;

        protected override void Awake()
        {
            base.Awake();

            DontDestroyOnLoad();
        }

		private void Start () {
			TransitionEnded?.Invoke ();
		}

		private void Update()
        {
            deltaTime = Time.deltaTime;
            OnUpdated();
        }

        private void OnUpdated()
        {
            Updated?.Invoke();
        }

		#region Transition

		private string nextSceneName;
		private TransitionScreen transition;
		public Action TransitionEnded;
		public AsyncOperation AsyncLoad;

		public void ChangeScene(string sceneName, TransitionScreen transitionScreen)
		{
			transition = transitionScreen;
			nextSceneName = sceneName;
			transition.ShowEnded += TransitionScreen_OpenEnded;
			transition.Show ();

			DontDestroyOnLoad (transitionScreen);
		}

		private void TransitionScreen_OpenEnded()
		{
			transition.ShowEnded -= TransitionScreen_OpenEnded;
			AsyncLoad = SceneManager.LoadSceneAsync (nextSceneName);
			AsyncLoad.completed += LoadSceneCompleted;
		}

		private void LoadSceneCompleted(AsyncOperation asyncOperation)
		{
            asyncOperation.completed -= LoadSceneCompleted;
            transition.Hide ();
			transition.HideEnded += TransitionScreen_CloseEnded;
		}

		private void TransitionScreen_CloseEnded()
		{
			transition.HideEnded -= TransitionScreen_CloseEnded;
			TransitionEnded?.Invoke ();
		}
		#endregion

	}
}