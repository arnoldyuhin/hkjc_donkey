﻿namespace GamestryLab.System
{
    public interface IManualUpdatable
    {
        void ManualUpdate();
    }
}