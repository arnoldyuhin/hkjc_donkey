﻿using System.Collections;
using System.Collections.Generic;
using GamestryLab.System;
using UnityEngine;
using UnityEngine.Events;

namespace BalanceBreakers.Common {
	public class SceneStarter : MonoBehaviour {
		public UnityEvent Started;
		public UnityEvent Awaked;

		private void Awake () {
			Awaked?.Invoke ();
		}

		private void Start () {
			Started?.Invoke ();
		}
	}
}