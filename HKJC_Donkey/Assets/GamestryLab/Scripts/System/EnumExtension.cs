﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.System
{
	public static class EnumExtension 
	{
		public static IEnumerable<Enum> GetFlags(this Enum @enum)
        {
            foreach (Enum value in Enum.GetValues(@enum.GetType()))
            {
                ulong bits = Convert.ToUInt64(value);
                if ((bits > 0) && @enum.HasFlag(value))
                    yield return value;
            }
        }
    }
}