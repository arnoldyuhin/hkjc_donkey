﻿using System.Collections;
using System.Collections.Generic;
//using Rewired;
using UnityEngine;

namespace GamestryLab.System {
	public class CursorConfiguration : MonoBehaviour {
		public enum CursorState { Visible, Invisible, AutoHide }
		[SerializeField] private CursorState state = CursorState.Visible;
		public CursorState State {
			get { return state; }
			set {
				state = value;
				switch (state) {
				case CursorState.Visible:
					Cursor.visible = true;
					break;
				case CursorState.Invisible:
					Cursor.visible = false;
					break;
				case CursorState.AutoHide:
					Cursor.visible = false;
					break;
				}
			}
		}
		[SerializeField] private float autoHideTime = 3f;

		private void Start () {
			switch (State) {
			case CursorState.Visible:
				Cursor.visible = true;
				break;
			case CursorState.Invisible:
				Cursor.visible = false;
				break;
			case CursorState.AutoHide:
				Cursor.visible = false;
				break;
			}
		}

		private void Update () {
			AutoHideCursor ();
		}

		private float lastMovementTime;
		private Vector3 lastMousePos;

		private void AutoHideCursor () {
			if (State != CursorState.AutoHide) {
				return;
			}
			if (Input.anyKey || Vector3.SqrMagnitude (lastMousePos - Input.mousePosition) > 0) {
				lastMovementTime = Time.time;
				lastMousePos = Input.mousePosition;
			}

			if (Time.time - lastMovementTime > autoHideTime) {
				if (Cursor.visible) {
					Cursor.visible = false;
				}
			} else {
				if (!Cursor.visible) {
					Cursor.visible = true;
				}

			}
			//if (ReInput.time.unscaledTime - ReInput.controllers.Mouse.GetLastTimeActive () > autoHideTime) {
			//	if (Cursor.visible) {
			//		Cursor.visible = false;
			//	}
			//} else {
			//	if (!Cursor.visible) {
			//		Cursor.visible = true;
			//	}

			//}

		}
	}
}