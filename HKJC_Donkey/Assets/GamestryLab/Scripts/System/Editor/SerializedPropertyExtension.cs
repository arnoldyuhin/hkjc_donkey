﻿using System.Collections;
using UnityEditor;

namespace GamestryLab.System
{
	public static class SerializedPropertyExtension
	{
		public static int IndexOf(this SerializedProperty property, string value)
		{
			if (property.isArray == false) { return -1; }

			int index = 0;

			IEnumerator enumerator = property.GetEnumerator();
			while (enumerator.MoveNext() == true)
			{
				SerializedProperty childProp = (enumerator.Current as SerializedProperty);
				if (childProp.stringValue == value)
				{
					return index;
				}
				index++;
			}

			return -1;
		}
	}
}