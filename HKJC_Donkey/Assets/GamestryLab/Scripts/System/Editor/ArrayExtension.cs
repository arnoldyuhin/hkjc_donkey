﻿using System;
using System.Collections;
using UnityEngine;
using UnityEditor;

namespace GamestryLab.System
{
	public static class ArrayExtension
	{
		public static void CopyTo(this Array array, SerializedProperty property)
		{
			if (property.isArray == false)
			{
				// Warning: SerializedProperty is not array!
				return;
			}

			property.ClearArray();

			int length = array.Length;
			for (int i = 0; i < length; i++)
			{
				property.InsertArrayElementAtIndex(i);
				property.GetArrayElementAtIndex(i).objectReferenceValue = (array.GetValue(i) as UnityEngine.Object);
			}
		}
	}
}