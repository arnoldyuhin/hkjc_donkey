﻿namespace GamestryLab.Net
{
    public enum PacketType
    {
		GameState,
		Transform,
		UserCommand,
        JoystickVibration
    }
}