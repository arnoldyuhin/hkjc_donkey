﻿using GamestryLab.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GamestryLab.Net
{
    public class PacketReceiver : MonoBehaviour, IService
    {
		private Dictionary<PacketType, IPacket> packets;
        private Dictionary<PacketType, PacketEvent> evts;

		private void Awake()
		{
			packets = new Dictionary<PacketType, IPacket>();
			evts = new Dictionary<PacketType, PacketEvent>();
		}

		public IPacket GetPacket(PacketType type)
		{
			if (packets.TryGetValue(type, out IPacket packet) == true)
			{
				return packet;
			}
			return null;
		}

		public void Receive(IPacket packet)
        {
			if (packets.ContainsKey(packet.Type) == false)
			{
				packets.Add(packet.Type, null);
			}
			packets[packet.Type] = packet;

            if (evts.TryGetValue(packet.Type, out PacketEvent evt) == true)
            {
                evt.Invoke(packet);
            }
        }

        public void StartListening(PacketType type, UnityAction<IPacket> listener)
        {
            PacketEvent evt = null;
            if (evts.TryGetValue(type, out evt))
            {
                evt.AddListener(listener);
            }
            else
            {
                evt = new PacketEvent();
                evt.AddListener(listener);
                evts.Add(type, evt);
            }
        }

        public void StopListening(PacketType type, UnityAction<IPacket> listener)
        {
            PacketEvent evt = null;
            if (evts.TryGetValue(type, out evt))
            {
                evt.RemoveListener(listener);
            }
        }
    }
}