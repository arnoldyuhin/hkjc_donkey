﻿using GamestryLab.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Net
{
    public class PacketSender : MonoBehaviour, IService
    {
		public void Send(IPacket packet)
        {
			Localhost.Broadcast(packet);
        }
    }
}