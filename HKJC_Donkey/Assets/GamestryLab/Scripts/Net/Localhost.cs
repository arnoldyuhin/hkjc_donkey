﻿using GamestryLab.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Net
{
    public class Localhost : MonoSingleton<Localhost>
	{
		public static void Broadcast(IPacket packet)
		{
			ServiceLocator.GetService<PacketReceiver>().Receive(packet);
		}
	}
}