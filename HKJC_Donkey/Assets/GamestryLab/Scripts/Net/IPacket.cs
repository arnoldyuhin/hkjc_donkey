﻿namespace GamestryLab.Net
{
    public interface IPacket
    {
        PacketType Type { get; }
    }
}