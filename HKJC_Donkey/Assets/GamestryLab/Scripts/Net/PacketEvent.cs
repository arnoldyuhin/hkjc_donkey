﻿using UnityEngine.Events;

namespace GamestryLab.Net
{
    public class PacketEvent : UnityEvent<IPacket> {}
}