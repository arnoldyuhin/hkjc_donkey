﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GamestryLab {
	public static class CodeAnimator {

		public static void BounceTextChange (Text _textObj, string _newContent, float _originalScale, float _changeScale, float _animationTime) {
			_textObj.transform.DOKill ();
			_textObj.transform.DOScale (_originalScale + _changeScale, _animationTime / 2f).SetEase (Ease.OutBack)
			    .OnComplete (() => {
				    _textObj.text = _newContent;
			    })
			    .OnKill (() => {
				    _textObj.text = _newContent;
			    });
			_textObj.transform.DOScale (_originalScale, _animationTime / 2f).SetEase (Ease.InBack).SetDelay (_animationTime / 2f);
		}

		//public static void BounceTextChange (OnScreenTextObject _textObj, string _newContent, float _originalScale, float _changeScale, float _animationTime) {
		//    _textObj.transform.DOKill ();
		//    _textObj.transform.DOScale (_originalScale + _changeScale, _animationTime / 2f).SetEase (Ease.OutBack)
		//        .OnComplete (() => {
		//            _textObj.text.text = _newContent;
		//        })
		//        .OnKill (() => {
		//            _textObj.text.text = _newContent;
		//        });
		//    _textObj.transform.DOScale (_originalScale, _animationTime / 2f).SetEase (Ease.InBack).SetDelay (_animationTime / 2f);
		//}

		//public static void BounceTextChange (OnScreenTMProUGUI _textObj, string _newContent, float _originalScale, float _changeScale, float _animationTime) {
		//    _textObj.transform.DOKill ();
		//    _textObj.transform.DOScale (_originalScale + _changeScale, _animationTime / 2f).SetEase (Ease.OutBack)
		//        .OnComplete (() => {
		//            _textObj.TMProText.text = _newContent;
		//        })
		//        .OnKill (() => {
		//            _textObj.TMProText.text = _newContent;
		//        });
		//    _textObj.transform.DOScale (_originalScale, _animationTime / 2f).SetEase (Ease.InBack).SetDelay (_animationTime / 2f);
		//}
	}
}