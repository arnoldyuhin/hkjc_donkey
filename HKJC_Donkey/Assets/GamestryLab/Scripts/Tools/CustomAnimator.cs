﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Tools
{
    [RequireComponent(typeof(Animator))]
    public class CustomAnimator : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        private void Awake()
        {
			if (animator == null)
			{
				animator = GetComponent<Animator>();
			}
        }

        public void PlayButStartOnFirstFrame(string stateName)
        {
            animator.Play(stateName, -1, 0f);
        }
    }
}