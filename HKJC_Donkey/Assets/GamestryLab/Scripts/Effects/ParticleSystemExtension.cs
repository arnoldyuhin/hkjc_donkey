﻿using UnityEngine;

namespace GamestryLab.Effects
{
	public static class ParticleSystemExtension 
	{
		public static void Emit(this ParticleSystem system, ParticlePlayground.SerializableEmitParams emitParams, int count)
		{
			ParticleSystem.EmitParams legacyEmitParams = new ParticleSystem.EmitParams
			{
				position = (emitParams.Parent == null) ? Vector3.zero : emitParams.Parent.position,
				rotation3D = (emitParams.Parent == null) ? Vector3.zero : emitParams.Parent.eulerAngles
			};

			if (emitParams.UseAngularVelocity == true) { legacyEmitParams.angularVelocity = emitParams.AngularVelocity; }
			if (emitParams.UseAngularVelocity3D == true) { legacyEmitParams.angularVelocity3D = emitParams.AngularVelocity3D; }
			if (emitParams.UseApplyShapeToPosition == true) { legacyEmitParams.applyShapeToPosition = emitParams.ApplyShapeToPosition; }
			if (emitParams.UsePosition == true) { legacyEmitParams.position += (emitParams.Parent == null) ? emitParams.Position : Quaternion.LookRotation(emitParams.Parent.forward) * emitParams.Position; }
			if (emitParams.UseRotation == true) { legacyEmitParams.rotation = emitParams.Rotation; }
			if (emitParams.UseRotation3D == true) { legacyEmitParams.rotation3D += (emitParams.Parent == null) ? emitParams.Rotation3D : Quaternion.LookRotation(emitParams.Parent.forward) * emitParams.Rotation3D; }
			if (emitParams.UseStartColor == true) { legacyEmitParams.startColor = emitParams.StartColor; }
			if (emitParams.UseStartLifetime == true) { legacyEmitParams.startLifetime = emitParams.StartLifetime; }
			if (emitParams.UseStartSize == true) { legacyEmitParams.startSize = emitParams.StartSize; }
			if (emitParams.UseStartSize3D == true) { legacyEmitParams.startSize3D = emitParams.StartSize3D; }
			if (emitParams.UseVelocity == true) { legacyEmitParams.velocity = emitParams.Velocity; }

			system.Emit(legacyEmitParams, count);
		}
	}
}