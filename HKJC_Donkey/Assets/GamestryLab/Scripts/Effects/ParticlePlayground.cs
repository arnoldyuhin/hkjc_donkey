﻿using GamestryLab.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamestryLab.Effects 
{
	public class ParticlePlayground : MonoBehaviour, IService
    {
		private Dictionary<int, ParticleSystem> particleSystems; 

		private void Awake()
		{
			particleSystems = new Dictionary<int, ParticleSystem>(); 
		} 

		public void Emit(ParticleSystem refSystem, ParticleSystem.EmitParams emitParams, int count)
		{
			if (refSystem == null)
			{
				// Reference Particle System not must be null! 
				return; 
			}

			int instanceID = refSystem.GetInstanceID(); 

			ParticleSystem system; 
			if (particleSystems.TryGetValue(instanceID, out system) == false)
			{
                GameObject clone = Instantiate(refSystem.gameObject) as GameObject;
                clone.transform.SetParent(transform, false);
                system = clone.GetComponent<ParticleSystem>();

                particleSystems.Add(instanceID, system);
			}

			system.Emit(emitParams, count); 
		}

        public void Emit(ParticleEmissionMonoParams @params)
        {
			if (@params.RefSystem == null)
			{
				// Reference Particle System not must be null! 
				return;
			}

			int instanceID = @params.RefSystem.GetInstanceID();

			ParticleSystem system;
			if (particleSystems.TryGetValue(instanceID, out system) == false)
			{
				GameObject clone = Instantiate(@params.RefSystem.gameObject) as GameObject;
				clone.transform.SetParent(transform, false);
				system = clone.GetComponent<ParticleSystem>();

				particleSystems.Add(instanceID, system);
			}

			system.Emit(@params.EmitParams, @params.Count);
		}

        [Serializable]
        public class SerializableEmitParams
        {
			[SerializeField] private Transform parent;
			public Transform Parent { get { return parent; } }

            [SerializeField] private bool useAngularVelocity;
			public bool UseAngularVelocity { get { return useAngularVelocity; } }
            [SerializeField] private float angularVelocity;
            public float AngularVelocity
            {
                get { return angularVelocity; }
                set
                {
                    useAngularVelocity = true;
                    angularVelocity = value;
                }
            }

            [SerializeField] private bool useAngularVelocity3D;
			public bool UseAngularVelocity3D { get { return useAngularVelocity3D; } }
			[SerializeField] private Vector3 angularVelocity3D;
            public Vector3 AngularVelocity3D
            {
                get { return angularVelocity3D; }
                set
                {
                    useAngularVelocity3D = true;
                    angularVelocity3D = value;
                }
            }

            [SerializeField] private bool useApplyShapeToPosition;
			public bool UseApplyShapeToPosition { get { return useApplyShapeToPosition; } }
			[SerializeField] private bool applyShapeToPosition;
            public bool ApplyShapeToPosition
            {
                get { return applyShapeToPosition; }
                set
                {
                    useApplyShapeToPosition = true;
                    applyShapeToPosition = value;
                }
            }

            [SerializeField] private bool usePosition;
			public bool UsePosition { get { return usePosition; } }
			[SerializeField] private Vector3 position;
            public Vector3 Position
            {
                get { return position; }
                set
                {
                    usePosition = true;
                    position = value;
                }
            }

            [SerializeField] private bool useRotation;
			public bool UseRotation { get { return useRotation; } }
			[SerializeField] private float rotation;
            public float Rotation
            {
                get { return rotation; }
                set
                {
                    useRotation = true;
                    rotation = value;
                }
            }

            [SerializeField] private bool useRotation3D;
			public bool UseRotation3D { get { return useRotation3D; } }
			[SerializeField] private Vector3 rotation3D;
            public Vector3 Rotation3D
            {
                get { return rotation3D; }
                set
                {
                    useRotation3D = true;
                    rotation3D = value;
                }
            }

            [SerializeField] private bool useStartColor;
			public bool UseStartColor { get { return useStartColor; } }
			[SerializeField] private Color startColor;
            public Color StartColor
            {
                get { return startColor; }
                set
                {
                    useStartColor = true;
                    startColor = value;
                }
            }

            [SerializeField] private bool useStartLifetime;
			public bool UseStartLifetime { get { return useStartLifetime; } }
			[SerializeField] private float startLifetime;
            public float StartLifetime
            {
                get { return startLifetime; }
                set
                {
                    useStartLifetime = true;
                    startLifetime = value;
                }
            }

            [SerializeField] private bool useStartSize;
			public bool UseStartSize { get { return useStartSize; } }
			[SerializeField] private float startSize;
            public float StartSize
            {
                get { return startSize; }
                set
                {
                    useStartSize = true;
                    startSize = value;
                }
            }

            [SerializeField] private bool useStartSize3D;
			public bool UseStartSize3D { get { return useStartSize3D; } }
			[SerializeField] private Vector3 startSize3D;
            public Vector3 StartSize3D
            {
                get { return startSize3D; }
                set
                {
                    useStartSize3D = true;
                    startSize3D = value;
                }
            }

            [SerializeField] private bool useVelocity;
			public bool UseVelocity { get { return useVelocity; } }
			[SerializeField] private Vector3 velocity;
            public Vector3 Velocity
            {
                get { return velocity; }
                set
                {
                    useVelocity = true;
                    velocity = value;
                }
            }

            public SerializableEmitParams()
            {
                useAngularVelocity = false;
                angularVelocity = 0f;
                useAngularVelocity3D = false;
                angularVelocity3D = Vector3.zero;
                useApplyShapeToPosition = false;
                applyShapeToPosition = false;
                usePosition = false;
                position = Vector3.zero;
                useRotation = false;
                rotation = 0f;
                useRotation3D = false;
                rotation3D = Vector3.zero;
                useStartColor = false;
                startColor = Color.white;
                useStartLifetime = false;
                startLifetime = 0f;
                useStartSize = false;
                startSize = 0f;
                useVelocity = false;
                velocity = Vector3.zero;
            }
        }
    }
}