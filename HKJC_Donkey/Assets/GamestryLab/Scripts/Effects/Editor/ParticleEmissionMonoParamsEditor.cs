﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GamestryLab.Effects
{
	[CustomEditor(typeof(ParticleEmissionMonoParams))]
	[CanEditMultipleObjects]
	public class ParticleEmissionMonoParamsEditor : Editor
	{
        private SerializedProperty foldoutsProp;
        private SerializedProperty refSystemProp;
        private SerializedProperty emitParamsProp;
		private SerializedProperty countProp;

		private void OnEnable()
		{
            foldoutsProp = serializedObject.FindProperty("foldouts");
            refSystemProp = serializedObject.FindProperty("refSystem");
            emitParamsProp = serializedObject.FindProperty("emitParams");
			countProp = serializedObject.FindProperty("count");
		}

		public override void OnInspectorGUI()
		{
            ParticleEmissionMonoParams monoParams = (target as ParticleEmissionMonoParams);

			serializedObject.Update();

            EditorGUILayout.PropertyField(refSystemProp, new GUIContent("Ref Particle System"));

            GetFoldout(0).boolValue = EditorGUILayout.Foldout(GetFoldout(0).boolValue, new GUIContent("Emit Params"));

            if (GetFoldout(0).boolValue == true)
            {
                EditorGUI.indentLevel++;
                {
                    EditorGUILayout.PropertyField(emitParamsProp);
                }
                EditorGUI.indentLevel--;
            }

			EditorGUILayout.PropertyField(countProp, new GUIContent("Count"));

			serializedObject.ApplyModifiedProperties();
		}

        private SerializedProperty GetFoldout(int index)
        {
            int length = index + 1;
            for (int i = foldoutsProp.arraySize; i < length; i++)
            {
                foldoutsProp.InsertArrayElementAtIndex(i);
                foldoutsProp.GetArrayElementAtIndex(i).boolValue = false;
            }
            return foldoutsProp.GetArrayElementAtIndex(index);
        }
	}
}