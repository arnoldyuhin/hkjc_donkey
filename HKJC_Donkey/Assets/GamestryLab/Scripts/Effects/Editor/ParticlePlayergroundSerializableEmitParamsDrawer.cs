﻿using System;
using UnityEngine;
using UnityEditor;

namespace GamestryLab.Effects
{
	[CustomPropertyDrawer(typeof(ParticlePlayground.SerializableEmitParams))]
	public class ParticlePlayergroundSerializableEmitParamsDrawer : PropertyDrawer
	{
		private bool initialized = false;
		public bool IsInitialized { get { return initialized; } }

		private float height;

		private void Initialize()
		{
			initialized = true;
		}

		// Draw the property inside the given rect
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (IsInitialized == false)
			{
				Initialize();
			}

			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);

			Rect rect;

			rect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			SerializedProperty parentProp = property.FindPropertyRelative("parent");
			parentProp.objectReferenceValue = (Transform)EditorGUI.ObjectField(rect, new GUIContent("Parent"), parentProp.objectReferenceValue, typeof(Transform), true);

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<float>(rect, property.FindPropertyRelative("useAngularVelocity"), property.FindPropertyRelative("angularVelocity"), new GUIContent("Angular Velocity"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<Vector3>(rect, property.FindPropertyRelative("useAngularVelocity3D"), property.FindPropertyRelative("angularVelocity3D"), new GUIContent("Angular Velocity 3D"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<bool>(rect, property.FindPropertyRelative("useApplyShapeToPosition"), property.FindPropertyRelative("applyShapeToPosition"), new GUIContent("Apply Shape To Position"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<Vector3>(rect, property.FindPropertyRelative("usePosition"), property.FindPropertyRelative("position"), new GUIContent("Position"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<float>(rect, property.FindPropertyRelative("useRotation"), property.FindPropertyRelative("rotation"), new GUIContent("Rotation"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<Vector3>(rect, property.FindPropertyRelative("useRotation3D"), property.FindPropertyRelative("rotation3D"), new GUIContent("Rotation 3D"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<Color>(rect, property.FindPropertyRelative("useStartColor"), property.FindPropertyRelative("startColor"), new GUIContent("Start Color"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<float>(rect, property.FindPropertyRelative("useStartLifetime"), property.FindPropertyRelative("startLifetime"), new GUIContent("Start Lifetime"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<float>(rect, property.FindPropertyRelative("useStartSize"), property.FindPropertyRelative("startSize"), new GUIContent("Start Size"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<Vector3>(rect, property.FindPropertyRelative("useStartSize3D"), property.FindPropertyRelative("startSize3D"), new GUIContent("Start Size 3D"));

			rect.y += (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
			DrawProperty<Vector3>(rect, property.FindPropertyRelative("useVelocity"), property.FindPropertyRelative("velocity"), new GUIContent("Velocity"));

			EditorGUI.EndProperty();
		}

		private void DrawProperty<T>(Rect position, SerializedProperty useProp, SerializedProperty fieldProp, GUIContent label)
		{
			Rect rect;

			rect = new Rect(position.x, position.y, EditorGUIUtility.labelWidth, position.height);
			useProp.boolValue = EditorGUI.ToggleLeft(rect, label, useProp.boolValue);

			EditorGUI.BeginChangeCheck();
			rect = new Rect(position.x + EditorGUIUtility.labelWidth, position.y, position.width - EditorGUIUtility.labelWidth, position.height);
			switch (typeof(T))
			{
				case Type t when t == typeof(int):
					fieldProp.intValue = EditorGUI.IntField(rect, new GUIContent(string.Empty), fieldProp.intValue);
					break;

				case Type t when t == typeof(float):
					fieldProp.floatValue = EditorGUI.FloatField(rect, new GUIContent(string.Empty), fieldProp.floatValue);
					break;

				case Type t when t == typeof(bool):
					fieldProp.boolValue = EditorGUI.Toggle(rect, new GUIContent(string.Empty), fieldProp.boolValue);
					break;

				case Type t when t == typeof(Vector3):
					fieldProp.vector3Value = EditorGUI.Vector3Field(rect, new GUIContent(string.Empty), fieldProp.vector3Value);
					break;

				case Type t when t == typeof(Color):
					fieldProp.colorValue = EditorGUI.ColorField(rect, new GUIContent(string.Empty), fieldProp.colorValue);
					break;
			}
			if (EditorGUI.EndChangeCheck() == true)
			{
				useProp.boolValue = true;
			}
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) * 12f;
		}
	}
}