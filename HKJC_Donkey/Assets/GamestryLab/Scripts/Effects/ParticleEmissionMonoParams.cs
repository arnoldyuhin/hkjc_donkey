﻿using UnityEngine;

namespace GamestryLab.Effects
{
    public class ParticleEmissionMonoParams : MonoBehaviour
    {
        [SerializeField] private bool[] foldouts;

        [SerializeField] private ParticleSystem refSystem;
        public ParticleSystem RefSystem { get { return refSystem; } }

        [SerializeField] private ParticlePlayground.SerializableEmitParams emitParams;
        public ParticlePlayground.SerializableEmitParams EmitParams { get { return emitParams; } }

		[SerializeField] private int count;
		public int Count { get { return count; } }
	}
}