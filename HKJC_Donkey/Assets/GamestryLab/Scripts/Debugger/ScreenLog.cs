﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GamestryLab.System;
using GamestryLab.Utilities;
using UnityEngine;

namespace GamestryLab.Debugger {
	public class ScreenLog : MonoSingleton<ScreenLog> {

		private List<LogMessage> logMessages = new List<LogMessage> ();

		[SerializeField] private TextAnchor anchor;

		[SerializeField] private float presetDuration = 5f;
		[SerializeField] private Color BasicLogColor = Color.white;
		[SerializeField] private int BasicFontSize = 36;

		[SerializeField] private Vector2 logAnchor = new Vector2 (0.05f, 0.05f);
		[SerializeField] private Vector2 logSize = new Vector2 (0.9f, 0.9f);

		private void Update () {
			for (int i = 0; i < logMessages.Count; i++) {
				logMessages [i].ManualUpdate ();
			}
		}

		public LogMessage GetLogMessage (string id) {
			LogMessage log = logMessages.Find (x => x.Id == id);
			if (log == null) {
				log = new LogMessage (id);
				log.LogColor = BasicLogColor;
				log.FontSize = BasicFontSize;
				log.Destroyed += (l => {
					logMessages.Remove (log);
				});
				logMessages.Add (log);
			}
			return log;
		}

		public static LogMessage Log (string id, string message) {
			return Log (id, message, Instance.presetDuration, LogMode.Replace);
		}

		public static LogMessage Log (string id, string message, float duration) {
			return Log (id, message, duration, LogMode.Replace);
		}

		public static LogMessage Log (string id, string message, LogMode logMode) {
			return Log (id, message, Instance.presetDuration, logMode);
		}

		public static LogMessage Log (string id, string message, float duration, LogMode logMode) {
			LogMessage log = Instance.GetLogMessage (id).SetDuration (duration);
			switch (logMode) {
			case LogMode.Replace:
				log.Message = message;
				break;
			case LogMode.AddLine:
				log.AddMessage (message);
				break;
			}
			return log;
		}

		public static void RemoveLog (string id) {
			if (Instance.logMessages.Exists (x => x.Id == id)) {
				LogMessage log = Instance.logMessages.Find (x => x.Id == id);
				Instance.logMessages.Remove (log);
			}
		}

		public static void RemoveAllLogs () {
			Instance.logMessages.Clear ();
		}

		private void OnGUI () {
			if (logMessages.Count == 0) {
				return;
			}
			GUILayout.BeginArea (new Rect (
				Screen.width * logAnchor.x,
				Screen.height * logAnchor.y,
				Screen.width * logSize.x,
				Screen.height * logSize.y));
			
			for (int i = 0; i < logMessages.Count; i++) {
				GUIStyle style = new GUIStyle ();
				style.alignment = anchor;
				style.wordWrap = true;
				style.fontSize = logMessages [i].FontSize;
				style.normal.textColor = logMessages [i].LogColor;
				GUILayout.Label (logMessages [i].Message, style);
			}
			GUILayout.EndArea ();
		}
	}
}
