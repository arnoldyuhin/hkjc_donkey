﻿using System;
using GamestryLab.System;
using UnityEngine;

namespace GamestryLab.Debugger {
	public class LogMessage : IManualUpdatable {
		public string Id;
		public string Message;
		public bool IsRelyOnTime = false;
		public float Duration;
		public Color LogColor = Color.black;
		public int FontSize = 12;

		public Action<LogMessage> Updated;
		public Action<LogMessage> Destroyed;

		public LogMessage (string id) {
			Id = id;
		}

		public LogMessage SetDuration (float duration) {
			IsRelyOnTime = true;
			Duration = duration;
			return this;
		}

		public LogMessage SetMessage (string msg) {
			Message = msg;
			return this;
		}

		public LogMessage SetColor (Color color) {
			LogColor = color;
			return this;
		}

		public LogMessage AddMessage (string message) {
			if (String.IsNullOrEmpty (message)) {
				return SetMessage (message);
			}
			Message = String.Concat (Message, "\n", message);
			return this;
		}

		public void ManualUpdate () {
			if (IsRelyOnTime) {
				Duration -= AppManager.Instance.deltaTime;
				if (Duration <= 0f) {
					Destroy ();
				}
			}
		}

		public void Destroy() {
			if (Destroyed != null) {
				Destroyed (this);
			}
		}

	}
}