﻿using System;
using System.Collections;
using System.Collections.Generic;
using GamestryLab.System;
using GamestryLab.Utilities;
using UnityEngine;

namespace GamestryLab.Debugger
{
    public class DebugLog : MonoSingleton<DebugLog>
    {
        public Action<string> DebugLogged;

        protected override void Awake()
        {
            base.Awake();
            this.transform.SetParent(AppManager.Instance.transform);
        }

        private void Start() { }

        public static void Log(string _msg)
        {
            Log(Color.black, _msg);
        }

        public static void Log(string _msg, params object[] _arg)
        {
            Log(Color.black, _msg, _arg);
        }

        public static void Log(Color _color, string _msg)
        {
            Instance.LogOutput(_color, _msg);
        }

        public static void Log(Color _color, string _msg, params object[] _arg)
        {
            Instance.LogOutput(_color, String.Format(_msg, _arg));
        }

        private void LogOutput(Color _color, string _msg)
        {
            if (isActiveAndEnabled == false)
            {
                return;
            }

#if UNITY_EDITOR
            Debug.LogFormat("<color=#{0}>#{1}: {2}</color>", ColorUtility.ToHtmlStringRGB(_color), Time.frameCount, _msg);
#endif
            DebugLogged?.Invoke(_msg);
        }
    }
}